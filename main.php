<?php
	//session_start();
	
	include_once 'global.php';
	include_once 'function.php';
	
	error_reporting();
	set_error_handler('errorHandler');
	
	include_once 'controller/db.php';
	include_once 'controller/route.php';
	
	function __autoload($class) {
		$class = str_replace('_', '/', $class) . '.php';
		
		if (file_exists($class)) {
			include_once $class;
		}
	}
	
	route::start($_SERVER['REDIRECT_URL']);
?>
