var YaShareInstance = new Ya.share({
		element: 'yaShare',
		elementStyle: {
		'type': 'small',
		'quickServices': ['vkontakte', 'facebook', 'twitter','gplus', 'linkedin']
		},
		onready: function(instance) {

		$(instance._block).find('a.b-share__handle:first').remove();
		$(instance._block).find('a.b-share__handle').each(function() {
		var el = $(this);
		el.removeClass();
		el.wrap( "<li></li>" );


		if (el.find('span').hasClass('b-share-icon_vkontakte')) {
		el.find('span').remove();
		el.addClass('img-replace vk').append('<i class="fa fa-vk"></i>');
		}
		else if (el.find('span').hasClass('b-share-icon_facebook')) {
		el.find('span').remove();
		el.addClass('img-replace facebook').append('<i class="fa fa-facebook"></i>');
		}
		else if (el.find('span').hasClass('b-share-icon_twitter')) {
		el.find('span').remove();
		el.addClass('img-replace twitter').append('<i class="fa fa-twitter"></i>');
		}
		else if (el.find('span').hasClass('b-share-icon_gplus')) {
		el.find('span').remove();
		el.addClass('img-replace google').append('<i class="fa fa-google-plus fa-fw"></i>');
		}
		else if (el.find('span').hasClass('b-share-icon_linkedin')) {
		el.find('span').remove();
		el.addClass('img-replace linkedin').append('<i class="fa fa-linkedin fa-fw"></i>');
		}


		});

		var inner=$(instance._block).addClass('share-social').find('span.b-share li').wrapAll( "<ul></ul>" );
		}
		});

		$('.reply-link').on('click',function(e) {
		e.preventDefault();
		$('html, body').stop().animate({scrollTop: $('#create-comment').offset().top}, 800);
		var commentID = $(this).data('commentid');
		$('#create-comment form input[name=PARENT_ID]').val(commentID);
		});
		YaShareInstance.updateShareLink('http://api.yandex.ru', 'API');