;(function () {
	
	'use strict';
	
		$(document).ready(function(){
			$('#security').on('change', function(){
				$('#wrong_secure').hide();
			});
			
            $(".eq").equalize({children: '.inco-block,.overlay'});
            $(".eq").equalize({children: '.service-offer'});
            $('form').on('submit', function(){
			event.preventDefault();
			
			if (!$('#security').is(':checked')) {
			$('#wrong_secure').show();
				return false;
			}
		
			$('#wrong_secure').hide();
			
			var fd  = new FormData();
			var arr = $(this).find("input, textarea, select");
			
			fd.append('participants_conf_id', $('fieldset li.active>a').attr('rel'));
			
			for (var i = 0, len = arr.length; i < len; ++i) {
				if ($(arr[i]).is(":radio")) {
					if ($(arr[i]).filter(":checked").val() != undefined) {
						fd.append($(arr[i]).attr('name'), $(arr[i]).filter(":checked").val());
					}
				}
				else if ($(arr[i])[0].type == 'checkbox') {
					fd.append($(arr[i]).attr('name'), $(arr[i])[0].checked? 1 : 0);
				}
				else {
					fd.append($(arr[i]).attr('name'), $(arr[i]).val());
				}
			}
			
			$.ajax({
				url         : MAIN + LANG + 'participant/ajaxAdd/',
				data        : fd,
				cache       : false,
				contentType : false,
				processData : false,
				type        : 'POST',
				dataType    : 'json',
				success     : function(data) {
					if (data['error']) {
						$('#myModal2').modal('show');
					}
					else {
						$('#myModal').modal('show');
					}
				}
			});
		});
	});


	// iPad and iPod detection	
	var isiPad = function(){
		return (navigator.platform.indexOf("iPad") != -1);
	};

	var isiPhone = function(){
	    return (
			(navigator.platform.indexOf("iPhone") != -1) || 
			(navigator.platform.indexOf("iPod") != -1)
	    );
	};

	// Parallax
	var parallax = function() {
		$(window).stellar();
	};



	// Burger Menu
	var burgerMenu = function() {

		$('body').on('click', '.js-inco-nav-toggle', function(event){

			event.preventDefault();

			if ( $('#navbar').is(':visible') ) {
				$(this).removeClass('active');
			} else {
				$(this).addClass('active');	
			}

			
			
		});

	};


	var goToTop = function() {

		$('.js-gotop').on('click', function(event){
			
			event.preventDefault();

			$('html, body').animate({
				scrollTop: $('html').offset().top
			}, 500);
			
			return false;
		});
	
	};


	// Page Nav
	var clickMenu = function() {

		$('#navbar a:not([class="external"])').click(function(event){
			var section = $(this).data('nav-section'),
				navbar = $('#navbar');

				if ( $('[data-section="' + section + '"]').length ) {
			    	$('html, body').animate({
			        	scrollTop: $('[data-section="' + section + '"]').offset().top
			    	}, 500);
			   }

		    if ( navbar.is(':visible')) {
		    	navbar.removeClass('in');
		    	navbar.attr('aria-expanded', 'false');
		    	$('.js-inco-nav-toggle').removeClass('active');
		    }

		    event.preventDefault();
		    return false;
		});


	};
	var clickBtn = function(){

		$('#inco-intro .inco-text a').click(function(event){
			var section = $(this).data('btn-section');

				if ( $('[data-section="' + section + '"]').length ) {
			    	$('html, body').animate({
			        	scrollTop: $('[data-section="' + section + '"]').offset().top
			    	}, 500);
			   }

		    event.preventDefault();
		    return false;
		});
		$('.reg-btn').click(function(event){
			var section = $(this).data('btn-section');

				if ( $('[data-section="' + section + '"]').length ) {
			    	$('html, body').animate({
			        	scrollTop: $('[data-section="' + section + '"]').offset().top
			    	}, 500);
			   }

		    event.preventDefault();
		    return false;
		});


	};
	// Reflect scrolling in navigation
	var navActive = function(section) {

		var $el = $('#navbar > ul');
		$el.find('li').removeClass('active');
		$el.each(function(){
			$(this).find('a[data-nav-section="'+section+'"]').closest('li').addClass('active');
		});

	};

	var navigationSection = function() {
		var $section = $('section[data-section]');
		
		$section.waypoint(function(direction) {
		  	
		  	if (direction === 'down') {
		    	navActive($(this.element).data('section'));
		  	}
		}, {
	  		offset: '150px'
		});

		$section.waypoint(function(direction) {
		  	if (direction === 'up') {
		    	navActive($(this.element).data('section'));
		  	}
		}, {
		  	offset: function() { return -$(this.element).height() + 155; }
		});

	};


	


	// Window Scroll
	var windowScroll = function() {
		var lastScrollTop = 0;

		$(window).scroll(function(event){

		   	var header = $('#inco-header'),
				scrlTop = $(this).scrollTop();

			if ( scrlTop > 500 && scrlTop <= 2000 ) {
				header.addClass('navbar-fixed-top inco-animated slideInDown');
			} else if ( scrlTop <= 500) {
				if ( header.hasClass('navbar-fixed-top') ) {
					header.addClass('navbar-fixed-top inco-animated slideOutUp');
					setTimeout(function(){
						header.removeClass('navbar-fixed-top inco-animated slideInDown slideOutUp');
					}, 100 );
				}
			} 
			
		});
	};



	// Animations
	// Home

	var homeAnimate = function() {
		if ( $('#inco-home').length > 0 ) {	

			$('#inco-home').waypoint( function( direction ) {
										
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {


					setTimeout(function() {
						$('#inco-home .to-animate').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInUp animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, 200);

					
					$(this.element).addClass('animated');
						
				}
			} , { offset: '80%' } );

		}
	};


	var introAnimate = function() {
		if ( $('#inco-intro').length > 0 ) {	

			$('#inco-intro').waypoint( function( direction ) {
										
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {


					setTimeout(function() {
						$('#inco-intro .to-animate').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInRight animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, 1000);

					
					$(this.element).addClass('animated');
						
				}
			} , { offset: '80%' } );

		}
	};

    var committeeAnimate = function() {
        if ( $('#committee').length > 0 ) {
            $('#committee').waypoint( function( direction ) {

                if( direction === 'down' && !$(this.element).hasClass('animated') ) {

                    setTimeout(function() {
                        $('#committee .to-animate').each(function( k ) {
                            var el = $(this);

                            setTimeout ( function () {
                                el.addClass('fadeInUp animated');
                            },  k * 200, 'easeInOutExpo' );

                        });
                    }, 200);
                    $(this.element).addClass('animated');

                }
            } , { offset: '80%' } );

        }
    };


    var cpsAnimate = function() {
        if ( $('#cps').length > 0 ) {

            $('#cps').waypoint( function( direction ) {

                if( direction === 'down' && !$(this.element).hasClass('animated') ) {


                    setTimeout(function() {
                        $('#cps .to-animate').each(function( k ) {
                            var el = $(this);

                            setTimeout ( function () {
                                el.addClass('fadeInUp animated');
                            },  k * 200, 'easeInOutExpo' );

                        });
                    }, 200);


                    $(this.element).addClass('animated');

                }
            } , { offset: '80%' } );

        }
    };

	var workAnimate = function() {
		if ( $('#inco-work').length > 0 ) {	

			$('#inco-work').waypoint( function( direction ) {
										
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {


					setTimeout(function() {
						$('#inco-work .to-animate').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInUp animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, 200);

					
					$(this.element).addClass('animated');
						
				}
			} , { offset: '80%' } );

		}
	};
	
	var eesAnimate = function() {
		if ( $('#inco-ees').length > 0 ) {	

			$('#inco-ees').waypoint( function( direction ) {
										
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {


					setTimeout(function() {
						$('#inco-ees .to-animate').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInUp animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, 200);

					
					$(this.element).addClass('animated');
						
				}
			} , { offset: '80%' } );

		}
	};

	var tkAnimate = function() {
		if ( $('#inco-tk').length > 0 ) {	

			$('#inco-tk').waypoint( function( direction ) {
										
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {


					setTimeout(function() {
						$('#inco-tk .to-animate').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInUp animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, 200);

					
					$(this.element).addClass('animated');
						
				}
			} , { offset: '80%' } );

		}
	};
	
		var  regAnimate = function() {
		if ( $('#inco-reg').length > 0 ) {	

			$('#inco-reg').waypoint( function( direction ) {
										
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {


					setTimeout(function() {
						$('#inco-reg .to-animate').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInUp animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, 200);

					
					$(this.element).addClass('animated');
						
				}
			} , { offset: '80%' } );

		}
	};
	
	var  activityAnimate = function() {
		if ( $('#inco-activity').length > 0 ) {	

			$('#inco-activity').waypoint( function( direction ) {
										
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {


					setTimeout(function() {
						$('#inco-activity .to-animate').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInUp animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, 200);

					
					$(this.element).addClass('animated');
						
				}
			} , { offset: '80%' } );

		}
	};
	
	var testimonialAnimate = function() {
		var testimonial = $('#inco-testimonials');
		if ( testimonial.length > 0 ) {	

			testimonial.waypoint( function( direction ) {
										
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {

					var sec = testimonial.find('.to-animate').length,
						sec = parseInt((sec * 200) - 400);

					setTimeout(function() {
						testimonial.find('.to-animate').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInUp animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, 200);

					setTimeout(function() {
						testimonial.find('.to-animate-2').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInDown animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, sec);

					
					$(this.element).addClass('animated');
						
				}
			} , { offset: '80%' } );

		}
	};

	var servicesAnimate = function() {
		var services = $('#inco-services');
		if ( services.length > 0 ) {	

			services.waypoint( function( direction ) {
										
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {

					var sec = services.find('.to-animate').length,
						sec = parseInt((sec * 200) + 400);

					setTimeout(function() {
						services.find('.to-animate').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInUp animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, 200);

					setTimeout(function() {
						services.find('.to-animate-2').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('bounceIn animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, sec);


					
					$(this.element).addClass('animated');
						
				}
			} , { offset: '80%' } );

		}
	};

	var aboutAnimate = function() {
		var about = $('#inco-about');
		if ( about.length > 0 ) {	

			about.waypoint( function( direction ) {
										
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {


					setTimeout(function() {
						about.find('.to-animate').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInUp animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, 200);

					

					$(this.element).addClass('animated');
						
				}
			} , { offset: '80%' } );

		}
	};

	var countersAnimate = function() {
		var counters = $('#inco-counters');
		if ( counters.length > 0 ) {	

			counters.waypoint( function( direction ) {
										
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {

					var sec = counters.find('.to-animate').length,
						sec = parseInt((sec * 200) + 400);

					setTimeout(function() {
						counters.find('.to-animate').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInUp animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, 200);

					setTimeout(function() {
						counters.find('.js-counter').countTo({
						 	formatter: function (value, options) {
				      		return value.toFixed(options.decimals);
				   		},
						});
					}, 400);

					setTimeout(function() {
						counters.find('.to-animate-2').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('bounceIn animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, sec);

					

					

					$(this.element).addClass('animated');
						
				}
			} , { offset: '80%' } );

		}
	};


	var contactAnimate = function() {
		var contact = $('#inco-contact');
		if ( contact.length > 0 ) {	

			contact.waypoint( function( direction ) {
										
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {

					setTimeout(function() {
						contact.find('.to-animate').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInUp animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, 200);

					$(this.element).addClass('animated');
						
				}
			} , { offset: '80%' } );

		}
	};

    var isFilled = function() {
        if (!String.prototype.trim) {
            (function() {
                String.prototype.trim = function() {
                    return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
                };
            })();
        }

        [].slice.call($('input.input__field')).forEach(function(inputEl) {
            // in case the input is already filled..
            if( inputEl.value.trim() !== '' ) {
				inputEl.parent().addClass('input--filled' );
            }

            // events:
            inputEl.addEventListener('focus', onInputFocus );
            inputEl.addEventListener('blur', onInputBlur );
        } );

        function onInputFocus(ev) {
            $(this).parent().addClass('input--filled' );
        }

        function onInputBlur( ev ) {
            if( ev.target.value.trim() === '' ) {
                $(this).parent().removeClass('input--filled' );
            }
        }
    };

    var stepForm = function(){


		var current_fs, next_fs, previous_fs, goTo_fs; //fieldsets
		var left, opacity, scale; //fieldset properties which we will animate
		var animating; //flag to prevent quick multi-click glitches
        var form = $("#inco-form");
        var nextAnim = function (current_fs, next_fs){
            current_fs.animate({opacity: 0}, {
                step: function(now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale current_fs down to 80%
                    //scale = 1 - (1 - now) * 0.2;
                    //2. bring next_fs from the right(50%)
                    left = (now * 50)+"%";
                    //3. increase opacity of next_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({
                        //'transform': 'scale('+scale+')',
                        'position': 'absolute'
                    });
                    next_fs.css({'left': left, 'opacity': opacity});
                },
                duration: 800,
                complete: function(){
                    next_fs.css({'position':'relative'});
                    current_fs.hide();
                    animating = false;
                },
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });
		};
        var prevAnim = function (current_fs, previous_fs){
            current_fs.animate({opacity: 0}, {
                step: function(now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale previous_fs from 80% to 100%
                    //scale = 0.8 + (1 - now) * 0.2;
                    //2. take current_fs to the right(50%) - from 0%
                    left = ((1-now) * 50)+"%";
                    //3. increase opacity of previous_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({'left': left});
                    previous_fs.css({
						//'transform': 'scale('+scale+')',
						'opacity': opacity
                    });
                },
                duration: 800,
                complete: function(){
                    previous_fs.css({'position':'relative'});
                    current_fs.hide();
                    animating = false;
                },
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });
		};
        var checkConf = function (){
            var eee = $('#reg-eee');
            if (!eee.hasClass('active')){
                eee.css({
                    'display' : 'none'
                });
            } else {
                eee.css({
                    'display' : 'block'
                })
            }
		};
        if (!form.hasClass('inco-eng-form')) {
            $.extend($.validator.messages, {required: "Это поле является обязательным"});
        }
$(".next").click(function(){
	if(animating) {
		return false;
	}


				form.validate();
					
				if (form.valid() === true){
					animating = true;
					current_fs = $(this).parent();
					next_fs = $(this).parent().next();
	
	
	
	//activate next step on progressbar using the index of next_fs
	$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
	
	//show the next fieldset
	next_fs.show(); 
	//hide the current fieldset with style
	nextAnim(current_fs, next_fs);
	checkConf();
}
});

$(".previous").click(function(){
	if(animating) {
		return false;
	}
	animating = true;


	current_fs = $(this).parent();
	previous_fs = $(this).parent().prev();
	
	//de-activate current step on progressbar
	$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
	
	//show the previous fieldset
	previous_fs.show(); 
	//hide the current fieldset with style
	prevAnim(current_fs, previous_fs);
});

$('#progressbar li').click(function(){
    if(animating) {
        return false;
    }
    animating = true;
    var actIndex = $('#progressbar li.active').last().index();
    var curIndex = $('#progressbar li').index(this);
    console.log(actIndex);
    console.log(curIndex);
	current_fs = $('fieldset:visible');
	goTo_fs = $('#inco-reg fieldset').eq(curIndex);
	if (curIndex > actIndex){
		for (var i=1; i<=curIndex; i++){
			$('#progressbar li').eq(i).addClass('active');
		}
        goTo_fs.show();
		nextAnim(current_fs, goTo_fs);
		checkConf();
	} else if (curIndex < actIndex) {
		for (var i=4; i>curIndex; i--){
            $('#progressbar li.active').eq(i).removeClass('active');
		}
        goTo_fs.show();
        prevAnim(current_fs, goTo_fs);
	}


});
	};



	

var timelineAnim = function(){
	
	var items = document.querySelectorAll(".timeline li");
 
	
// code for the isElementInViewport function
	function isElementInViewport(el) {
  	var rect = el.getBoundingClientRect();
  	return (
    rect.top >= 0 &&
    rect.left >= 0 &&
    rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
    rect.right <= (window.innerWidth || document.documentElement.clientWidth)
  );
}
 
function callbackFunc() {
  for (var i = 0; i < items.length; i++) {
    if (isElementInViewport(items[i])) {
      items[i].classList.add("in-view");
    }
  }
}
 
window.addEventListener("load", callbackFunc);
window.addEventListener("scroll", callbackFunc);
};
	
	/*
    |--------------------------------------------------------------------------
    |  iCheck
    |--------------------------------------------------------------------------
    */
 
	var ickeckInit = function(){
		$('input').iCheck({
    		checkboxClass: 'icheckbox_flat-green',
  		});
	};
	// Document on load.
	$(function(){

		parallax();

		burgerMenu();

		clickMenu();

		windowScroll();

		navigationSection();

		goToTop();

		timelineAnim();
		ickeckInit();


		// Animations
		homeAnimate();
		clickBtn();
		introAnimate();
		workAnimate();
        committeeAnimate();
        cpsAnimate();
		eesAnimate();
		tkAnimate();
		regAnimate();
		activityAnimate();
		testimonialAnimate();
		servicesAnimate();
		aboutAnimate();
		countersAnimate();
		contactAnimate();
		stepForm();
		isFilled();
		
 //Validate
		$("#inco-form").validate();
	});


}());

function Strech(name) {
	var bl = [],
		padding_top = 0,
		padding_bottom = 0,
		border_top = 0,
		border_bottom = 0;
	$(name).height("auto");
	$(name).each(function(i) {
		padding_top = parseFloat($(this).css("padding-top").replace("px", ""));
		padding_bottom = parseFloat($(this).css("padding-bottom").replace("px", ""));
		border_top = parseFloat($(this).css("border-top-width").replace("px", ""));
		border_bottom = parseFloat($(this).css("border-bottom-width").replace("px", ""));
		bl[i] = $(this).height() + padding_top + padding_bottom + border_top + border_bottom;
	});
	var mh = Math.max.apply(Math, bl);
	$(name).each(function(i) {
		$(this).css("height", mh);
	});
}

$(document).ready(function () {
	$('.owl-carousel').each(function(index) {
		var Owl = $(this);
		Owl.owlCarousel({
			itemsCustom: [
				[0, 1],
				[500, Owl.data("owl-xs")],
				[786, Owl.data("owl-sm")],
				[992, Owl.data("owl-md")],
				[1170, Owl.data("owl-lg")]
			],
			navigation: true
		});
	});
	$(window).on("load", function() {
		StrAll();
	});

	function StrAll() {
		setTimeout(Strech(".speaker-card__name"), 300);
	}
});