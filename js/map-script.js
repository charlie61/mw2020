// JavaScript Document
"use strict";
$(function(){
    var mainColor = "#4491ed";
    var markerColor ="#09c";
    var svgimg = '<svg viewBox="0 0 30 40" xmlns="http://www.w3.org/2000/svg">\n' +
        '<path d="m11.6 0.4c3.3-0.8 6.8-0.4 9.8 1.1 2.8 1.4 5.2 3.7 6.6 6.5 1.5 2.8 2.2 6.1 1.8 9.3-0.4 2.7-1.5 5.3-3 7.6-1.3 2-2.7 3.9-4.3 5.7-2.5 3.2-5.1 6.3-7.6 9.4-3-3.3-5.9-6.6-8.7-10.1-2-2.2-3.7-4.8-4.9-7.5-1.2-2.8-1.6-5.9-1.2-9 0.4-3.2 1.8-6.2 4-8.6 2-2.1 4.6-3.7 7.5-4.4m3.4 8.5c-3.5 0-6.3 2.8-6.3 6.3s2.8 6.3 6.3 6.3 6.3-2.8 6.3-6.3-2.8-6.3-6.3-6.3z" fill="'+ markerColor +'"/>\n' +
        '</svg>';
    if($('#map').length){
    var mapOptions = {
        center: new google.maps.LatLng(60.006581497247694,30.379514694213867),
        zoom: 14,
        scrollwheel: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var bounds = new google.maps.LatLngBounds();
    var map = new google.maps.Map(document.getElementById('map'), mapOptions);

    // Multiple Markers
    var markers = [
            ['Research Building',60.006581497247694,30.379514694213867]
        ];

    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(),
        marker, i;

    // Loop through our array of markers & place each one on the map
    for (i = 0; i < markers.length; i++) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            icon:{
                url: 'data:image/svg+xml;utf8,' + encodeURIComponent(svgimg),
                anchor: new google.maps.Point(15,40),
                scaledSize: new google.maps.Size(30,40)
            },
            title: markers[i][0]
        });
        // Allow each marker to have an info window
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent('<div class="info_content">'+markers[i][0]+'</div>');
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // This is needed to set the zoom after fitbounds,
        google.maps.event.addListener(map, 'zoom_changed', function() {
            var zoomChangeBoundsListener = google.maps.event.addListener(map, 'bounds_changed', function(event) {
                if (this.getZoom() > 15 && this.initialZoom == true) {
                    // Change max/min zoom here
                    this.setZoom(16);
                    this.initialZoom = false;
                }
                google.maps.event.removeListener(zoomChangeBoundsListener);
            });
        });
        // Automatically center the map fitting all markers on the screen
        map.initialZoom = true;
        map.fitBounds(bounds);
    }
}
});
