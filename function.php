<?php
	function getHash($size = 32) {
		$str  = "abcdefghijklmnopqrstuvwxyz0123456789";
		$hash = "";
		$len  = strlen($str) - 1;
		
		for ($i = 0; $i < $size; $i++) {
			$hash.= $str[rand(0, $len)];
		}
		return $hash;
	}
	
	function errorHandler ($errno, $errstr, $errfile, $errline) {
		if	($errno) {
			$date = date('d_m_y', time());
			if (!file_exists(LOG)) {
				mkdir(LOG, 755);
			}
			if (!file_exists(LOG . date('d_m_y', time()) . '_errors.html')) {
				$pattern = file_get_contents(LOG . 'error_pattern.html');
				file_put_contents(LOG . $date . '_errors.html', $pattern);
			}
			$err  = fopen(LOG . $date . '_errors.html', 'a');
			$text = '
			<tr>
				<td>' . date('H:i:s', time()) . '</td>
				<td>' . $errfile . ' </td>
				<td>' . $errline . ' </td>
				<td>' . $errno   . ' </td>
				<td>' . $errstr  . ' </td>
				<td></td>
			</tr> ';
			fwrite ($err, $text);
			fclose ($err);
		}
		return true;
	}
	
	function writeErrLog ($file, $line, $num, $text, $query) {
		$date = date('d_m_y', time());
			if (!file_exists(LOG)) {
				mkdir(LOG, 755);
			}
			if (!file_exists(LOG . date('d_m_y', time()) . '_errors.html')) {
				$pattern = file_get_contents(LOG . 'error_pattern.html');
				file_put_contents(LOG . $date . '_errors.html', $pattern);
			}
			$err  = fopen(LOG . $date . '_errors.html', 'a');
			$text = '
			<tr class="db">
				<td>' . date('H:i:s', time()) . '</td>
				<td>' . $file  . ' </td>
				<td>' . $line  . ' </td>
				<td>' . $num   . ' </td>
				<td>' . $text  . ' </td>
				<td>' . $query . ' </td>
			</tr> ';
			fwrite ($err, $text);
			fclose ($err);
	}
	
	function getPageContent($page, $item = array()) {
		$content = '';
		
		if (isset($page)) {
			ob_start();
			viewInit($page, array('item' => $item));
			$content = ob_get_contents();
			ob_end_clean();
		}
		
		return $content;
	}
	
	function viewInit($view, $arrList = array()) {
		global $title;
		global $infoBlocks;
		global $sets;
		
		foreach ($arrList as $key => $value) {
			$$key = $value;
		}
		
		$view     = str_replace('.html', '', $view);
		$viewPath = VIEW . "$view.html";
		
		if(file_exists($viewPath)) {
			include $viewPath;
		}
	}
?>