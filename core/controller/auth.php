<?php
	require("core.php");//глобальные переменные и функции
	
	$user = checkUser();
	
	if (!empty($user)) {
		header('Location: ' . COREPATH);
	}
	
	// $arrJs - массив для подключения дополнительных скриптов
	$arrJs[] = CORE_JS . 'reg.js';
	$arrJs[] = CORE_JS . 'bootstrap-notify.min.js';
	
	// $arrCss - массив для подключения дополнительных стилей
	$arrCss[] = CORE_CSS . 'bootstrap.css';
	
	// $arrMain - массив для подключения основных страниц, заполняется для каждого конекретного блока
	$arrMain  = array();
	$mainPaig = 'auth/auth';
	
	viewCoreInit('auth/auth_header', array('arr' => $arrCss));
	viewCoreInit($mainPaig, $arrMain);
	viewCoreInit('main/jcore', array('arr' => $arrJs));
	viewCoreInit('main/admin_end');
?>
	