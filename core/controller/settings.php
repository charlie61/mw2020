<?php
	require("core.php");
	
	$arrJs   = array();
	$arrCss  = array();
	$arrMain = array();
	$user    = checkUser();
	
	if (empty($user)) {
		header('Location: ' . CONTROL . 'auth/');
	}
	elseif ((int)($user['rules'] & SET) == 0) {
		viewCoreInit('main/admin_header', array('arr' => $arrCss));
		viewCoreInit('main/admin_menu');
		echo 'У Вас нет прав для работы с этим разделом';
		viewCoreInit('main/admin_footer');
		viewCoreInit('main/jcore', array('arr' => $arrJs));
		viewCoreInit('main/admin_end');
	}
	else {
		$arrJs[] = MAIN_CORE . 'ckeditor/ckeditor.js';
		$arrJs[] = MAIN_CORE . 'ckeditor/adapters/jquery.js';
		$arrJs[] = MAIN_CORE . 'ckfinder/ckfinder.js';
		$arrJs[] = CORE_JS   . 'settings.js';
		$arrJs[] = TEST ? 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBVzmVtkPKmYpW8ARtflLkjue5G4Baalvw&sensor=false&callback=initMap' : 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCBcS_wxjhY6HsbL28cpmccVjmX3yw0O0c&callback=initMap';
		
		$mainPaig  = '';
		
		if ($action == 'edit' AND $id == 0) {
			$action = '';
		}
		
		switch ($action) {
			case 'update':
				$data = $_POST;
				
				if(isset($_POST['fupload_logo_head'])) {
					$data['settings_logo_head'] = str_replace(USERFILES, '', $_POST['fupload_logo_head']);
				}
				if(isset($_POST['fupload_logo_head_en'])) {
					$data['settings_logo_head_en'] = str_replace(USERFILES, '', $_POST['fupload_logo_head_en']);
				}
				if(isset($_POST['fupload_logo_parent'])) {
					$data['settings_logo_parent'] = str_replace(USERFILES, '', $_POST['fupload_logo_parent']);
				}
				if(isset($_POST['fupload_logo_parent_en'])) {
					$data['settings_logo_parent_en'] = str_replace(USERFILES, '', $_POST['fupload_logo_parent_en']);
				}
				if(isset($_POST['fupload_logo_footer'])) {
					$data['settings_logo_footer'] = str_replace(USERFILES, '', $_POST['fupload_logo_footer']);
				}
				if(isset($_POST['fupload_logo_footer_en'])) {
					$data['settings_logo_footer_en'] = str_replace(USERFILES, '', $_POST['fupload_logo_footer_en']);
				}
				if(isset($_POST['fupload_marker'])) {
					$data['settings_marker'] = str_replace(USERFILES, '', $_POST['fupload_marker']);
				}
				
				$cnt = getValue('SELECT COUNT(*) FROM `settings`;');
				
				if ($cnt) {
					update('settings', $data);
				}
				else {
					$id = insert('settings', $data);
				}
				
				file_put_contents(dirname($_SERVER['DOCUMENT_ROOT'] . $_SERVER['SCRIPT_NAME']) . '/robots.txt', $_POST['settings_robots']);
				header('Location: ' . CONTROL . 'settings/');
				break;
			
			default:
				$title    = "Просмотр";
				$settings = getRow("SELECT * FROM `settings`;");
				$mainPaig = 'settings/settings_edit';
				$arrMain  = array('settings' => $settings);
		}
		
		// Отображение всех необходимых страниц из вьювера
		viewCoreInit('main/admin_header', array('arr' => $arrCss));
		viewCoreInit('main/admin_menu');
		viewCoreInit($mainPaig, $arrMain);
		viewCoreInit('main/admin_footer');
		viewCoreInit('main/jcore', array('arr' => $arrJs));
		viewCoreInit('main/admin_end');
	}
?>