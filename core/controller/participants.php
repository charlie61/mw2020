<?php
	require("core.php");//глобальные переменные и функции
	
	$user = checkUser();
	
	if (empty($user)) {
		header('Location: ' . CONTROL . 'auth/');
	}
	elseif ((int)($user['rules'] & PAR) == 0) {
		viewCoreInit('main/admin_header', array('arr' => $arrCss));
		viewCoreInit('main/admin_menu');
		echo 'У Вас нет прав для работы с этим разделом';
		viewCoreInit('main/admin_footer');
		viewCoreInit('main/jcore', array('arr' => $arrJs));
		viewCoreInit('main/admin_end');
	}
	else {
		// $arrJs - массив для подключения дополнительных скриптов
		$arrJs[]   = MAIN_CORE . 'ckeditor/ckeditor.js';
		$arrJs[]   = MAIN_CORE . 'ckeditor/adapters/jquery.js';
		$arrJs[]   = MAIN_CORE . 'ckfinder/ckfinder.js';
		$arrJs[]   = CORE_JS   . 'jquery.tagsinput.js';
		$arrJs[]   = CORE_JS   . 'participants.js';
		
		// $arrCss - массив для подключения дополнительных стилей
		$arrCss    = array();
		
		// $arrMain - массив для подключения основных страниц, заполняется для каждого конекретного блока
		$arrMain   = array();
		$mainPaig  = '';
		
		if ($id) {
			$query       = "SELECT * FROM `participants` WHERE `participants_id` = " . $id . ";";
			$participant = getRow($query);
			$title       = $participant['participants_last_name'] . ' ' . $participant['participants_first_name'] . ' ' . $participant['participants_patronymic'];
			$mainPaig    = 'participants/participant';
			$arrMain     = array('participant' => $participant);
		}
		else {
			$title = "Список";
			$info['filter'] = isset($_POST['filter']) ? mysqli_real_escape_string($db, $_POST['filter']) : '';
			$query = "
				SELECT 
						`participants_id`         AS `id`,
						`participants_last_name`  AS `lastName`,
						`participants_first_name` AS `firtName`,
						`participants_patronymic` AS `patronymic`,
						`participants_phone`      AS `phone`,
						`participants_email`      AS `email`,
						`participants_conf_id`    AS `conf_id`
					FROM `participants`
				WHERE `participants_is_delete` = 0
				AND (`participants_last_name` LIKE '%" . $info['filter'] . "%'
				OR  `participants_patronymic` LIKE '%" . $info['filter'] . "%'
				OR  `participants_first_name` LIKE '%" . $info['filter'] . "%'
				OR  `participants_email` LIKE '%" . $info['filter'] . "%'
				)
				ORDER BY `participants_add` DESC;
			";
			$participants = getArray($query);
			
			$mainPaig = 'participants/participants_main';
			$arrMain  = array('participants' => $participants, 'info' => $info);
		}
		
		// Отображение всех необходимых страниц из вьювера
		viewCoreInit('main/admin_header', array('arr' => $arrCss));
		viewCoreInit('main/admin_menu');
		viewCoreInit($mainPaig, $arrMain);
		viewCoreInit('main/admin_footer');
		viewCoreInit('main/jcore', array('arr' => $arrJs));
		viewCoreInit('main/admin_end');
	}
?>