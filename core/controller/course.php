<?php
	require("core.php");
	
	$arrJs   = array();
	$arrCss  = array();
	$arrMain = array();
	$user    = checkUser();
	
	if (empty($user)) {
		header('Location: ' . CONTROL . 'auth/');
	}
	elseif ((int)($user['rules'] & COU) == 0) {
		viewCoreInit('main/admin_header', array('arr' => $arrCss));
		viewCoreInit('main/admin_menu');
		echo 'У Вас нет прав для работы с этим разделом';
		viewCoreInit('main/admin_footer');
		viewCoreInit('main/jcore', array('arr' => $arrJs));
		viewCoreInit('main/admin_end');
	}
	else {
		$arrJs[] = MAIN_CORE . 'ckeditor/ckeditor.js';
		$arrJs[] = MAIN_CORE . 'ckeditor/adapters/jquery.js';
		$arrJs[] = MAIN_CORE . 'ckfinder/ckfinder.js';
		
		$mainPaig  = '';
		
		if ($action == 'edit' AND $id == 0) {
			$action = '';
		}
		
		switch ($action) {
			case 'update':
				$data = $_POST;
				
				$cnt = getValue('SELECT COUNT(*) FROM `course`;');
				
				if ($cnt) {
					update('course', $data);
				}
				else {
					$id = insert('course', $data);
				}
				
				header('Location: ' . CONTROL . 'course/');
				break;
			
			default:
				$title    = "Просмотр";
				$course   = getRow("SELECT * FROM `course`;");
				$mainPaig = 'course/course_edit';
				$arrMain  = array('course' => $course);
		}
		
		// Отображение всех необходимых страниц из вьювера
		viewCoreInit('main/admin_header', array('arr' => $arrCss));
		viewCoreInit('main/admin_menu');
		viewCoreInit($mainPaig, $arrMain);
		viewCoreInit('main/admin_footer');
		viewCoreInit('main/jcore', array('arr' => $arrJs));
		viewCoreInit('main/admin_end');
	}
?>