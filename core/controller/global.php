<?php
# Секция CORE 
define('COREPATH',        MAIN . 'control/participants/');
define('CORE',            MAIN . 'core/');
define('MAIN_CORE',       MAIN . 'core/');
define('CORE_MODEL',      DIR  . 'model/');
define('CORE_CONTROLLER', DIR  . 'controller/');
define('CORE_VIEW',       DIR  . 'view/');
define('CORE_CSS',        MAIN . 'core/css/');
define('CORE_JS',         MAIN . 'core/js/');
define('CORE_FONT',       MAIN . 'core/fonts/');
define('CORE_IMG',        MAIN . 'core/img/');
define('CORE_LIB',        MAIN . 'core/lib/');
define('FILE',            MAIN . 'files/');
define('CONTROL',         MAIN . 'control/');

define('SET', 1);
define('EMP', 2);
define('PAR', 4);
define('COU', 8);
define('ADS', 16);
define('FAQ', 32);