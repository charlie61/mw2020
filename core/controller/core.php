<?php
	require_once("global.php");
	//require CORE_MODEL . "sitemapGenerator.php";
	
	function sortList($items, $parent) {
		$block = array();
		foreach ($items as $item) {
			if ($item['parent'] == $parent) {
				$block[$item['id']] = $item;
				$block[$item['id']]['children'] = sortList($items, $item['id']);
			}
		}
		
		return $block;
	}
	
function show_info_block($lang, $block_name)  // вывод блока 
{
	$sql_slider_head="SELECT block_content_". $lang." from block WHERE block_name_ru='".$block_name."'";
	//echo($sql_slider_head);
	$res_slider=mysqli_query($GLOBALS['db'],$sql_slider_head);
	$rows_slider=mysqli_num_rows($res_slider);
	if($rows_slider)
	{
		$r_sl=mysqli_fetch_array($res_slider);
		if(!isset($r_sl['block_content_'. $lang]) || $r_sl['block_content_'. $lang] == '')
			return;
		return ( decode_cool( $r_sl['block_content_'. $lang] ) );
	}
}

function send_one_letter( $acceptor , $acceptor_hash_mail , $message  , $theme ) {
	$type_array = array();
	$type_array[] = $acceptor;
	
	$html=str_get_html(html_entity_decode(stripslashes($message)));
	foreach($html->find('img') as $key=>$img)
	{
		$cut=explode("currentFolder=", $img->src);//режем путь ссылки до миниатюры
		if( isset($cut[1]) &&  $cut[1]!='')
		{
			$im_name=explode("FileName=",$cut[1]);
			$img->src='http://pokrovorg.ru/userfiles/_thumbs/Images/'.$im_name[1]; 
		}
		else {$img->src='http://pokrovorg.ru'.$img->src;}
	}

	//$letter_header = show_info_block("ru" ,"letter_header" );
	
	$msg='<!DOCTYPE html>
	<html>
	<head>
	<meta charset="utf-8"/>
	<title></title>
	<style>
	a:hover {
		text-decoration: none!important;
		
	}

	@media screen {
	@font-face {
	  font-family: "Trajan Pro 3";
	  src: url("http://pokrovorg.ru/fonts/TrajanPro3-Bold.eot"); /* IE9 Compat Modes */
	  src: url("http://pokrovorg.ru/fonts/TrajanPro3-Bold.eot?#iefix") format("embedded-opentype"), /* IE6-IE8 */
		   url("http://pokrovorg.ru/fonts/TrajanPro3-Bold.woff") format("woff"), /* Modern Browsers */
		   url("http://pokrovorg.ru/fonts/TrajanPro3-Bold.ttf")  format("truetype"), /* Safari, Android, iOS */
		   url("http://pokrovorg.ru/fonts/TrajanPro3-Bold.svg#34a8b4aa993307be176dbb5105a0803a") format("svg"); /* Legacy iOS */
	}
	@font-face {
		font-family: "Open Sans";
		font-style: normal;
		font-weight: 400;
		src: local("Open Sans"), local("OpenSans"), url("http://themes.googleusercontent.com/static/fonts/opensans/v6/cJZKeOuBrn4kERxqtaUH3T8E0i7KZn-EPnyo3HZu7kw.woff") format("woff")
	}
	@font-face {
		font-family: "Open Sans";
	fon t-style:normal;
		font-weight: 700;
		src: local("Open Sans Bold"), local("OpenSans-Bold"), url(http://themes.googleusercontent.com/static/fonts/opensans/v6/k3k702ZOKiLJc3WVjuplzHhCUOGz7vYGh680lGh-uXM.woff) format("woff")
	}
	}
	</style>
	</head>
	<body>
		<table bgcolor="#444" width="100%" cellspacing="0" cellpadding="0" border="0" style="color: #333; font-size: 14px; line-height: 1.58; font-family: "Open Sans", Gill Sans, Arial, Helvetica, sans-serif;">
			<tr>
				<td align="center">
					<table width="600" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse;margin:40px 0px 50px;">
						<tr>
							<td bgcolor="#ffffff" align="center" valign="middle" style="padding-top: 10px;padding-bottom: 10px;">
								<a href="http://pokrovorg.ru"><img style="display: block;" border="0" src="http://pokrovorg.ru/img/logo_letter.png" height="108" width="108" alt="Ассоциация  содействия духовно-нравственному просвещению - Покров" title="Ассоциация  содействия духовно-нравственному просвещению - Покров"  />
								</a>
							</td>
						</tr>
						 <tr>
							<td bgcolor="#2b9cce" align="center" valign="middle" style="font-family:\'Trajan Pro 3\'; padding-top: 20px;padding-bottom: 20px; color: #FFFFFF;  text-align: center;">
						Ассоциация  содействия духовно-нравственному просвещению &laquo;Покров&raquo;
						 </td>
						</tr>
						<tr>
							<td bgcolor="#f5f5f5" style="padding-top: 25px; padding-right: 35px; padding-bottom: 25px; padding-left: 48px; border-bottom: 1px solid #e1e6ea;border-top: 1px solid #e1e6ea;">'.$html.'</td>
						</tr>
						<tr>
							<td bgcolor="#ffffff" align="center" style="padding-top: 15px; padding-right: 40px; padding-bottom: 15px; padding-left: 40px;">
								<p style="color:#545353;font-size: 12px; margin-top: 10px; margin-bottom: 8px; font-family:\'Trajan Pro 3\';">&copy; '.date('Y').' Ассоциация &laquo;Покров&raquo;</p><div style="margin-bottom:13px;">
									<a href="mailto:office@pokrovorg.ru" title="Отправить нам письмо" style="font-size: 12px;margin-left: 4px; margin-right: 4px; color:#2b9cce;font-family: \'Open Sans\'">office@pokrovorg.ru</a>
									<a href="http://pokrovorg.ru/dell_signup.php?signup_name='.$acceptor_hash_mail.'" title="Отказаться от рассылки" style="font-size: 12px;margin-left: 4px; margin-right: 4px; color:#2b9cce;font-family: \'Open Sans\'">Отказаться от рассылки</a>
								</div>
								
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
	</html>';	
	
	

	send_mime_mail_a('Ассоциация Покров', // имя отправителя
                     'info@pokrovorg.ru', // email отправителя
                     $type_array, // массив имен получателя
                     $type_array, // массив email-адресов получателя
                     'UTF-8', // кодировка переданных данных
                     'UTF-8', // кодировка письма
                     $theme, // тема письма
                     $msg // текст письма
						//$bound
    );
	if(isset($html))
	{
		$html->clear();
		unset($html);
	}		
}

function send_post_vk($news_id,$news_header,$news_content , $brief_ru,  $url, $news_icon)
{
	$img_array = array();
	foreach(str_get_html( $news_content )->find('img') as $key=>$img)	
	
		$img_array[] = $img->src;		
	
	$sql_auth="SELECT * FROM pandora WHERE pandora_name='pokrov'";
	$res_auth = mysqli_query($GLOBALS['db'], $sql_auth);
	if(mysqli_num_rows($res_auth))
	{
		$r_auth=mysqli_fetch_object($res_auth);
		$pandora = new VKPublic($r_auth->pandora_gid, $r_auth->pandora_uid,$r_auth->pandora_app_id, $r_auth->pandora_app_secret,$GLOBALS['db']);
		$pandora->setAccessData($r_auth->pandora_access_token,$r_auth->pandora_access_secret);
		
		$attachments = "";
		if(isset($news_icon)&&$news_icon){
				$img_array[]='/'.$news_icon;
		}
		
		/*echo('<pre>');
		print_r($img_array);
		echo('</pre>');*/
		
		foreach( $img_array as $key=>$value)
			$attachments .= strval($pandora->createPhotoAttachment($value)) . ',';
		$attachments .='http://'.$_SERVER['HTTP_HOST'].'/'.$url;
		$post_id_get= $pandora->wallPostAttachment($attachments, html_entity_decode( stripslashes( str_get_html( $brief_ru )->plaintext)), 1, 0);
		if(isset($post_id_get) && ($post_id_get)!=''){
			$sql_put_post_id="UPDATE news  SET news_vkpost_id='".$post_id_get."'  WHERE news_id='".$news_id."'";
			$res_post_id = mysqli_query($GLOBALS['db'], $sql_put_post_id); //выполним запрос и положим в бд
			
			}
	}		
}


function edit_post_vk($post_id,$news_id,$news_header,$news_content , $brief_ru,  $url, $news_icon)
{
	$img_array = array();
	foreach(str_get_html( $news_content )->find('img') as $key=>$img)	
		$img_array[] = $img->src;		
	
	$sql_auth="SELECT * FROM pandora WHERE pandora_name='pokrov'";
	$res_auth = mysqli_query($GLOBALS['db'], $sql_auth);
	if(mysqli_num_rows($res_auth))
	{
		$r_auth=mysqli_fetch_object($res_auth);
		$pandora = new VKPublic($r_auth->pandora_gid, $r_auth->pandora_uid,$r_auth->pandora_app_id, $r_auth->pandora_app_secret,$GLOBALS['db']);
		$pandora->setAccessData($r_auth->pandora_access_token,$r_auth->pandora_access_secret);
		
		$attachments = "";
		if(isset($news_icon)&&$news_icon){
				$img_array[]='/'.$news_icon;
		}
		foreach( $img_array as $key=>$value)
			$attachments .= strval($pandora->createPhotoAttachment($value)) . ',';
		//$attachments .='http://pokrovorg.ru/'.$url;
		$attachments .='http://'.$_SERVER['HTTP_HOST'].'/'.$url;
		$post_id_get= $pandora->wallPostEdit($post_id,$attachments, html_entity_decode( stripslashes( str_get_html( $brief_ru )->plaintext)), 1, 0);
	}
}

function dell_post_vk($post_id){
	$sql_auth="SELECT * FROM pandora WHERE pandora_name='pokrov'";
	$res_auth = mysqli_query($GLOBALS['db'], $sql_auth);
	if(mysqli_num_rows($res_auth)){
		$r_auth=mysqli_fetch_object($res_auth);
		$pandora = new VKPublic($r_auth->pandora_gid, $r_auth->pandora_uid,$r_auth->pandora_app_id, $r_auth->pandora_app_secret,$GLOBALS['db']);
		$pandora->setAccessData($r_auth->pandora_access_token,$r_auth->pandora_access_secret);
		
		$post_del= $pandora->wallDel($post_id);
		}
}


function send_me_a_postcard_darling( $message  , $theme , $indexes)
{
	
	$type_array = array();
	$select_type = "SELECT signups_id, signup_email_address , signups_hash_mail , signup_type_id 
					FROM signups where signup_type_id in (".$indexes.")";
	// echo($select_type . "<br/>");
	$result_type = mysqli_query($GLOBALS['db'], $select_type) or die (mysqli_error($db));
	while ($rows_type = mysqli_fetch_assoc($result_type))  
    {
		if( isset($rows_type['signup_email_address'] ) && $rows_type['signup_email_address']  !="" && 
		    isset($rows_type['signups_hash_mail'] ) && $rows_type['signups_hash_mail']  !="" )
		send_one_letter( $rows_type['signup_email_address'] , $rows_type['signups_hash_mail'] , $message  , $theme  );
    }

}


function select_albums_aray_core(  $lang  ) // выбрать те альбомы у которых есть картинка в которых есть фотки  
// и лишь те, которые привязаны к даной странице если  хочется получить все годные альбомы то прислать отрицательное число в качаестве page_id 
{
	/*echo('<pre>');
	print_r( $result_array);
	echo('</pre>');*/

	$sql_qwery="SELECT albums_id, albums_img, albums_title_".$lang." as title, albums_date , 
							albums_desc_".$lang." as description, 
						   (   select COUNT(photos_id) 
								from photos 
								where photos_albums_id = albums_id
							) as cat_num
					FROM albums 
					LEFT JOIN category on category.category_id = albums.albums_category_id		
					where albums_hide = 0 AND category.category_id = 22					
					ORDER BY  albums_date DESC "; // category.category_id != 60
	$res=mysqli_query($GLOBALS['db'], $sql_qwery);
	//echo($sql_qwery);
	$num_rows=mysqli_num_rows($res);
	$result_array = array();
	
	for($c=0;$c<$num_rows;$c++)
	{
		$rq=mysqli_fetch_object($res);	
		//echo("<br>". $rq->cat_num);
		
		if(!isset($rq->title) || $rq->title == '')
			continue;
		if(!isset($rq->description) || $rq->description == '')
			continue;
		if(!isset($rq->cat_num) || $rq->cat_num < 1)
			continue;
		if(!isset($rq->albums_img) || $rq->albums_img == '')
			continue;
		
		
		$result_array[$rq->albums_id] = $rq;
	}
	
	return $result_array;

}

function select_cat_id_by_type_core($type_id)
{
//category.category_name_".$lang." as cat_name ,
	$sql_query= "select 
					category.category_id as cat_id,
					rewrite_url, rewrite_path
					type_name, type_php 
				from category 
				LEFT JOIN  type  on category.category_type_id = type.type_id 
				where type.type_php = '".$type_id."'" ;
	/*if ($parent_type == 7)*/
		//echo ($sql_query) 	;	
	$res_q=mysqli_query($GLOBALS['db'],$sql_query);
	$row = mysqli_fetch_array($res_q);
	
	//$cat_name = mb_strtoupper( trim($row['cat_name']), 'UTF-8') ;
	
	//$link = (( isset($row['rewrite_url']) &&  $row['rewrite_url'] != '' )? $row['rewrite_url'] : "");
	//$link = $row['type_php'].'.php?cat='.$row['cat_id'];
	return 	$row['cat_id'];		
}

function create_sitemap() {
		
        //include 'core/sitemap_generator.class.php';
		//include("func.php");
		
        $time = explode(" ",microtime());
        $time = $time[1];
		$site=$_SERVER['HTTP_HOST'];
        $lang = 'ru';
		
		$db = call_db(HOST, USER, PASSWORD, DBNAME);
		$query_news="SELECT news_id, rewrite_url FROM news WHERE  
					(news_archive is NULL OR news_archive='0')
					 AND news_blog = 1  AND
					( news_header_".$lang." is not null AND LENGTH(TRIM(news_header_".$lang."))>2  ) AND 
					( news_brief_".$lang." is not null AND LENGTH(TRIM(news_brief_".$lang."))>2  ) ";
		
		$albums_array = select_albums_aray_core( 'ru' );
		
		$query_cat="SELECT category_id as local_id, category_menu_id, category_type_id, 
							category_page_id, rewrite_url, rewrite_path, type_name, type_php 
					FROM category 
					LEFT JOIN type on category.category_type_id = type.type_id 
					WHERE category_menu_id = '1' and category_hidden = '0' and category_parentcategory_id is NULL ";
        // create object
        $sitemap = new SitemapGenerator("http://".$site."/", "");

        // will create also compressed (gzipped) sitemap
        $sitemap->createGZipFile = true;

        // determine how many urls should be put into one file
        $sitemap->maxURLsPerSitemap = 10000;

        // sitemap file name
        $sitemap->sitemapFileName = "sitemap.xml";

        // sitemap index file name
        $sitemap->sitemapIndexFileName = "sitemap_index.xml";

        // robots file name
        $sitemap->robotsFileName = "robots.txt";
		
		$urls[]= array("http://".$site."",date('c'),'daily','1');
		$result_cat = mysqli_query($GLOBALS['db'],$query_cat);
		if(mysqli_num_rows($result_cat))
		{
			while ($row=mysqli_fetch_array($result_cat))
			{ 
				$type=$row['type_php'];//тип страницы
				if($type == 'index')
					continue;
				$url = $row['rewrite_url'];
				$url = $type.'.php?cat='.$row['local_id'];
				$urls[]= array("http://".$site.'/'.$url."",date('c'),'daily','0.5');
			}
		}//add all pages
				
		$result_news = mysqli_query($GLOBALS['db'],$query_news);
		if(mysqli_num_rows($result_news))
		{
			$news_page_id = select_cat_id_by_type_core('news');
			while ($row=mysqli_fetch_array($result_news))
			{
				$url = $row['rewrite_url'];
				$url = 'single_news.php?cat='.$news_page_id.'&news_id='.$row['news_id'];
				$urls[]= array("http://".$site.'/'.$url."",date('c'),'daily','0.5');
			}
		}//add all news
		
		$gall_page_id = select_cat_id_by_type_core('gallery');
		foreach( $albums_array as $key => $value )		
		{			
			$url = $row['rewrite_url'];
			$url = 'album.php?cat='.$gall_page_id.'&album_id='.$key ; // 'single_news.php?cat='.$news_page_id.'&news_id='.$row['news_id'];
			$urls[]= array("http://".$site.'/'.$url."",date('c'),'daily','0.5');
			
		}//add all news
		
        // add many URLs at one time
        $sitemap->addUrls($urls);

       // try {
            // create sitemap
            $sitemap->createSitemap();

            // write sitemap as file
            $sitemap->writeSitemap();

            // update robots.txt file
            $sitemap->updateRobots();

            // submit sitemaps to search engines
            $result = $sitemap->submitSitemap("yahooAppId");
            // shows each search engine submitting status
            /*echo "<pre>";
            print_r($result);
            echo "</pre>";*/
            
     //   }
        /*catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

        echo "Memory peak usage: ".number_format(memory_get_peak_usage()/(1024*1024),2)."MB";
        $time2 = explode(" ",microtime());
        $time2 = $time2[1];
        echo "<br>Execution time: ".number_format($time2-$time)."s";*/
}


function cat_id_by_type($type_id)
{
//category.category_name_".$lang." as cat_name ,
	$sql_query= "select 
					category.category_id as cat_id,
					rewrite_url, rewrite_path
					type_name, type_php 
				from category 
				LEFT JOIN  type  on category.category_type_id = type.type_id 
				where type.type_php = '".$type_id."'" ;
	/*if ($parent_type == 7)*/
		//echo ($sql_query) 	;	
	$res_q=mysqli_query($GLOBALS['db'],$sql_query);
	$row = mysqli_fetch_array($res_q);
	
	//$cat_name = mb_strtoupper( trim($row['cat_name']), 'UTF-8') ;
	
	//$link = (( isset($row['rewrite_url']) &&  $row['rewrite_url'] != '' )? $row['rewrite_url'] : "");
	//$link = $row['type_php'].'.php?cat='.$row['cat_id'];
	return 	$row['cat_id'];		
}
#######################################################
function get_cbr_xml($file_rt,$day){
	$tmp_file='xml/tmp.xml';//исправил val
	$fp = fopen($tmp_file, 'w');
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,'http://www.cbr.ru/scripts/XML_daily.asp?date_req='.$day);
	curl_setopt($ch, CURLOPT_FILE, $fp);
	curl_exec($ch);
	fclose($fp);
	curl_close ($ch);
    if(!filesize($tmp_file)){
        $contents=@file_get_contents('http://www.cbr.ru/scripts/XML_daily.asp?date_req='.$day);
        if($contents)file_put_contents($tmp_file,$contents);
    } else{
    	$contents=@file_get_contents($tmp_file);
    	if($contents)file_put_contents($file_rt,$contents);
    }
}
#######################################################
function parse_xml_exch($xml,$json=1,$scur=array('BYR','USD','EUR','UAH','KZT','CHF','JPY')){
    if(filesize($xml)){
	$file = simplexml_load_file($xml);
	$curr = array();
	foreach ($file AS $el){
		if (in_array($el->CharCode, $scur)) {
			$curr[strval($el->CharCode)] = strval(str_replace(',', '.',$el->Value));
			}
	}
	if($json){return json_encode($curr);}
	else{return $curr;}
    }else{
        return 0;//курс временно недоступен!
    }
}
##########################################################


function get_cat_id_by_type($type_id){
	$select_category = "select category_id from category where category_type_id = '".$type_id."'";
	//echo("<br>".$select_category. "<br>");
	$result_category = mysqli_query($GLOBALS['db'],$select_category) or die (mysqli_error($GLOBALS['db']));
	if(0 == mysqli_num_rows ($result_category))	{
		echo ("Неверно определили идентификатор id  категории для типа= ". $type_id ."! <br> ");
		exit;
	}
	$rs = mysqli_fetch_object($result_category);
	return $rs->category_id;
}

function edit_rewrite_url($rewrite_url){
	if (check_rewrite_url($rewrite_url) == false){
		return $rewrite_url;
	} else {
		return edit_rewrite_url($rewrite_url."_");
	}
}

function edit_rewrite_url_update($rewrite_url , $id , $key){
	if (check_rewrite_url_update($rewrite_url, $id , $key) == false){
		return $rewrite_url;
	} else {
		return edit_rewrite_url_update($rewrite_url."_", $id , $key);
	}
}

function check_rewrite_url_update($rewrite_url , $id , $key)
{
	$select_category = "select category_id from category where rewrite_url = '".$rewrite_url."'";	
	if($key == 1) 
		$select_category .= " and category_id != '".$id."'";//echo("<br>".$select_category. "<br>");
	$result_category = mysqli_query($GLOBALS['db'],$select_category) or die (mysqli_error($GLOBALS['db']));
	if(0 != mysqli_num_rows ($result_category))	
		return true;
		
	$select_category = "select albums_id from albums where rewrite_url = '".$rewrite_url."'";
	if($key == 2 ) 
		$select_category .= " and albums_id != '".$id."'";//echo("<br>".$select_category. "<br>");
	$result_category = mysqli_query($GLOBALS['db'],$select_category) or die (mysqli_error($GLOBALS['db']));
	if(0 != mysqli_num_rows ($result_category))
		return true;
		
	$select_category = "select news_id from news where rewrite_url = '".$rewrite_url."'";
	if($key == 3 ) 
		$select_category .= " and news_id != '".$id."'";//echo("<br>".$select_category. "<br>");
	$result_category = mysqli_query($GLOBALS['db'],$select_category) or die (mysqli_error($GLOBALS['db']));
	if(0 != mysqli_num_rows ($result_category))
		return true;	
	
	$select_category = "select news_id from service where rewrite_url = '".$rewrite_url."'";
	if($key == 4 ) 
		$select_category .= " and news_id != '".$id."'";//echo("<br>".$select_category. "<br>");
	$result_category = mysqli_query($GLOBALS['db'],$select_category) or die (mysqli_error($GLOBALS['db']));
	if(0 != mysqli_num_rows ($result_category))
		return true;	
	
	$select_category = "select news_id from projects where rewrite_url = '".$rewrite_url."'";
	if($key == 5 ) 
		$select_category .= " and news_id != '".$id."'";//echo("<br>".$select_category. "<br>");
	$result_category = mysqli_query($GLOBALS['db'],$select_category) or die (mysqli_error($GLOBALS['db']));
	if(0 != mysqli_num_rows ($result_category))
		return true;	
	return false;
}

function insert_news_tags($news_tags , $news_id , $tags_news , $tags_albums,  $tags_project , $tags_service,   $table_name){
	$brief_elements = explode(',',$news_tags);
	$num_words=count($brief_elements);
	/*echo("<pre>");
	print_r($brief_elements );
	echo("</pre>");*/
	$tag_dictionary = array();
	$select_tag = " SELECT  tags_id , tags_name FROM tags 
					WHERE tags_news = '".$tags_news."' and  tags_project ='".$tags_project."' 
					and tags_album ='".$tags_albums."' and tags_service ='".$tags_service."'"; // tags_name '".$value."'";					   
	//echo("<br>".$select_tag. "<br>");
	$result_tag = mysqli_query($GLOBALS['db'],$select_tag) or die (mysqli_error($GLOBALS['db']));
	while ($rows_type = mysqli_fetch_assoc($result_tag)) {
		$tag_dictionary[$rows_type['tags_name']] = $rows_type['tags_id'];
    }
	/*echo("<pre>");
	print_r($tag_dictionary );
	echo("</pre>");*/
	foreach ($brief_elements as $key => $value)	{	
		//echo("<br>элемент массива из ввода ключ:".$key. " , значение: ".$value ."<br>");
		$value = mb_strtolower(trim($value), 'UTF-8');	      // В нижний регистр
		$value = preg_replace('/\s{2,}/', ' ', $value);  // Удаляем лишние символы
		//$value = preg_replace("/[,-]/ui", " ", $value);         // Заменяем на пробелы
		//$value = preg_replace("/[_]+/ui", " ", $value);         // Заменяем 1 и более пробелов на "-"	
		if ($value == ""){continue;}
		//echo("<br>!элемент массива из ввода ключ:".$key. " , значение: ".$value ."<br>");
		$tag_id = -1;
		if(!isset($tag_dictionary[$value]) ){
			$insert_tag = "insert into tags  
							set 
							tags_name = '" . $value."' , 
							tags_news = '".$tags_news."', 
							tags_project ='".$tags_project."', 
							tags_service ='".$tags_service."', 
							tags_album ='".$tags_albums."'";
			mysqli_query($GLOBALS['db'],$insert_tag) or die (mysqli_error($GLOBALS['db']));
			///echo("</br> добавим новый тэг в словарь тэгов:".$insert_tag . "</br>" ) ; 
			$tag_id = mysqli_insert_id($GLOBALS['db']);
		} else {
			$tag_id = $tag_dictionary[$value];
		}
		
		$select_tag = " SELECT news_tags_id , news_tags_news_id , news_tags_tags_id 
						FROM ".$table_name." 
						INNER JOIN  tags ON tags.tags_id =  ".$table_name.".news_tags_tags_id 
						WHERE 
						news_tags_news_id = '" . $news_id."' and  
						news_tags_tags_id = '" . $tag_id."'"; // tags_name '".$value."'";
		//echo("</br> Проверим нужно ли добавлять в смежную таблицу :".$select_tag . "</br>" ) ; 
		$result_tag = mysqli_query($GLOBALS['db'],$select_tag) or die (mysqli_error($GLOBALS['db']));
		if(0 == mysqli_num_rows ($result_tag)){
			$insert_tag_news = "insert into  ".$table_name." set news_tags_news_id = '" . $news_id."', news_tags_tags_id = '" . $tag_id."'";
			mysqli_query($GLOBALS['db'],$insert_tag_news) or die (mysqli_error($GLOBALS['db']));
			//echo("</br> добавляем в смежную таблицу :".$insert_tag_news . "</br>" ) ; 
		}
	}
}

function update_news_tags( $news_tags , $news_id  , $tags_news , $tags_albums,  $tags_project, $tags_service,  $table_name)
{
	$brief_elements = explode(',',$news_tags);
	$num_words=count($brief_elements);
	/*echo("<pre>");
	print_r($brief_elements );
	echo("</pre>");*/
	$tag_dictionary = array();
	$select_tag = " SELECT  tags_id , tags_name 
					FROM tags 
					WHERE 
					tags_news = '".$tags_news."' and  
					tags_project ='".$tags_project."' and 
					tags_service ='".$tags_service."' and 
					tags_album ='".$tags_albums."'"; // tags_name '".$value."'";
	//echo("<br>".$select_tag. "<br>");
	$result_tag = mysqli_query($GLOBALS['db'],$select_tag) or die (mysqli_error($GLOBALS['db']));
	while ($rows_type = mysqli_fetch_assoc($result_tag)) {
		$tag_dictionary[$rows_type['tags_name']] = $rows_type['tags_id'];
    }
	/*echo("<pre>");
	print_r($tag_dictionary );
	echo("</pre>");*/
	foreach ($brief_elements as $key => $value)	{
		//echo("<br>элемент массива из ввода ключ:".$key. " , значение: ".$value ."<br>");
		$value = mb_strtolower(trim($value), 'UTF-8');	      // В нижний регистр
		$value = preg_replace('/\s{2,}/', ' ', $value);  // Удаляем лишние символы
		//$value = preg_replace("/[,-]/ui", " ", $value);         // Заменяем на пробелы
	    //$value = preg_replace("/[_]+/ui", " ", $value);         // Заменяем 1 и более пробелов на "-"	
		if ($value == ""){continue;}
		//echo("<br>!элемент массива из ввода ключ:".$key. " , значение: ".$value ."<br>");
		$tag_id = -1;
		if( !isset($tag_dictionary[$value]) ){
			$insert_tag = " insert into tags 
							set tags_name = '" . $value."', 
							tags_news = '".$tags_news."', 
							tags_project = '".$tags_project."', 
							tags_service = '".$tags_service."', 
							tags_album = '".$tags_albums."'";;
			mysqli_query($GLOBALS['db'],$insert_tag) or die (mysqli_error($GLOBALS['db']));
			//echo("</br> добавим новый тэг в словарь тэгов:".$insert_tag . "</br>" ) ; 
			$tag_id = mysqli_insert_id($GLOBALS['db']);
		} else {
			$tag_id = $tag_dictionary[$value];
		}
		$select_tag = " SELECT 
								news_tags_id , news_tags_news_id , news_tags_tags_id 
							FROM 
								 ".$table_name."
							INNER JOIN  tags ON tags.tags_id =  ".$table_name.".news_tags_tags_id 
							WHERE 
								news_tags_news_id = '" . $news_id."' and  
								news_tags_tags_id = '" . $tag_id."'"; // tags_name '".$value."'";
								
		//echo("</br> проверим нужно ли добавлять в смежную таблицу :".$select_tag . "</br>" ) ; 
		$result_tag = mysqli_query($GLOBALS['db'],$select_tag) or die (mysqli_error($GLOBALS['db']));
			
		if(0 == mysqli_num_rows ($result_tag))
		{
			$insert_tag_news = "insert into  ".$table_name."  
							set news_tags_news_id = '" . $news_id."' , 
							news_tags_tags_id = '" . $tag_id."'";
				mysqli_query($GLOBALS['db'],$insert_tag_news) or die (mysqli_error($GLOBALS['db']));
				//echo("</br> добавляем в смежную таблицу :".$insert_tag_news . "</br>" ) ; 
		}
		
		
	}
	
	//echo("<br> ! ВСЕ ДОБАВИЛИ ТЕПЕРЬ ПЫТАЕМСЯ УДАЛИТЬ ЛИШНЕЕ<br>");
	
	$select_tag = " SELECT 
								news_tags_id , news_tags_news_id , news_tags_tags_id , tags_name, tags_id
							FROM 
								 ".$table_name."
							INNER JOIN  tags ON tags.tags_id =  ".$table_name.".news_tags_tags_id 
							WHERE 
								news_tags_news_id = '" . $news_id."'"; 
	$news_tag = array();
	$result_tag = mysqli_query($GLOBALS['db'],$select_tag) or die (mysqli_error($GLOBALS['db']));
		
	while ($rows_type = mysqli_fetch_assoc($result_tag))  
    {
		$news_tag[$rows_type['tags_id']] = $rows_type['tags_name'];
    }
	
	//echo("<br> то что лежит в базе для этой новости <br>");
	/*echo("<pre>");
	print_r($news_tag );
	echo("</pre>");
	
	
	echo("<br> то что ввели  - brief_elements<br>");
	echo("<pre>");
	print_r($brief_elements );
	echo("</pre>");*/

	foreach ($news_tag as $key => $value) // выбрали все тэги для этой новости 
	{
		//echo("<br> ищем значение ".$value." в brief_elements <br>");
		
		//!in_array ($key, $brief_elements)
		//array_search(strtolower($key),array_map('strtolower',$brief_elements)) == false 
		//_value_in_array($brief_elements,$key )
		if( !_value_in_array($brief_elements,$value ) )// и если среди введенных значений такого тэга нет то его следует удалить
		{
			
			$insert_tag_news = "delete from  ".$table_name."  
							    where  news_tags_tags_id = '" . $key."' and news_tags_news_id = '" . $news_id."'";
			//echo("</br> мы собрались что то удалять  :".$insert_tag_news . "</br>" ) ; 
			//echo("</br> решили удалять :".$insert_tag_news . "</br>" ) ; 
			mysqli_query($GLOBALS['db'],$insert_tag_news) or die (mysqli_error($GLOBALS['db']));
		}
	}
	
	
	///**на всякий случай удалим теги из справочника которые ни к чему не привязаны **//
	$select_tag = " SELECT  tags_id , tags_name , 
					(SELECT count(news_tags_id) FROM  ".$table_name." WHERE news_tags_tags_id = tags_id  ) as num
				    FROM tags WHERE 
						tags_news = '".$tags_news."' and  
						tags_project ='".$tags_project."' and 
						tags_service ='".$tags_service."' and 
						tags_album ='".$tags_albums."'"; // tags_name '".$value."'";
				   
	
					   
	//echo("<br>".$select_tag. "<br>");
	$result_tag = mysqli_query($GLOBALS['db'],$select_tag) or die (mysqli_error($GLOBALS['db']));
		
	while ($rows_type = mysqli_fetch_assoc($result_tag))  
    {
		if( $rows_type['num'] == 0 )
		{
			$insert_tag_news = "delete from tags  
							    where  tags_id = '" . $rows_type['tags_id']."'";
		//	echo("</br> мы собрались что то удалять  :".$insert_tag_news . "</br>" ) ; 
			//echo("</br> решили удалять :".$insert_tag_news . "</br>" ) ; 
			mysqli_query($GLOBALS['db'],$insert_tag_news) or die (mysqli_error($GLOBALS['db']));
		}
		
    }
	
	

}


function _value_in_array($array, $find)
{
	$exists = FALSE;
	if(!is_array($array))
	{
		return;
	}
	
	foreach ($array as $key => $value) 
	{
		if(trim($find) == trim($value))
		{
			$exists = TRUE;
		}
	}
	return $exists;
}
function select_news_tags_core( $news_id) // , $special
{
	$sql_query="SELECT news_tags.news_tags_id, news_tags.news_tags_news_id, news_tags.news_tags_tags_id  , tags.tags_name as name, tags.tags_id as id 
					FROM news_tags
					INNER JOIN tags on tags.tags_id = news_tags.news_tags_tags_id
					INNER  JOIN news on news.news_id = news_tags.news_tags_news_id
					WHERE tags.tags_news = 1  AND tags.tags_project = 0 AND 
					 tags.tags_album = 0 AND tags.tags_service = 0  "; // news.news_special= ".$special." AND
	if ( $news_id > 0 )				
		$sql_query .= "	AND news_tags_news_id ='".$news_id."'";
	
	//echo "</br>". $sql_query . "</br>";
	$res_q=mysqli_query($GLOBALS['db'],$sql_query);
	$tags_list = array();
	while ($tags_row = mysqli_fetch_array($res_q)) 
	{
			/*echo('<pre>');
			print_r($tags_row);
			echo('<pre>');*/
		if( !isset( $tags_list[$tags_row['id']]) )			
			$tags_list[$tags_row['id']] = $tags_row['name'];
	}
	return $tags_list;
}
function check_rewrite_url($rewrite_url)
{
	$select_category = "select category_id 
					    from category 			 
						where rewrite_url = '".$rewrite_url."'";
	//echo("<br>".$select_category. "<br>");
	$result_category = mysqli_query($GLOBALS['db'],$select_category) or die (mysqli_error($GLOBALS['db']));
	if(0 != mysqli_num_rows ($result_category))
		return true;
		
	$select_category = "select albums_id
					    from albums 			 
						where rewrite_url = '".$rewrite_url."'";
	//echo("<br>".$select_category. "<br>");
	$result_category = mysqli_query($GLOBALS['db'],$select_category) or die (mysqli_error($GLOBALS['db']));
	if(0 != mysqli_num_rows ($result_category))
		return true;
		
	$select_category = "select news_id
					    from news 			 
						where rewrite_url = '".$rewrite_url."'";
	//echo("<br>".$select_category. "<br>");
	$result_category = mysqli_query($GLOBALS['db'],$select_category) or die (mysqli_error($GLOBALS['db']));
	if(0 != mysqli_num_rows ($result_category))
		return true;
		
	return false;
}

function get_parent_category($cat_id, $news_name='',  $flag = true,  $result_array=array())
{
	//	echo("язык = ".$lang);
		$query_part = 	"SELECT category_id, rewrite_url, category_page_id, 
								category_parentcategory_id, category_type_id,
								type_name, type_php , category_icon
						FROM category 
						LEFT JOIN  type  on category.category_type_id = type.type_id 
						WHERE category_id=".$cat_id;
		//echo("<br>". $query_part);
		$res=mysqli_query($GLOBALS['db'],$query_part) or die (mysqli_error($GLOBALS['db']));
		$r=mysqli_fetch_array($res);
		
		if(!isset($_SESSION['quit'])){$_SESSION['quit']=0;}
	
		
		
		if($news_name != ''){$flag = false;	}
		
		if($_SESSION['quit']>1)
		{
			$news_name ='';
		}
		
		if(isset($r['rewrite_url'])&& $r['rewrite_url'] != '')
		{
			//echo("123");
			$curr_header = html_entity_decode(stripslashes($r['rewrite_url'])).(($news_name != '')?'/'.$news_name:'');
		}
		else
		{
			$curr_header = (($news_name != '')?$news_name:'');
		}
		
		if($r['category_parentcategory_id'] != 0)
		{	
			
			$result_array = get_parent_category($r['category_parentcategory_id'],'',  false , $result_array);
			
			if(isset($curr_header) && $curr_header != '')
			{
				if(isset($result_array["title"] ) && $result_array["title"]  != '')
					$result_array["title"] =     $result_array["title"] .'/'. $curr_header;
				else
					$result_array["title"] = $curr_header;
			}
			
		}
		else
		{
			
			if(isset($curr_header) && $curr_header != '')
			{
				if(isset($result_array["title"] ) && $result_array["title"]  != '')
					$result_array["title"] =     $result_array["title"] .'/'. $curr_header;
				else
					$result_array["title"] = $curr_header;
			}
			
			unset($_SESSION['quit']);
			return $result_array;
		}
		return $result_array;
}


function htaccess_del_line($alias_url,$real_url){
	$file = $_SERVER['DOCUMENT_ROOT'] .'/.htaccess';
	if (file_exists($file)&&(isset($alias_url,$real_url))){
		$rule="RewriteRule ^".$alias_url."$ /".$real_url." [L]\n";
		$contents = file_get_contents($file);
		$contents = str_replace($rule, '', $contents);
		file_put_contents($file, $contents);
	}
}


function htaccess_del_allines($alias_url)
{
	$file = $_SERVER['DOCUMENT_ROOT'] .'/.htaccess';
	if (file_exists($file)&&(isset($alias_url)))
	{
		$file_out = file($file); // Считываем весь файл в массив
		foreach ($file_out as $key => $value)
		{
			if(strpos($value, "RewriteRule ^".$alias_url) !== false) // если существует - удалеям всю строку
			{
				unset($file_out[$key]);
			}
		}
		file_put_contents($file, implode("", $file_out));
	}
	
	

}

function htaccess_add_line($alias_url,$real_url){
	$file = $_SERVER['DOCUMENT_ROOT'] .'/.htaccess';
	if (file_exists($file)&&(isset($alias_url,$real_url))){
		$rule="RewriteRule ^".$alias_url."$ /".$real_url." [L]\n";
		$contents = file_get_contents($file);
		if (strpos($contents, $rule) === false) {
			$f = fopen($file, "a+");
			fwrite($f,$rule);
			fclose($f);
		}
		else{ echo 'запись уже есть!';}
	}
}


 function translit($to_url) {
		
    $trans = array('А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 
                   'Е' => 'E', 'Ё' => 'Jo', 'Ж' => 'Zh', 'З' => 'Z', 'И' => 'I', 
                   'Й' => 'J', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 
                   'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 
                   'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C', 'Ч' => 'Ch', 
                   'Ш' => 'Sh', 'Щ' => 'Shh', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '', 
                   'Э' => 'Je', 'Ю' => 'Ju', 'Я' => 'Ja',
					
                   'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 
                   'е' => 'e', 'ё' => 'jo', 'ж' => 'zh', 'з' => 'z', 'и' => 'i', 
                   'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 
                   'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 
                   'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch', 
                   'ш' => 'sh', 'щ' => 'shh', 'ъ' => '', 'ы' => 'y', 'ь' => '', 
                   'э' => 'je', 'ю' => 'ju', 'я' => 'ja');
					
    $url = strtr($to_url, $trans);    // Заменяем кириллицу согласно массиву замены
    $url = mb_strtolower($url);	      // В нижний регистр
	
    $url = preg_replace("/[^a-z0-9-s,]/i", " ", $url);  // Удаляем лишние символы
    $url = preg_replace("/[,-]/ui", " ", $url);         // Заменяем на пробелы
    $url = preg_replace("/[_]+/ui", "_", $url);         // Заменяем 1 и более пробелов на "-"	
		
    return $url;
}


/*---Функция отправки декодированной почты---*/
function send_mime_mail_a($name_from, // имя отправителя
                        $email_from, // email отправителя
                        $name_to, // массив имен получателя
                        $email_to, // массив email-адресов получателя
                        $data_charset, // кодировка переданных данных
                        $send_charset, // кодировка письма
                        $subject, // тема письма
                        $body // текст письма
						//$bound
                        ) {
  $to = '';
  
  for($i=0;$i<count($name_to);$i++) {
    $to .= mime_header_encode($name_to[$i], $data_charset, $send_charset)  . ' <' . $email_to[$i] . '>' . ( ($i<count($name_to)-1)?', ':'');
  }
  $subject = mime_header_encode($subject, $data_charset, $send_charset);
  $from =  mime_header_encode($name_from, $data_charset, $send_charset).' <' . $email_from . '>';
  //$body="--$bound\n".$body;
  if($data_charset != $send_charset) {
    $body = iconv($data_charset, $send_charset, $body);
  }
  $headers = "From: ".$from."\r\n";
  //$headers .= "Content-Type: multipart/alternative; boundary=$bound \r\n";
  $headers .= "Content-type: text/html; charset=".$send_charset."\r\n";
  $headers .= "Mime-Version: 1.0\r\n";
$mail_sent=mail($to, $subject, $body, $headers);
if($mail_sent){$echo='Данные успешно отправлены!';}
else{$echo='Ошибка отправки данных!';}
return $echo;
}

/*---Функция декодирования заголовков письма---*/
function mime_header_encode($str, $data_charset, $send_charset) {
  if($data_charset != $send_charset) {
    $str = iconv($data_charset, $send_charset, $str);
  }
  return '=?' . $send_charset . '?B?' . base64_encode($str) . '?=';
}

/*---Генерация кода капчи---*/
function generate_code() //генерируем код
{
                
    $hours = date("H"); // час       
    $minuts = substr(date("H"), 0 , 1);// минута 
    $mouns = date("m");    // месяц             
    $year_day = date("z"); // день в году
    $str = $hours . $minuts . $mouns . $year_day; //создаем строку
    $str = md5(md5($str)); //дважды шифруем в md5
	$str = strrev($str);// реверс строки
	$str = substr($str,2, 7); // извлекаем 7 символов, начиная с 2
	
	
    $array_mix = preg_split('//', $str, -1, PREG_SPLIT_NO_EMPTY);
    srand ((float)microtime()*1000000);
    shuffle ($array_mix);
	//Тщательно перемешиваем, соль
    return implode("", $array_mix);
}

/*---Функция проверки кода---*/
function check_code($code) //проверяем код
{
    $code = trim($code);//удаляем пробелы
    $array_mix = preg_split ('//', generate_code(), -1, PREG_SPLIT_NO_EMPTY);
    $m_code = preg_split ('//', $code, -1, PREG_SPLIT_NO_EMPTY);
    $result = array_intersect ($array_mix, $m_code);
if (strlen(generate_code())!=strlen($code))
{
    return FALSE;
}
if (sizeof($result) == sizeof($array_mix))
{
    return TRUE;
}
else
{
    return FALSE;
}
}
/*--------------Декодирование символов УРЛ строки-----------*/
function editor_urlencode($text) {
  if ($text!='') {
    return str_replace(array('%2F', '%3F', '%3D', '%26', '%2523','%20'),
                       array('/', '?', '=', '&', '#',' '),
                      $text);
  }
  else {
    return str_replace('%2F', '/', rawurlencode($text));
  }
}
	
/*--------------Пересчёт секунд в часы и дни----------------*/
function Sec2Time($time)
{
  if(is_numeric($time))
  {
    $value = array(
      "years" => 0, "days" => 0, "hours" => 0,
      "minutes" => 0, "seconds" => 0,
    );
    if($time >= 31556926){
      $value["years"] = floor($time/31556926);
      $time = ($time%31556926);
    }
    if($time >= 86400){
      $value["days"] = floor($time/86400);
      $time = ($time%86400);
    }
    if($time >= 3600){
      $value["hours"] = floor($time/3600);
      $time = ($time%3600);
    }
    if($time >= 60){
      $value["minutes"] = floor($time/60);
      $time = ($time%60);
    }
    $value["seconds"] = floor($time);
    return (array) $value;
  }
  else
  {
    return (bool) FALSE;
  }
}

 /*--------------Загрузка файла на сервер----------------*/	
function downloadFile($file){
        $file_name = $file;
        $mime = 'application/force-download';
    header('Pragma: public');   
    header('Expires: 0');       
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Cache-Control: private',false);
    header('Content-Type: '.$mime);
    header('Content-Disposition: attachment; filename="'.basename($file_name).'"');
    header('Content-Transfer-Encoding: binary');
    header('Connection: close');
    readfile($file_name);  
    exit();
}
	
/*==============Функция конвертации даты из формата MYSQL в нормальный==================*/
function conv_date($dt)
{
       preg_match("|([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})\s([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})|",$dt,$out);
	return "$out[3].$out[2].$out[1]";
}

 /*==============Функция конвертации даты из формата MYSQL в русский формат==================*/
function conv_date_to_rus($dt)
{
    preg_match("|([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})\s([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})|",$dt,$out);
	$timestamp=mktime($out[4],$out[5],$out[6],$out[2],$out[3],$out[1]);
	$today=date("H:i",$timestamp);
	$date=explode(".", date("l.d.m.Y",$timestamp));
	switch ($date[0])
	{		
		case 'Monday':$d='Понедельник';break;
		case 'Tuesday':$d='Вторник';break;
		case 'Wednesday':$d='Среда';break;
		case 'Thursday':$d='Четверг';break;
		case 'Friday':$d='Пятница';break;
		case 'Saturday':$d='Суббота';break;
		case 'Sunday':$d='Воскресенье';break;
	}
	switch ($date[2])
	{
		case 1: $m='Января'; break;
		case 2: $m='Февраля'; break;
		case 3: $m='Марта'; break;
		case 4: $m='Апреля'; break;
		case 5: $m='Мая'; break;
		case 6: $m='Июня'; break;
		case 7: $m='Июля'; break;
		case 8: $m='Августа'; break;
		case 9: $m='Сентября'; break;
		case 10: $m='Октября'; break;
		case 11: $m='Ноября'; break;
		case 12: $m='Декабря'; break;
	}
	echo $d.',&nbsp;'.$date[1].'&nbsp;'.$m.'&nbsp;'.$date[3].',&nbsp;'.$today;
	
} 

/*===============Проверка правильности даты===================*/
function chk_err_date_time($date)
{	
	htmlspecialchars(trim($date));
	$err=0;
	preg_match("|([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})\s([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})|",$date,$out);//разбиение даты и времени
	if(checkdate($out[2],$out[3],$out[1])){
		if($out[4]>23 || $out[4]<0){ $err='Неправильный формат часов!'; return $err;}
		if($out[5]>59 || $out[5]<0){ $err='Неправильный формат минут!'; return $err;}
		if($out[6]>59 || $out[6]<0){ $err='Неправильный формат секунд!'; return $err;}
		}else{$err='Неправильный формат даты!'; return $err;}
	return $err;
}
/*==============Ограничение количества  слов в строке=====================*/
function lim_word($strg,$nw)
{
	 $brief_elements = explode(' ',$strg);
	 $num_words=count($brief_elements);
	 $str_brief_result = '';
	 if ($nw>=$num_words)
	 { 
		$nw=$num_words-1;
	 }
	 else 
	 {
		$nw=$nw-1;
	 }
	 for($i = 0; $i <= $nw; $i++)
	 {
		$str_brief_result = $str_brief_result.$brief_elements[$i].' ';
	 
	 }
	 while($str_brief_result{strlen($str_brief_result)-1}==' ')
	 {
	    $str_brief_result=trim($str_brief_result);
	 }
	   $last=$str_brief_result{strlen($str_brief_result)-1};
	  /* $end='<img src="im/ae.png" width="7" height="7" />';//Картинка вместо многоточия*/
		 $end='...';
	   switch ($last)
	   {
		   case '.':return $str_brief_result.$end;
		   case '!':return $str_brief_result.$end;
		   case '?':return $str_brief_result.$end;
		   case '`':$str_brief_result{strlen($str_brief_result)-1}=' ';return($str_brief_result.$end);
		   case "'":$str_brief_result{strlen($str_brief_result)-1}=' ';return($str_brief_result.$end);
		   case '+':$str_brief_result{strlen($str_brief_result)-1}=' ';return($str_brief_result.$end);
		   case '-':$str_brief_result{strlen($str_brief_result)-1}=' ';return($str_brief_result.$end);
		   case ':':$str_brief_result{strlen($str_brief_result)-1}=' ';return($str_brief_result.$end);
		   case ';':$str_brief_result{strlen($str_brief_result)-1}=' ';return($str_brief_result.$end);
		   case ',':$str_brief_result{strlen($str_brief_result)-1}=' ';return($str_brief_result.$end);
		   default:return($str_brief_result.$end);
	   }
}

/*==============Считает число слов в блоке текста и сравнивает с лимитом====================*/
function count_symbols_lim($strg,$lim)
{
	$brief_elements = explode(' ',$strg);
	$num_words=count($brief_elements);
	 for($j = 0; $j <= ($num_words-1); $j++){
	 $length=strlen($brief_elements[$i]);
	 if($length>$lim){$tm="true";}
	 }
	 return $tm;
}



/*==============Дерево для админки====================*/
	function get_tree($category_menu_id, $parent_id = "NULL", $counter = "")
	{
		$query ="SELECT category_id, category_name_ru, category_menu_id, category_type_id, category_page_id, category_parentcategory_id, category_hidden,  category_code, menu_name_ru, type_name, type_php,
		(case when category_page_id is NULL then 0 else 1 END) as num_page FROM category LEFT JOIN menu on category.category_menu_id  = menu.menu_id LEFT JOIN  type  on category.category_type_id = type.type_id 
		WHERE category_menu_id = '".$category_menu_id."' and category_parentcategory_id ";
		
		if($parent_id == "NULL"){
			$query .= "is NULL";
		} else {
			$query .= " ='".$parent_id."'";
		}
		
		$query.=" ORDER BY category_code  ASC, category.category_id ASC ";
		$result = mysqli_query($GLOBALS['db'],$query);
		
		if($parent_id == 0 &&  mysqli_num_rows( $result ) == 0 ) {
			echo '<div style="display:block; margin:15px 10px;"> Пока нет ни одного раздела. Добавьте разделы, пожалуйста !</div>';
		}
		
		if(mysqli_num_rows( $result )==0) {return;}
		
		$currCounter = 1;
		if($parent_id == 0){
			echo '<ul id="my-menu" class="sample-menu">';
		} else { 
			echo '<ul>';
		}
		
		while ($row = mysqli_fetch_array($result)) {
			$flag = true;
			if($row['num_page'] == 1 ) {
				$flag  = true;
			}
			
			$subcategoryquery = "select category_id	from category  where category_parentcategory_id ='".$row['category_id']."'";
			$result_subcategoryquery = mysqli_query($GLOBALS['db'], $subcategoryquery);
			
			if(mysqli_num_rows($result_subcategoryquery) > 0) {
				$flag  = false;
			}
		
			$subcategoryquery = "select albums_id, albums_category_id	from albums  where albums_category_id ='".$row['category_id']."'";
			$result_subcategoryquery = mysqli_query($GLOBALS['db'], $subcategoryquery);
			
			if(mysqli_num_rows($result_subcategoryquery) > 0) {
				$flag  = false;
			}
			
			if(isset($row['category_page_id']) && $row['category_page_id']!= 'NULL' && $row['category_page_id'] != 0 ) {
				$select_album_page = "select count(album_page_id) as num from album_page  where album_page_page_id ='".$row['category_page_id']."'";									  
				$result_select_album_page = mysqli_query($GLOBALS['db'], $select_album_page);
				$r_count_album = mysqli_fetch_object($result_select_album_page);
				
				if($r_count_album->num > 0)	{
					$flag  = false;
				}
			}
			
			$count_news =   "select count(news_id) as num from news LEFT JOIN page on news.news_page_id  = page.page_id	LEFT JOIN category on page.page_id   = category.category_page_id where category_id = '".$row['category_id']."'";
			$result_count_news = mysqli_query($GLOBALS['db'], $count_news);
			$r_count_news = mysqli_fetch_object($result_count_news);
			
			if($counter != "") {
				$currPrefix = $counter.".".$currCounter;
			} else {
				$currPrefix = $currCounter;
			}
			
			if($r_count_news->num > 0) {
				$flag  = false;
			}
			
			echo '<li><a href="#0">';
			
			/* Точно не нужно.
			if($category_menu_id  == 1){
				echo("<input type = 'hidden' name = 'old_val[".$row['category_id']."]'  value = '".
				( (isset($row['category_code']) && $row['category_code'] != 0) ? ( $row['category_code']):('') )."'>");
				
				$code_value = '';
				if ( isset($_POST['new_val'][$row['category_id']]) ) {
					$code_value = $_POST['new_val'][$row['category_id']];
				} else if( isset($row['category_code']) && $row['category_code'] != 0) {
					$code_value  = $row['category_code'];
				}
				
				echo $currPrefix.". ";
				echo("<input type='input' size='5' maxlength='4' name = 'new_val[".$row['category_id']."]' value = '".$code_value."'/>");
			}
			*/
			
			echo decode_cool($row['category_name_ru']);
			
			//echo ("<img src='img/addcat.png' name='add' class='tree_img' title='Добавить подкатегорию' alt='Добавить подкатегорию' onClick='AddAll(".$row['category_id'].");' />");
			//echo ("<img src='img/edit.png' name='edit' class='tree_img' title='Редактировать' alt='Редактировать' onClick='EditAll(".$row['category_id'].");' />");
			
			/*if($flag == true) {
				echo ("<img src='img/del.png' name = 'del' class='tree_img' title='Удалить' alt='Удалить' onClick='DelAll(".$row['category_id'].");' />");
			} else {
				echo ("<img src='img/no_delete.png' name='delete' class='tree_img' title='Удалить' alt='Удалить'/>");
			}*/
			
			if($row['category_hidden']) {
				echo(" (скрыта)");
			}
			
			/*
			if($category_menu_id  == 1)	{
				if(!$row['num_page']) {
					echo ("<img  src='img/add_page.png'  name = 'add_page' class='tree_img' title='Добавить Страницу' alt='Добавить Страницу' onClick='AddPage(".$row['category_id'].");'/>");
				}
				
				echo("<img src='img/add_album.png' name = 'view_albums' class='tree_img' alt='Присоединённые альбомы' title='Присоединённые альбомы' onClick='ViewAlbums(".$row['category_id'].");'/>");
				if($row['num_page'])
				{
					echo("<img src='img/edit_page.png' name = 'edit_page' class='tree_img' alt='Редактирование страницы' title='Редактирование страницы' onClick='EditPage(".$row['category_id'].");'/>");
					
					
					if($r_count_news->num > 0)
					{
						echo("<img src='img/view_news.png' name = 'add_news' class='tree_img' alt='Присоединённые новости' title='Присоединённые новости' onClick='ViewPage(".$row['category_id'].");'/>");
					}
					else
					{
						echo("<img src='img/add_news.png' name = 'view_page' class='tree_img' alt='Добавить новости' title='Добавить новости' onClick='ViewPage(".$row['category_id'].");'/>");
					}
					
				}
				echo("<img src='img/forw.png' name = 'view_page' class='tree_img' alt='Ключевые слова' title='Ключевые слова' onClick='ViewKeywords(".$row['category_id'].");'/>");
			}
			
			else if($category_menu_id  == 2)
			{
				echo("<img src='img/forw.png' name = 'add_servise'  class='tree_img' alt='Добавить альбомы' title='Добавить альбомы' onClick='AddServices(".$row['category_id'].");'/>");
			}
			
			*/	
				
				
				
				
			echo '</a>';
				get_tree($category_menu_id, $row['category_id'], $currPrefix);
			$currCounter += 1;
			
		}
		echo '</li>'; 
	}
	
	function select_max_category_code($parent)
	{
		$select_category = " SELECT category_code  
							 FROM  category 
							 where category_parentcategory_id ";
		if($parent == 0)//проверяем откуда начинать вывод (начало - 0)
		{
			$select_category .= " is NULL";//если с начала, то корневая директория - 0			
		}
		else
		{
			$select_category .= " ='".$parent."'";//или начинаем выввод от директории с номером parent_id
		}
						
		$select_category .= " order by category_code desc";
		//echo $select_category;
		$result = mysqli_query($GLOBALS['db'],$select_category) or die (mysqli_error($GLOBALS['db']));
		if(0 == mysqli_num_rows ($result))
			return 1;

		$rs = mysqli_fetch_object($result);
		return ((int)$rs->category_code)+1;
		
	}
	
	/*==============Дерево для админки старое====================*/
	function get_tree_old($category_menu_id, $parent_id = "NULL", $counter = "")
	{
		
		$query ="SELECT category_id, category_name_ru, category_menu_id, category_type_id,
						category_page_id, category_parentcategory_id, category_hidden,  category_code,
						menu_name_ru, type_name, type_php,
						(case when category_page_id is NULL then 0 else 1 END) as num_page 
					FROM category
					LEFT JOIN menu on category.category_menu_id  = menu.menu_id 
					LEFT JOIN  type  on category.category_type_id = type.type_id 
					WHERE category_menu_id = '".$category_menu_id."' and category_parentcategory_id ";
		if($parent_id == "NULL")
		{
			$query .= "is NULL";
		}
		else
		{
			$query .= " ='".$parent_id."'";
		}
		$query.=" ORDER BY category_code  ASC, category.category_id ASC ";
		$result = mysqli_query($GLOBALS['db'],$query);
		
		//echo($query);
		if($parent_id == 0 &&  mysqli_num_rows( $result )==0 )
		{
			echo '<div style="display:block; margin:15px 10px;"> Пока нет ни одного раздела. Добавьте разделы, пожалуйста !</div>';
		}
		if(mysqli_num_rows( $result )==0)
		{	
			
			return;
		}
		
		$currCounter = 1;
		if($parent_id == 0)
		{
			echo '<ul id="my-menu" class="sample-menu">';
		}
		else
		{ 
			echo '<ul>';
		}
		$curcat = 1 ;
		while ($row = mysqli_fetch_array($result)) 
		{
			
			
			$flag = true;
			if($row['num_page'] == 1 )
			{
				$flag  = true;//исправил VAL(теперь работает)
			}
			
			$subcategoryquery = "select category_id	from category  where category_parentcategory_id ='".$row['category_id']."'";
			$result_subcategoryquery = mysqli_query($GLOBALS['db'], $subcategoryquery);
			if(mysqli_num_rows($result_subcategoryquery)>0)
			{
				$flag  = false;
			}
		//	если есть страницы все равно волевым решением можно удальять и категорию и связанную с ней страницу
		/*	if(isset($row['category_page_id']) && $row['category_page_id']!= 'NULL' && $row['category_page_id'] != 0 )
			{
				$flag  = false;
			}*/
			$subcategoryquery = "select albums_id, albums_category_id	from albums  where albums_category_id ='".$row['category_id']."'";
			$result_subcategoryquery = mysqli_query($GLOBALS['db'], $subcategoryquery);
			if(mysqli_num_rows($result_subcategoryquery)>0)
			{
				$flag  = false;
			}
			
			if(isset($row['category_page_id']) && $row['category_page_id']!= 'NULL' && $row['category_page_id'] != 0 )
			{
				$select_album_page = "select count(album_page_id) as num
									  from album_page 
									  where album_page_page_id ='".$row['category_page_id']."'";
									  
				$result_select_album_page = mysqli_query($GLOBALS['db'], $select_album_page);
				$r_count_album = mysqli_fetch_object($result_select_album_page);
				if($r_count_album->num > 0)
				{
				//	echo('есть альбомы');
					$flag  = false;
				}
			}
			
			$count_news =   "select count(news_id) as num
							from news 
							LEFT JOIN page on news.news_page_id  = page.page_id
							LEFT JOIN category on page.page_id   = category.category_page_id
							where category_id = '".$row['category_id']."'";
							//echo($count_news);
			$result_count_news = mysqli_query($GLOBALS['db'], $count_news);
			$r_count_news = mysqli_fetch_object($result_count_news);
			
			if($counter!="")
			{
				$currPrefix = $counter.".".$currCounter;
			}
			else
			{
				$currPrefix = $currCounter;
			}
			
			if($r_count_news->num > 0)
			{
				$flag  = false;
			}
			echo '<li><a href="#0">';
			if($category_menu_id  == 1)
			{
				echo("<input type = 'hidden' name = 'old_val[".$row['category_id']."]'  value = '".
				( (isset($row['category_code']) && $row['category_code'] != 0) ? ( $row['category_code']):('') )."'>");
				
				$code_value = '';
				if ( isset($_POST['new_val'][$row['category_id']]) )
				{
					//echo("<br>" . $_POST['new_val'][$row['category_id']] . "<br>");
					$code_value = $_POST['new_val'][$row['category_id']];
				}
				else if( isset($row['category_code']) && $row['category_code'] != 0)
				{
					$code_value  = $row['category_code'];
				}
				
				
				echo $currPrefix.". ";
				echo("<input type='input' size='5' maxlength='4' name = 'new_val[".$row['category_id']."]'  
				value = '".$code_value."'/>");
			}
			
			echo decode_cool($row['category_name_ru']);
			if($category_menu_id  == 1)
			{
				echo ("<i class='fa fa-plus fa-green' title='Добавить подкатегорию' onClick='AddAll(".$row['category_id'].");'></i>");
			}
			echo ("<i class='fa fa-pencil-square-o fa-orange' title='Редактировать' onClick='EditAll(".$row['category_id'].");'></i>");
			if($flag == true)
			{
				echo ("<i class='fa fa-trash-o fa-red' title='Удалить' onClick='DelAll(".$row['category_id'].");'></i>");
			}
			else
			{
				echo ("<i class='fa fa-trash-o fa-dis' title='Нельзя удалить (есть вложенные категории)'></i>");
			}
			if($row['category_hidden'])
			{
				echo("<i class='fa fa-eye' title='Скрытый'></i>");
			}
			
			if($category_menu_id  == 1)
			{
				
			//	echo("<i class='fa fa-picture-o' title='Присоединить альбомы'  onClick='ViewAlbums(".$row['category_id'].");'></i>");
				if(isset($row['num_page']) && $row['num_page'] != '0' && $row['num_page'] != '')
				{
					echo("<i class='fa  fa-file-o' alt='Редактирование страницы' title='Редактирование страницы' onClick='EditPage(".$row['category_id'].");'></i>");
					echo("<i class='fa fa-trash-o fa-red' alt='Удаление страницы' title='Удаление страницы' onClick='DellPage(".$row['category_id'].");'></i>");
					
					if($r_count_news->num > 0)
					{
						//echo("<i class='fa fa-list' alt='Присоединённые новости' title='Присоединённые новости' onClick='ViewPage(".$row['category_id'].");'></i>");
					}
					else
					{
						//echo("<i class='fa fa-list' alt='Добавить новости' title='Добавить новости' onClick='ViewPage(".$row['category_id'].");'></i>");
					}
					
					/*if($category_menu_id  == 1)
					{
						echo("<img src='img/forw.png' name = 'view_page' class='tree_img' alt='Ключевые слова' title='Ключевые слова' onClick='ViewKeywords(".$row['category_id'].");'/>");
					}*/
				//echo($r_count_news->num);
				}
				else
				{
					echo ("<i class='fa fa-file-o' title='Добавить Страницу' alt='Добавить Страницу' onClick='AddPage(".$row['category_id'].");'></i>");
				}
				echo("<i class='fa fa-key' alt='Ключевые слова' title='Ключевые слова' onClick='ViewKeywords(".$row['category_id'].");'></i>");
			}
			
			else if($category_menu_id  == 2)
			{
				echo("<i class='fa fa-list' alt='Добавить альбомы' title='Добавить альбомы' onClick='AddServices(".$row['category_id'].");'></i>");
				
			}
			//}
			
			
			echo '</a>';
				get_tree_old($category_menu_id, $row['category_id'], $currPrefix);
			$currCounter += 1;
			$curcat +=1;
		}echo '</li>';
		echo '</ul>'; //по выходу из цикла
	}
	
	function get_tree_price($category_menu_id, $parent_id = "NULL", $counter = "")
	{
		
		$query ="SELECT category_id, category_name_ru, category_menu_id, category_type_id, category_price, 
						category_page_id, category_parentcategory_id, category_hidden,  category_code,
						menu_name_ru, type_name, type_php					 
					FROM category
					LEFT JOIN menu on category.category_menu_id  = menu.menu_id 
					LEFT JOIN  type  on category.category_type_id = type.type_id 
					WHERE category_menu_id = '3' and category_parentcategory_id ";
		if($parent_id == "NULL")
		{
			$query .= "is NULL";
		}
		else
		{
			$query .= " ='".$parent_id."'";
		}
		$query.=" ORDER BY category_code  ASC, category.category_id ASC ";
		$result = mysqli_query($GLOBALS['db'],$query);
		//echo("<br>". $query . "</br>");
		//echo($query);
		if($parent_id == 0 &&  mysqli_num_rows( $result )==0 )
		{
			echo '<div style="display:block; margin:15px 10px;"> Пока нет ни одного раздела. Добавьте разделы, пожалуйста !</div>';
		}
		if(mysqli_num_rows( $result )==0)
		{	
			
			return;
		}
		
		$currCounter = 1;
		if($parent_id == 0)
		{
			echo '<ul id="my-menu" class="sample-menu">';
		}
		else
		{ 
			echo '<ul>';
		}
		$curcat =1;
		while ($row = mysqli_fetch_array($result)) 
		{
			$flag = true;
			/*$insert_category = "update category set  category_code = ".$curcat." where category_id = ".$row['category_id'];
			//echo ($insert_category);
			mysqli_query($GLOBALS['db'],$insert_category) or die (mysqli_error($GLOBALS['db']));*/

			$subcategoryquery = "select category_id	from category  where category_parentcategory_id ='".$row['category_id']."'";
			$result_subcategoryquery = mysqli_query($GLOBALS['db'], $subcategoryquery);
			
			if(mysqli_num_rows($result_subcategoryquery)>0)
				$flag  = false;
			
			if($counter!="")
				$currPrefix = $counter.".".$currCounter;
			else
				$currPrefix = $currCounter;
			
			echo '<li><a href="#0">';		
			echo("<input type = 'hidden' name = 'old_val[".$row['category_id']."]'  value = '".
			( (isset($row['category_code']) && $row['category_code'] != 0) ? ( $row['category_code']):('') )."'>");
				
			$code_value = '';
			if ( isset($_POST['new_val'][$row['category_id']]) )
			{
				//echo("<br>" . $_POST['new_val'][$row['category_id']] . "<br>");
				$code_value = $_POST['new_val'][$row['category_id']];
			}
			else if( isset($row['category_code']) && $row['category_code'] != 0)
			{
				$code_value  = $row['category_code'];
			}
	
			echo $currPrefix.". ";
			echo("<input type='input' size='5' maxlength='4' name = 'new_val[".$row['category_id']."]'  
			value = '".$code_value."'/>");

			echo decode_cool($row['category_name_ru']);		
			if (isset ($row['category_price']) && $row['category_price'] !='')
				echo "&nbsp; - цена:&nbsp;".decode_cool($row['category_price']);	
			echo ("<i class='fa fa-plus fa-green' title='Добавить подкатегорию' onClick='AddAll(".$row['category_id'].");'></i>");
			
			echo ("<i class='fa fa-pencil-square-o fa-orange' title='Редактировать' onClick='EditAll(".$row['category_id'].");'></i>");
			if($flag == true)
				echo ("<i class='fa fa-trash-o fa-red' title='Удалить' onClick='DelAll(".$row['category_id'].");'></i>");
			else
				echo ("<i class='fa fa-trash-o fa-dis' title='Нельзя удалить (есть вложенные категории)'></i>");
				
			if($row['category_hidden'])
				echo("<i class='fa fa-eye' title='Скрытый'></i>");

			echo '</a>';
				get_tree_price($category_menu_id, $row['category_id'], $currPrefix);
			$currCounter += 1;
			$curcat +=1;
		}
		echo '</li>';
		echo '</ul>'; //по выходу из цикла
	}
	
	
	

/*---Функция декодирования символов---*/
function decode_cool($str)
{
	$str=htmlspecialchars_decode(stripslashes(trim($str)));
	return $str;
}

/*function cool_string($str)
{
	return $str=htmlspecialchars(addslashes(trim($str)));
}


function un_cool_string($str)
{
	return $str=htmlspecialchars_decode(stripslashes($str));
}
*/
function createThumbnail($filename, $path, $final_width_of_image) 
{
	//$final_width_of_image = 113; //Размер изображения которые Вы хотели бы получить (И ШИРИНА И ВЫСОТА)
	$path_to_image_directory =  //Папка, куда будут загружаться полноразмерные изображения
	$path_to_thumbs_directory = $path;//Папка, куда буду тзгружать миниатюры
	
	if(preg_match('/[.](jpg|jpeg|JPG|JPE|jpe)$/', $filename)) {
		$im = imagecreatefromjpeg($path_to_image_directory . $filename);
	} else if (preg_match('/[.](gif)$/', $filename)) {
		$im = imagecreatefromgif($path_to_image_directory . $filename);
	} else if (preg_match('/[.](png)$/', $filename)) {
		$im = imagecreatefrompng($path_to_image_directory . $filename);
	} //Определяем формат изображения
	
	$ox = imagesx($im);
	$oy = imagesy($im);
	
	if( $ox > $oy || $ox == $oy )
	{
		$nx = $final_width_of_image;
		$ny = floor($oy * ($final_width_of_image / $ox));
	}
	else
	{
		$ny =  $final_width_of_image;
		$nx = floor($ox * ($final_width_of_image / $oy));
	}
	
	$nm = imagecreatetruecolor($nx, $ny);
	
	imagecopyresized($nm, $im, 0,0,0,0,$nx,$ny,$ox,$oy);
	
	if(!file_exists($path_to_thumbs_directory)) {
	  if(!mkdir($path_to_thumbs_directory)) {
           die("Возникли проблемы! попробуйте снова!");
	  } 
       }
	imagejpeg($nm, $path_to_thumbs_directory . $filename);
	//$tn = '<img src="' . $path_to_thumbs_directory . $filename . '" alt="image" />';
	//$tn .= '<br />Поздравляем! Ваше изображение успешно загружено и его миниатюра удачно выполнена. Выше Вы можете просмотреть результат:';
	//echo $tn;
}//Сжимаем изображение, если есть оишибки, то говорим о них, если их нет, то выводим получившуюся миниатюру

function del_slash($target){
	$target=trim($target);
	if ($target{0}=='/'){
	$target=substr($target,1);}
	if ($target{strlen($target)-1}=='/'){
		$target=substr($target,(strlen($target)-1));
		}
	return $target;
}
function aspect_ratio_resize($image,$fw=0,$fh=0){
	if(isset($image,$fw,$fh)){
		$info = getimagesize($image);
		$w = $info[0];//ширина картинки
		$h = $info[1];//высота картинки
		unset($info);
		$ar = $w/$h;//отношение сторон
		($ar>1)?($fh=0):($fw=0);
		if($fw){
			$out[]=$fw;
			$out[]=$h*$ar;
			$out[]='width="'.$fw.'" height="'.$h*$ar.'"';
			return $out;
			}
		elseif($fh){
			$out[]=$w*$ar;
			$out[]=$fh;
			$out[]='width="'.$w*$ar.'" height="'.$fh.'"';
			return $out;
			}
		}
}

function get_data_curl($url) {
			$ch = curl_init();
  			$timeout = 5;
  			curl_setopt($ch, CURLOPT_URL, $url);
//			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
 			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
  			$data = curl_exec($ch);
  			curl_close($ch);
  			return $data;
}

function ck_var($var=''){
if($var!=''){
		$var=trim($var);
		$var=stripslashes($var);
		$var=htmlspecialchars($var);
		$var=mysqli_real_escape_string($GLOBALS['db'],$var);
		return $var;
		}
		else{return $var='';}
}
	
	function viewCoreInit($view, $arrList = array()) {
		global $title;
		foreach ($arrList as $key => $value) {
			$$key = $value;
		}
		
		$view     = str_replace('.html', '', $view);
		$viewPath = CORE_VIEW . "$view.html";
		
		if(file_exists($viewPath)) {
			include $viewPath;
		}
	}
	

	
	function checkUser() {
		global $db;
		
		if (isset($_COOKIE['t']) && isset($_COOKIE['s'])) {
			
			$session = $_COOKIE['s'];
			$token   = $_COOKIE['t'];
			
			$query = mysqli_query($db, "
				SELECT `emp`.* FROM `emp`
				LEFT JOIN `connect` ON( `id` = `user_id` )
				WHERE `session` = '$session'
				AND   `token`   = '$token';
			");
			
			if (mysqli_num_rows($query) == 1) {
				$token   = getHash();
				
				setcookie('t', $token, time() + 60 * 60 * 24 * 7, '/');
				
				mysqli_query($db, "UPDATE `connect` SET `token` = '$token' WHERE `session` = '$session'");
				
				return mysqli_fetch_assoc($query);
			}
			else {
				$query = mysqli_query($db, "
					SELECT `user_id` FROM `connect`
					WHERE `session` = '$session';
				");
				
				if (mysqli_num_rows($query) > 0) {
					mysqli_query($db, "
						DELETE FROM `connect`
						WHERE `session` = '$session';
					");
				}
				
				setcookie('s', '', time() - 1, '/');
				setcookie('t', '', time() - 1, '/');
				
				return array();
			}
		}
		else {
			setcookie('s', '', time() - 1, '/');
			setcookie('t', '', time() - 1, '/');
			
			return array();
		}
	}
	
?>