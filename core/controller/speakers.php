<?php
	require("core.php");//глобальные переменные и функции
	
	$user = checkUser();
	
	if (empty($user)) {
		header('Location: ' . CONTROL . 'auth');
	}
	
	// $arrJs - массив для подключения дополнительных скриптов
	$arrJs[]   = MAIN_CORE . 'ckeditor/ckeditor.js';
	$arrJs[]   = MAIN_CORE . 'ckeditor/adapters/jquery.js';
	$arrJs[]   = MAIN_CORE . 'ckfinder/ckfinder.js';
	$arrJs[]   = CORE_JS   . 'speakers.js';
	
	// $arrCss - массив для подключения дополнительных стилей
	$arrCss    = array();
	
	// $arrMain - массив для подключения основных страниц, заполняется для каждого конекретного блока
	$arrMain   = array();
	$mainPaig  = '';


	if ($action == 'edit' AND $id == 0) {
		$action = '';
	}
	
	switch ($action) {
		case 'update': // Обновляем запись
			$id    = (int)$_POST['id'] | 0;
			
			if ($id) {
				
				$speaker = '
					UPDATE `speakers` SET 
						`speaker_name`  = "' . sql_cool($_POST['speaker_name'])   . '",
						`speaker_name_en`  = "' . sql_cool($_POST['speaker_name_en'])   . '", 
						`speaker_surname` = "' . sql_cool($_POST['speaker_surname'])  . '",
						`speaker_surname_en` = "' . sql_cool($_POST['speaker_surname_en'])  . '",
						`speaker_descr`      = "' . sql_cool($_POST['speaker_descr'])      . '",
						`speaker_descr_en`      = "' . sql_cool($_POST['speaker_descr_en'])      . '",
						`speaker_position`      = "' . sql_cool($_POST['speaker_position'])      . '",
						`speaker_position_en`      = "' . sql_cool($_POST['speaker_position_en'])      . '",
						`speaker_ranks`      = "' . sql_cool($_POST['speaker_ranks'])      . '",
						`speaker_ranks_en`      = "' . sql_cool($_POST['speaker_ranks_en'])      . '"
				';
			}
		
			else {
				$speaker = '
					INSERT INTO `speakers` SET 
						`speaker_name`  = "' . sql_cool($_POST['speaker_name'])   . '",
						`speaker_name_en`  = "' . sql_cool($_POST['speaker_name_en'])   . '", 
						`speaker_surname` = "' . sql_cool($_POST['speaker_surname'])  . '",
						`speaker_surname_en` = "' . sql_cool($_POST['speaker_surname_en'])  . '",
						`speaker_descr`      = "' . sql_cool($_POST['speaker_descr'])      . '",
						`speaker_descr_en`      = "' . sql_cool($_POST['speaker_descr_en']) . '",
						`speaker_position`      = "' . sql_cool($_POST['speaker_position']) . '",
						`speaker_position_en`      = "' . sql_cool($_POST['speaker_position_en']). '",
						`speaker_ranks`      = "' . sql_cool($_POST['speaker_ranks'])      . '",
						`speaker_ranks_en`      = "' . sql_cool($_POST['speaker_ranks_en'])      . '"
				';
			}
			
			if(isset($_POST['fupload']) && ($_POST['fupload']!='')) {
				$target = substr_replace($_POST['fupload'] , '', 0, 1  );
				$speaker  .= " , `speaker_img` = '$target'";
			}
			
			$speaker.= $id ? " WHERE `speaker_id` = $id" : '';
			mysqli_query($db, $speaker);
			
			header('Location: ' . CONTROL . 'speakers');
			break;
			
		case 'add': case 'edit' :
			if ($id) {
				$title = "Редактирование";
				$query = "
					SELECT  `speaker_id`,
							`speaker_name` 			as `name`,
							`speaker_name_en`   	as `name_en`,
							`speaker_surname` 		as `surname`,
							`speaker_surname_en` 	as `surname_en`,  
							`speaker_img` 			as `img`,   
							`speaker_descr`     	as `descr`,
							`speaker_descr_en`      as `descr_en`,
							`speaker_position`		as `position`,
							`speaker_position_en`	as `position_en`,  
							`speaker_ranks` 		as `ranks`,
							`speaker_ranks_en` 		as `ranks_en` 
					FROM 
						`speakers`
					WHERE 
						`speaker_id` = $id;
				";
			}
			else {
				$title = "Добавить";
			}
			
			$speakers  = getRow($query);
			$mainPaig = 'speakers/speakers_edit';
			$arrMain  = array('speakers' => $speakers);
			break;
		
		default:
			$title = "Просмотр";
			$info['filter'] = isset($_POST['filter']) ? mysqli_real_escape_string($db, $_POST['filter']) : '';
/*			$speaker_school="
		SELECT  `speaker_id`,  
				`speaker_name` 			as `name`,
				`speaker_name_en` 		as `name_en`,
				`speaker_surname` 		as `surname`,
				`speaker_surname_en` 	as `surname_en`,  
				`speaker_img` 			as `img`,   
				`speaker_descr`         as `descr`,
				`speaker_descr_en`      as `descr_en`,
				`speaker_position`		as `position`,
				`speaker_position_en`	as `position_en`,
				`speaker_ranks` 		as `ranks`,
				`speaker_ranks_en` 		as `ranks_en`,
				schools.school_name 	as `school`, 
				schools.school_start    as `school_start`, 
				schools.school_end
		FROM  `speakers` 
		INNER JOIN  `speakers_schools` ON speakers_schools.speakers_schools_speaker_id = speakers.speaker_id
		INNER JOIN  `schools` ON speakers_schools.speakers_schools_school_id = schools.school_id
		ORDER BY  `speaker_id` 
		LIMIT 0 , 30
	";*/
			$speakers  = getArray("
					SELECT  `speaker_id`,
							`speaker_name` 			as `name`,
							`speaker_name_en`   	as `name_en`,
							`speaker_surname` 		as `surname`,
							`speaker_surname_en` 	as `surname_en`,  
							`speaker_img` 			as `img`,   
							`speaker_descr`     	as `descr`,
							`speaker_descr_en`      as `descr_en`,
							`speaker_position`		as `position`,
							`speaker_position_en`	as `position_en`,  
							`speaker_ranks` 		as `ranks`,
							`speaker_ranks_en` 		as `ranks_en` 
					FROM 
						`speakers`
				");

			$mainPaig = 'speakers/speakers_main';
			$arrMain  = array('speakers' => $speakers, 'info' => $info);
	}


	
	// Отображение всех необходимых страниц из вьювера
	viewCoreInit('main/admin_header', array('arr' => $arrCss));
	viewCoreInit('main/admin_menu');
	if ((int)($user['rules'] & 2) == 0) {
		echo 'У Вас нет прав для работы с этим разделом';
	}
	else {
		viewCoreInit($mainPaig, $arrMain);
	}
	viewCoreInit('main/admin_footer');
	viewCoreInit('main/jcore', array('arr' => $arrJs));
	viewCoreInit('main/admin_end');
?>