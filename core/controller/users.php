<?php
	require("core.php");
	$db   = call_db(HOST, USER, PASSWORD, DBNAME);
	$user = checkUser();
	
	if (empty($user)) {
		header('Location: ' . CONTROL . 'auth/');
	}
	elseif ((int)($user['rules'] & EMP) == 0) {
		viewCoreInit('main/admin_header', array('arr' => $arrCss));
		viewCoreInit('main/admin_menu');
		echo 'У Вас нет прав для работы с этим разделом';
		viewCoreInit('main/admin_footer');
		viewCoreInit('main/jcore', array('arr' => $arrJs));
		viewCoreInit('main/admin_end');
	}
	else {
		// $arrJs - массив для подключения дополнительных скриптов
		$arrJs[] = CORE_JS . 'users.js';
		
		// $arrCss - массив для подключения дополнительных стилей
		$arrCss    = array();
		
		// $arrMain - массив для подключения основных страниц, заполняется для каждого конекретного блока
		$arrMain   = array();
		$mainPaig  = '';
		
		if ($action == 'edit' AND $id == 0) {
			$action = '';
		}
		
		switch ($action) {
			case 'update': // Обновляем запись
				$id    = (int)$_POST['id'] | 0;
				$rules = 0;
				foreach($_POST['user_rule'] as $rule) {
					$rules+= (int)$rule;
				}
				
				if ($id) {
					$count = getRow("SELECT COUNT( * ) AS `cnt` FROM `emp` WHERE `id` = $id AND `passwd` = '" . sql_cool($_POST['pass']) . "'");
					
					$user = '
						UPDATE `emp` SET 
							`last_name`  = "' . sql_cool($_POST['last_name'])   . '", 
							`first_name` = "' . sql_cool($_POST['first_name'])  . '",
							`patronymic` = "' . sql_cool($_POST['patronymic']) . '",
							`login`      = "' . sql_cool($_POST['login'])      . '",
							' . ($count['cnt'] == 0 ? '`passwd` = "' . md5($_POST['pass']) . '",' : '') . '
							`email`      = "' . sql_cool($_POST['email'])      . '",
							`rules`      = ' . $rules . '
						WHERE `id` = ' . $id . ';
					';
				}
				else {
					$user = '
						INSERT INTO `emp` SET 
							`last_name`  = "' . sql_cool($_POST['last_name'])   . '", 
							`first_name` = "' . sql_cool($_POST['first_name'])  . '",
							`patronymic` = "' . sql_cool($_POST['patronymic']) . '",
							`login`      = "' . sql_cool($_POST['login'])      . '",
							`passwd`     = "' . md5($_POST['pass'])            . '",
							`email`      = "' . sql_cool($_POST['email'])      . '",
							`rules`      = ' . $rules . '
					';
				}
				
				mysqli_query($db, $user);
				header('Location: ' . CONTROL . 'users/');
				break;
				
			case 'add': case 'edit' :
				if ($id) {
					$title = "Редактирование";
					$query = "
						SELECT 
							`id`         AS `id`,
							`last_name`  AS `lastname`,
							`first_name` AS `firstname`,
							`patronymic` AS `patronymic`,
							`login`      AS `login`, 
							`email`      AS `email`,
							`passwd`     AS `pass`,
							`rules`      AS `rule`
						FROM 
							`emp`
						WHERE 
							`id` = $id;
					";
				}
				else {
					$title = "Добавить";
				}
				
				$users  = getRow($query);
				$mainPaig = 'users/users_edit';
				$arrMain  = array('users' => $users);
				break;
			
			default:
				$title = "Просмотр";
				$users  = getArray("
					SELECT 
						`id`                                                      AS `id`,
						`login`                                                   AS `login`,
						CONCAT_WS( ' ', `last_name`, `first_name`, `patronymic` ) AS `name`,
						`email`                                                   AS `mail`,
						DATE_FORMAT( `lastvisit`, '%d.%m.%y' )                    AS `date`             
					FROM 
						`emp`
					ORDER BY `name`;
				");
				$mainPaig = 'users/users_main';
				$arrMain  = array('users' => $users);
		}
		
		// Отображение всех необходимых страниц из вьювера
		viewCoreInit('main/admin_header', array('arr' => $arrCss));
		viewCoreInit('main/admin_menu');
		viewCoreInit($mainPaig, $arrMain);
		viewCoreInit('main/admin_footer');
		viewCoreInit('main/jcore', array('arr' => $arrJs));
		viewCoreInit('main/admin_end');
	}
?>