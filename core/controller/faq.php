<?php
	require("core.php");
	
	$arrJs   = array();
	$arrCss  = array();
	$arrMain = array();
	$user    = checkUser();
	
	if (empty($user)) {
		header('Location: ' . CONTROL . 'auth/');
	}
	elseif ((int)($user['rules'] & FAQ) == 0) {
		viewCoreInit('main/admin_header', array('arr' => $arrCss));
		viewCoreInit('main/admin_menu');
		echo 'У Вас нет прав для работы с этим разделом';
		viewCoreInit('main/admin_footer');
		viewCoreInit('main/jcore', array('arr' => $arrJs));
		viewCoreInit('main/admin_end');
	}
	else {
		$arrJs[] = MAIN_CORE . 'ckeditor/ckeditor.js';
		$arrJs[] = MAIN_CORE . 'ckeditor/adapters/jquery.js';
		$arrJs[] = MAIN_CORE . 'ckfinder/ckfinder.js';
		$arrJs[] = CORE_JS   . 'faq.js';
		
		$mainPaig  = '';
		
		if ($action == 'edit' AND $id == 0) {
			$action = '';
		}
		
		switch ($action) {
			case 'update':
				$id   = (int)$_POST['faq_id'] | 0;
				$data = $_POST;
				
				if (!$id) {
					$id = insert('faq', $data);
				}
				else {
					$where = array('faq_id' => $id);
					update('faq', $data, $where);
				}
				
				header('Location: ' . CONTROL . 'faq/');
				break;
			
			case 'add': case 'edit':
				if ($id) {
					$title = "Редактирование";
					$faq  = getRow("
						SELECT 
							`faq_id`       AS `id`,
							`faq_question` AS `question`,
							`faq_answer`   AS `answer`,
							`faq_sort`     AS `sort`
						FROM `faq` 
						WHERE `faq_id` = $id
					");
				}
				else {
					$title = "Добавление";
					$query = "SHOW COLUMNS FROM `faq`;";
					$cols  = getArray($query);
					
					foreach ($cols as $col) {
						$name = preg_replace('/^faq_(.*)/U', '$1', $col['Field']);
						$faq[$name] = '';
					}
				}
				
				$mainPaig = 'faq/faq_edit';
				$arrMain  = array('info' => $info, 'faq' => $faq);
				break;
			
			default:
				$filter = isset($_POST['filter']) ? mysqli_real_escape_string($db, $_POST['filter']) : '';
				$title  = 'Просмотр';
				$faq    = getArray('
					SELECT 
						`faq_id`,
						`faq_question`,
						`faq_answer`,
						`faq_sort`
					FROM `faq`
					WHERE 1
					AND (`faq_question` LIKE "%' . $filter . '%"
					OR  `faq_answer`    LIKE "%' . $filter . '%")
					ORDER BY `faq_sort`;
				');
				$mainPaig = 'faq/faq_list';
				$arrMain  = array('faq' => $faq, 'filter' => $filter);
		}
		
		// Отображение всех необходимых страниц из вьювера
		viewCoreInit('main/admin_header', array('arr' => $arrCss));
		viewCoreInit('main/admin_menu');
		viewCoreInit($mainPaig, $arrMain);
		viewCoreInit('main/admin_footer');
		viewCoreInit('main/jcore', array('arr' => $arrJs));
		viewCoreInit('main/admin_end');
	}
?>