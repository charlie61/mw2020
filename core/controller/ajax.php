<?php
	require("core.php");//глобальные переменные и функции
	
	$answer['code'] = 0;
	$answer['text'] = 'Ошибка обработки AJAX запроса';
	
	$month = array(1 => "Янв", 2 => "Фев", 3 => "Март", 4 => "Апр", 5 => "Май", 6 => "Июнь", 7 => "Июль", 8 => "Авг", 9 => "Сен", 10 => "Окт", 11 => "Ноя", 12 => "Дек");
	
	if (isset($_POST['type'])) {
		$type = mysqli_real_escape_string($db, $_POST['type']);
		
		switch($type) {
			case 'remove_partners':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					
					mysqli_query($db, "DELETE FROM `partners` WHERE `partner_id` = $id");
					
					$answer['code'] = 1;
					$answer['text'] = 'Информация о партнере успешно удалена';
				}
				else {
					$answer['text'] = 'Пертнер не выбрана!';
				}
				break;
			case 'remove_schools':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					
					mysqli_query($db, "DELETE FROM `schools` WHERE `school_id` = $id");
					
					$answer['code'] = 1;
					$answer['text'] = 'Информация о школе успешно удалена';
				}
				else {
					$answer['text'] = 'Школа не выбрана!';
				}
				break;
				
			case 'remove_template':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					
					mysqli_query($db, "DELETE FROM `template_blocks` WHERE `template_block_id` = $id");
					
					$answer['code'] = 1;
					$answer['text'] = 'Информация о спикере успешно удалена';
				}
				else {
					$answer['text'] = 'Спикер не выбран!';
				}
				break;
				
			case 'remove_speakers':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					mysqli_query($db, "DELETE FROM `speakers_schools` WHERE `speakers_schools_speaker_id ` = $id");
					mysqli_query($db, "DELETE FROM `speakers` WHERE `speaker_id` = $id");
					
					$answer['code'] = 1;
					$answer['text'] = 'Информация о спикере успешно удалена';
				}
				else {
					$answer['text'] = 'Спикер не выбран!';
				}
				break;
			
			case 'remove_participants':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					mysqli_query($db, "UPDATE `participants` SET `participants_is_delete` = 1 WHERE `participants_id` = $id LIMIT 1");
					
					$answer['code'] = 1;
					$answer['text'] = 'Участник успешно удален';
				}
				else {
					$answer['text'] = 'Участник не выбран!';
				}
				break;
			
		case 'return_news':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					mysqli_query($db, "UPDATE `news` SET `news_is_deleted` = 0 WHERE `news_id` = $id LIMIT 1");
					
					$answer['code'] = 1;
					$answer['text'] = 'Новость успешно восстановлена';
				}
				else {
					$answer['text'] = 'Новость не выбрана!';
				}
				break;
		
		case 'return_events':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					mysqli_query($db, "UPDATE `events` SET `events_is_deleted` = 0 WHERE `events_id` = $id LIMIT 1");
					
					$answer['code'] = 1;
					$answer['text'] = 'Анонс успешно восстановлен';
				}
				else {
					$answer['text'] = 'Анонс не выбран!';
				}
				break;
		
			case 'remove_profile':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					mysqli_query($db, "DELETE FROM `profile` WHERE `profile_id` = $id LIMIT 1");
					
					$answer['code'] = 1;
					$answer['text'] = 'Профиль подготовки успешно удален';
				}
				else {
					$answer['text'] = 'Профиль подготовки не выбран!';
				}
				break;
			
			case 'remove_redirects':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					mysqli_query($db, "DELETE FROM `redirects` WHERE `redirects_id` = $id LIMIT 1");
					
					$answer['code'] = 1;
					$answer['text'] = 'Редирект успешно удален';
				}
				else {
					$answer['text'] = 'Редирект не выбран!';
				}
				break;
			
			case 'remove_vacancies':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					mysqli_query($db, "DELETE FROM `vacancies` WHERE `vacancies_id` = $id LIMIT 1");
					
					$answer['code'] = 1;
					$answer['text'] = 'Вакансия успешно удалена';
				}
				else {
					$answer['text'] = 'Вакансия не выбрана!';
				}
				break;
			
			case 'remove_conferences':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					mysqli_query($db, "DELETE FROM `conferences` WHERE `conferences_id` = $id LIMIT 1");
					
					$answer['code'] = 1;
					$answer['text'] = 'Конференция успешно удалена';
				}
				else {
					$answer['text'] = 'Конференция не выбрана!';
				}
				break;
				
			case 'remove_users':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					mysqli_query($db, "DELETE FROM `emp` WHERE `id` = $id LIMIT 1");
					
					$answer['code'] = 1;
					$answer['text'] = 'Пользователь успешно удален';
				}
				else {
					$answer['text'] = 'Пользователь не выбран!';
				}
				break;
			
			case 'remove_program':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					mysqli_query($db, "DELETE FROM `courses` WHERE `courses_id` = $id LIMIT 1");
					
					$answer['code'] = 1;
					$answer['text'] = 'Курс успешно удален';
				}
				else {
					$answer['text'] = 'Курс не выбран!';
				}
				break;
			
			case 'remove_direction':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					mysqli_query($db, "DELETE FROM `spec` WHERE `spec_id` = $id LIMIT 1");
					
					$answer['code'] = 1;
					$answer['text'] = 'Направление подготовки успешно удалено';
				}
				else {
					$answer['text'] = 'Направление подготовки не выбрано!';
				}
				break;
			
			case 'remove_ads':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					mysqli_query($db, "DELETE FROM `ads` WHERE `ads_id` = $id LIMIT 1");
					
					$answer['code'] = 1;
					$answer['text'] = 'Объявление успешно удалено';
				}
				else {
					$answer['text'] = 'Объявление не выбрано!';
				}
				break;
				
			case 'remove_faq':
			if (isset($_POST['id'])) {
				$id = (int)$_POST['id'];
				mysqli_query($db, "DELETE FROM `faq` WHERE `faq_id` = $id LIMIT 1");

				$answer['code'] = 1;
				$answer['text'] = 'Объявление успешно удалено';
			}
			else {
				$answer['text'] = 'Объявление не выбрано!';
			}
			break;
				
			case 'remove_tags':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					mysqli_query($db, "DELETE FROM `tags` WHERE `tags_id` = $id LIMIT 1");
					mysqli_query($db, "DELETE FROM `news_tags` WHERE `news_tags_tags_id` = $id");
					mysqli_query($db, "DELETE FROM `nevents_tags` WHERE `nevents_tags_tags_id` = $id");
					$answer['code'] = 1;
					$answer['text'] = 'Тег успешно удален';
				}

				else {
					$answer['text'] = 'Тег не выбран!';
				}
				break;
			
			case 'remove_section':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					mysqli_query($db, "DELETE FROM `section` WHERE `section_id` = $id LIMIT 1");
					$answer['code'] = 1;
					$answer['text'] = 'Секция успешно удалена';
				}

				else {
					$answer['text'] = 'Секция не выбрана!';
				}
				break;
			
			case 'remove_partners':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					mysqli_query($db, "DELETE FROM `partners` WHERE `partners_id` = $id LIMIT 1");
					
					$answer['code'] = 1;
					$answer['text'] = 'Партнер успешно удален';
				}
				else {
					$answer['text'] = 'Партнер не выбран!';
				}
				break;
			
			case 'remove_info':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					mysqli_query($db, "DELETE FROM `info_blocks` WHERE `info_blocks_id` = $id LIMIT 1");
					
					$answer['code'] = 1;
					$answer['text'] = 'Инфоблок успешно удален';
				}
				else {
					$answer['text'] = 'Инфоблок не выбран!';
				}
				break;
			
			case 'remove_reviews':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					mysqli_query($db, "DELETE FROM `reviews` WHERE `reviews_id` = $id LIMIT 1");
					
					$answer['code'] = 1;
					$answer['text'] = 'Отзыв успешно удален';
				}
				else {
					$answer['text'] = 'Отзыв не выбран!';
				}
				break;
			
			
			
			case 'remove_discipline':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					mysqli_query($db, "DELETE FROM `discipline` WHERE `discipline_id` = $id LIMIT 1");
					
					$answer['code'] = 1;
					$answer['text'] = 'Дисциплина успешно удалена';
				}
				else {
					$answer['text'] = 'Дисциплина не выбрана!';
				}
				break;
				
			case 'remove_album':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					mysqli_query($db, "DELETE FROM `albums` WHERE `albums_id` = $id LIMIT 1");
					
					$answer['code'] = 1;
					$answer['text'] = 'Альбом успешно удален';
				}
				else {
					$answer['text'] = 'Альбом не выбран!';
				}
				break;
				
			case 'remove_videos':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					mysqli_query($db, "DELETE FROM `videos` WHERE `videos_id` = $id LIMIT 1");
					
					$answer['code'] = 1;
					$answer['text'] = 'Видео успешно удалено';
				}
				else {
					$answer['text'] = 'Видео не выбрано!';
				}
				break;
				
			case 'remove_pages':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					mysqli_query($db, "DELETE FROM `pages` WHERE `pages_id` = $id LIMIT 1");
					
					$answer['code'] = 1;
					$answer['text'] = 'Страница успешно удалеан';
				}
				else {
					$answer['text'] = 'Страница не выбрана!';
				}
				break;
			
			case 'remove_person':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					mysqli_query($db, "DELETE FROM `person` WHERE `person_id` = $id LIMIT 1");
					
					$answer['code'] = 1;
					$answer['text'] = 'Сотрудник удален';
				}
				else {
					$answer['text'] = 'Сотрудник не выбран!';
				}
				break;
			
			case 'remove_science':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					mysqli_query($db, "DELETE FROM `science` WHERE `science_id` = $id LIMIT 1");
					
					$answer['code'] = 1;
					$answer['text'] = 'Научное направление успешно удалено';
				}
				else {
					$answer['text'] = 'Научное направление не выбрано!';
				}
				break;
			
			case 'remove_struct':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					mysqli_query($db, "DELETE FROM `struct` WHERE `struct_id` = $id LIMIT 1");
					
					$answer['code'] = 1;
					$answer['text'] = 'Структурное подразделение успешно удалено';
				}
				else {
					$answer['text'] = 'Структурное подразделение не выбрано!';
				}
				break;
			
			case 'remove_menu_zone':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					mysqli_query($db, "UPDATE `menu_zone` SET `menu_zone_active` = 0 WHERE `menu_zone_id` = $id LIMIT 1");
					
					$answer['code'] = 1;
					$answer['text'] = 'Область меню успешно удалена';
				}
				else {
					$answer['text'] = 'Область меню не выбрана!';
				}
				break;
			
			case 'remove_events':
				if (isset($_POST['id'])) {
					$id = (int)$_POST['id'];
					mysqli_query($db, "UPDATE `events` SET `events_is_deleted` = 1 WHERE `events_id` = $id LIMIT 1");
					
					$answer['code'] = 1;
					$answer['text'] = 'Анонс успешно удален';
				}
				else {
					$answer['text'] = 'Анонс не выбран!';
				}
				break;
			
			case 'tags_list':
				$text = mysqli_real_escape_string($db, $_POST['text']);
				$tags = getArray("SELECT `tags_name` AS `tag` FROM `tags` WHERE `tags_news` = 1 AND `tags_name` LIKE '%$text%' ORDER BY `tags_name`");
				$answer['text'] = array();
				
				foreach($tags as $key => $tag) {
					$answer['text'][] = $tag['tag'];
				}
				
				$answer['code'] = 1;
				break;
			
			case 'check_tag':
				$tag = mysqli_real_escape_string($db, $_POST['tag']);
				$id  = mysqli_query($db, "SELECT `tags_id` FROM `tags` WHERE `tags_news` = 1 AND `tags_name` = '$tag';");
				
				if (mysqli_num_rows($id) > 0) {
					$answer['code'] = 1;
				}
				
				break;
			
			case 'auth':
				$login = sql_cool($_POST['login']);
				$pass  = md5($_POST['pass']);
				
				$user = getRow("SELECT `id` FROM `emp` WHERE `login` = '$login' AND `passwd` = '$pass';");
				
				if (!empty($user)) {
					$id      = $user['id'];
					$session = getHash();
					$token   = getHash();
					
					setcookie('s', $session, time() + 60 * 60 * 24 * 7, '/');
					setcookie('t', $token, time() + 60 * 60 * 24 * 7, '/');
					
					mysqli_query ($db, "INSERT INTO `connect` SET `session` = '$session', `token` = '$token', `user_id` = $id;");
					mysqli_query ($db, "UPDATE `emp` SET `lastvisit` = NOW() WHERE `id` = $id");
					$answer['code'] = 1;
					$answer['text'] = '';
				}
				else {
					$answer['text'] = 'Неверная связка логин/пароль';
				}
				
				break;
			
			case 'exit':
				if (isset($_COOKIE['s'])) {
					$session = $_COOKIE['s'];
					$token   = $_COOKIE['t'];
					
					setcookie('s', '', time() - 1, '/');
					
					mysqli_query($db, "DELETE FROM `connect` WHERE `session` = '$session';");
					$answer['code'] = 1;
					$answer['text'] = '';
				}
				
				break;
			
			case 'get_menu_item':
				$id     = (int)$_POST['id'];
				$text   = htmlspecialchars($_POST['text']);
				$parent = (int)$_POST['parent'];
				$zone   = (int)$_POST['zone'];
				$page   = getRow("SELECT `pages_name` AS `name`, `pages_title` AS `title` FROM `pages` WHERE `pages_id` = $id");
				mysqli_query($db, "
					INSERT INTO `menu_item` SET 
						`menu_item_parent_id` = $parent, 
						`menu_item_page_id`   = $id, 
						`menu_item_menu_zone` = $zone,
						`menu_item_name`      = '{$page['name']}',
						`menu_item_title`     = '{$page['title']}';
				");
				$newId = mysqli_insert_id($db);
				
				$answer['text'] = "
				<li class='dd-item' data-id='$newId' data-type='new_link'>
					<div class='dd-handle' rel='$id'>
						<span class='item-name'>$text</span>
						<span class='item-type'>Страница</span>
					</div>
					<span class='toggle-settings'><i class='fa fa-angle-down'></i></span>
					<div class='dd-item-settings'>
						<fieldset class='form-group'>
							<div class='col-md-6'>
								<label for='menu_item_name' class='item-settings-label'>Название пункта меню:</label>
								<input type='text' value='{$page['name']}' class='form-control' name='menu_item_name' id='menu_item_name' data-toggle='tooltip' data-placement='top'>
							</div>
							<div class='col-md-6'>
								<label for='menu_item_title' class='item-settings-label'>Атрибут title:</label>
								<input type='text' value='{$page['title']}' class='form-control' name='menu_item_title' data-toggle='tooltip' data-placement='top'>
							</div>
						</fieldset>
						<fieldset class='form-group'>
							<div class='col-md-6'>
								<label for='menu_item_page_template' class='item-settings-label'>Адрес ссылки</label>
								<input type='text'  value='' class='form-control' disabled name='menu_item_title' data-toggle='tooltip' data-placement='top'>
							</div>
							<div class='col-md-6'>
								<label class='item-settings-label label-block'>Тип отображения меню:</label>
								<input type='checkbox'  name='menu_item_mega_menu' class='item-checkbox' id='menu_item_mega_menu' value='true'>
								<label class='item-settings-label' for='menu_item_mega_menu'> Мегаменю</label>
							</div>
						</fieldset>
						<fieldset class='form-group'>
							<div class='col-md-5'>
								<label class='item-settings-label label-block'>Открывать в отдельном окне:</label>
								<input type='checkbox' name='menu_item_link_target' disabled>
								<label class='item-settings-label'>target='_blank'</label>
							</div>
							<div class='col-md-4'>
								<label class='item-settings-label label-block'>Тип ссылки:</label>
								<input type='checkbox' name='menu_item_link_out' disabled>
								<label class='item-settings-label'>Внешняя ссылка</label>
							</div>
							<div class='col-md-3'>
								<section class='submit'>
									<input type='button' name='link' value='Удалить' title='Удалить' class='ui-button ui-widget ui-state-default ui-corner-all remove_link'>
								</section>
							</div>
						</fieldset>
					</div>
					<ol class='dd-list' rel='$newId'></ol>
				</li>
			";
			break;
			
			case 'add_link':
				$zone = (int)$_POST['zone'];
				mysqli_query($db, "INSERT INTO `menu_item` SET `menu_item_parent_id` = 0, `menu_item_menu_zone` = $zone, `menu_item_type` = 'link';");
				$newId = mysqli_insert_id($db);
				
				$answer['text'] = "
				<li class='dd-item' data-id='$newId' data-type='new_link'>
					<div class='dd-handle' rel='0'>
						<span class='item-name'></span>
						<span class='item-type'>Ссылка</span>
					</div>
					<span class='toggle-settings'><i class='fa fa-angle-down'></i></span>
					<div class='dd-item-settings'>
						<fieldset class='form-group'>
							<div class='col-md-6'>
								<label for='menu_item_name' class='item-settings-label'>Название пункта меню:</label>
								<input type='text' value='' class='form-control' name='menu_item_name' id='menu_item_name' data-toggle='tooltip' data-placement='top'>
							</div>
							<div class='col-md-6'>
								<label for='menu_item_title' class='item-settings-label'>Атрибут title:</label>
								<input type='text' value='' class='form-control' name='menu_item_title' data-toggle='tooltip' data-placement='top'>
							</div>
						</fieldset>
						<fieldset class='form-group'>
							<div class='col-md-6'>
								<label for='menu_item_page_template' class='item-settings-label'>Адрес ссылки</label>
								<input type='text'  value='' class='form-control' name='menu_item_link_href' data-toggle='tooltip' data-placement='top'>
							</div>
							<div class='col-md-6'>
								<label class='item-settings-label label-block'>Тип отображения меню:</label>
								<input type='checkbox'  name='menu_item_mega_menu' class='item-checkbox' id='menu_item_mega_menu' value='true'>
								<label class='item-settings-label' for='menu_item_mega_menu'> Мегаменю</label>
							</div>
						</fieldset>
						<fieldset class='form-group'>
							<div class='col-md-5'>
								<label class='item-settings-label label-block'>Открывать в отдельном окне:</label>
								<input type='checkbox' name='menu_item_link_target'>
								<label class='item-settings-label'>target='_blank'</label>
							</div>
							<div class='col-md-4'>
								<label class='item-settings-label label-block'>Тип ссылки:</label>
								<input type='checkbox' name='menu_item_link_out'>
								<label class='item-settings-label'>Внешняя ссылка</label>
							</div>
							<div class='col-md-3'>
								<section class='submit'>
									<input type='button' name='link' value='Удалить' title='Удалить' class='ui-button ui-widget ui-state-default ui-corner-all remove_link'>
								</section>
							</div>
						</fieldset>
					</div>
					<ol class='dd-list' rel='$newId'></ol>
				</li>
			";
			break;
			
			case 'remove_menu_item':
				$id   = (int)$_POST['id'];
				$info = getRow("SELECT `menu_item_menu_zone` AS `zone`, `menu_item_parent_id` AS `parent` FROM `menu_item` WHERE `menu_item_id` = $id;");
				mysqli_query($db, "DELETE FROM `menu_item` WHERE `menu_item_id` = $id LIMIT 1;");
				mysqli_query($db, "
					UPDATE 
						`menu_item` 
					SET 
						`menu_item_parent_id` = {$info['parent']}
					WHERE 
						`menu_item_parent_id` = $id
					AND 
						`menu_item_menu_zone` = {$info['zone']};
				");
				echo mysqli_error($db);
				break;
			
			case 'save_menu_changed':
				$id   = (int)$_POST['id'];
				$name = sql_cool($_POST['name']);
				$val  = sql_cool($_POST['val']);
				
				mysqli_query($db, "UPDATE `menu_item` SET `$name` = '$val' WHERE `menu_item_id` = $id;");
				break;
			
			case 'update_sort':
				$data = json_decode($_POST['data']);
				foreach ($data as $id => $list) {
					mysqli_query($db, "
						UPDATE `menu_item` SET 
							`menu_item_parent_id` = {$list->prnt}, 
							`menu_item_sort`      = $id
						WHERE 
							`menu_item_id` = {$list->id};
					");
				}
				break;
			
			case 'profile_info':
				$d = (int)$_POST['d'];
				$s = (int)$_POST['s'];
				$p = (int)$_POST['p'];
				$q = getRow("SELECT `profile_discipline_desc` AS `desc` FROM `profile_discipline` WHERE `profile_discipline_discipline_id` = $d AND `profile_discipline_semester` = $s AND `profile_discipline_profile_id` = $p;");
				$answer['text'] = isset($q['desc']) ? htmlspecialchars_decode($q['desc']) : '';
				$answer['list'] = '';
				$q = getArray("SELECT `discipline_name` AS `name`, `discipline_id` AS `id` FROM `profile_discipline` LEFT JOIN `discipline` ON `discipline`.`discipline_id` = `profile_discipline`.`profile_discipline_discipline_id` WHERE `profile_discipline_semester` = $s AND `profile_discipline_profile_id` = $p;");
				
				foreach ($q as $discipline) {
					$answer['list'].= '<li rel="' . $discipline['id'] . '">' . $discipline['name'] . '</li>';
				}
				break;
			
			case 'save_discip':
				$d = (int)$_POST['d'];
				$s = (int)$_POST['s'];
				$p = (int)$_POST['p'];
				$t = htmlspecialchars($_POST['t']);
				
				$q = getRow("SELECT `profile_discipline_id` AS `id` FROM `profile_discipline` WHERE `profile_discipline_discipline_id` = $d AND `profile_discipline_semester` = $s AND `profile_discipline_profile_id` = $p;");
				
				if(!empty($q)) {
					mysqli_query($db, "UPDATE `profile_discipline` SET `profile_discipline_desc` = '$t' WHERE `profile_discipline_id` = {$q['id']};");
				}
				else {
					mysqli_query($db, "
						INSERT INTO `profile_discipline` SET 
							`profile_discipline_desc`          = '$t',
							`profile_discipline_discipline_id` = $d,
							`profile_discipline_profile_id`    = $p,
							`profile_discipline_semester`      = $s;
					");
				}
				break;
			
			case 'unbind_discip':
				$d = (int)$_POST['d'];
				$s = (int)$_POST['s'];
				$p = (int)$_POST['p'];
				mysqli_query($db, "DELETE FROM `profile_discipline` WHERE `profile_discipline_discipline_id` = $d AND `profile_discipline_semester` = $s AND `profile_discipline_profile_id` = $p LIMIT 1;");
				break;
			
			case 'resort_photo':
				$ids  = json_decode($_POST['id']);
				$sort = 1;
				
				foreach ($ids as $i => $id) {
					mysqli_query($db, "UPDATE `photos` SET `photos_sort` = $sort WHERE `photos_id` = $id;");
					$sort++;
				}
				
				break;
			
			case 'getParticipantsList':
				$table = $_POST['table'];

            try{

            require_once(DIR . "../lib/PHPExcel/PHPExcel.php");
            require_once(DIR . "../lib/PHPExcel/PHPExcel/Writer/Excel2007.php");
            require_once(DIR . "../lib/PHPExcel/PHPExcel/IOFactory.php");

                $objPHPExcel = new PHPExcel();
                $objPHPExcel->getProperties()->setCreator("SPbSTU")
                    ->setLastModifiedBy("SPbSTU")
                    ->setTitle("Office 2007 XLSX Test Document")
                    ->setSubject("Office 2007 XLSX Test Document")
                    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Result file");

                $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A1", " № ");
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue("B1", "ФИО");
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue("C1", "Телефон");
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue("D1", "E-mail");

                $i = 1;
                foreach($table as $val)
                {
                    $i++;
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A$i", htmlspecialchars($val[0]));
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("B$i", htmlspecialchars($val[1]));
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("C$i", htmlspecialchars($val[2]));
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("D$i", htmlspecialchars($val[3]));
                }

                $objPHPExcel->setActiveSheetIndex(0);
                $objPHPExcel->getActiveSheet()->setTitle('List of Participants');
                $cellIterator = $objPHPExcel->getActiveSheet()->getRowIterator()->current()->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(true);
                foreach ($cellIterator as $cell){
                    $objPHPExcel->getActiveSheet()->getColumnDimension($cell->getColumn())->setAutoSize(true);
                }
                header('Content-Type: application/vnd.ms-excel');
                $file_name = "kpi_form_".date("Y-m-d_H:i:s").".xls";
                header("Content-Disposition: attachment; filename=$file_name");
                header('Cache-Control: max-age=1');

                $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
                ob_start();
                $objWriter->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();
                $answer['error'] = 0;
                $answer['file']  = "data:application/vnd.ms-excel;base64,".base64_encode($xlsData);
                $answer['text'] = 'ok';

            } catch(\Exception $ex){

            }
                break;

			default:
				$answer['text'] = 'Неизвестный тип запроса';
		}
	}
	
	echo json_encode($answer, JSON_UNESCAPED_UNICODE);
?>