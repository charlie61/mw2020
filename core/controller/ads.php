<?php
	require("core.php");
	
	$arrJs   = array();
	$arrCss  = array();
	$arrMain = array();
	$user    = checkUser();
	
	if (empty($user)) {
		header('Location: ' . CONTROL . 'auth/');
	}
	elseif ((int)($user['rules'] & ADS) == 0) {
		viewCoreInit('main/admin_header', array('arr' => $arrCss));
		viewCoreInit('main/admin_menu');
		echo 'У Вас нет прав для работы с этим разделом';
		viewCoreInit('main/admin_footer');
		viewCoreInit('main/jcore', array('arr' => $arrJs));
		viewCoreInit('main/admin_end');
	}
	else {
		$arrJs[] = MAIN_CORE . 'ckeditor/ckeditor.js';
		$arrJs[] = MAIN_CORE . 'ckeditor/adapters/jquery.js';
		$arrJs[] = MAIN_CORE . 'ckfinder/ckfinder.js';
		$arrJs[] = CORE_JS   . 'ads.js';
		
		$mainPaig  = '';
		
		if ($action == 'edit' AND $id == 0) {
			$action = '';
		}
		
		switch ($action) {
			case 'update':
				$id   = (int)$_POST['ads_id'] | 0;
				$data = $_POST;
				
				if(isset($_POST['fupload'])) {
					$data['ads_img'] = str_replace(USERFILES, '', $_POST['fupload']);
				}
				
				if (!$id) {
					$id = insert('ads', $data);
				}
				else {
					$where = array('ads_id' => $id);
					update('ads', $data, $where);
				}
				
				header('Location: ' . CONTROL . 'ads/');
				break;
			
			case 'add': case 'edit':
				if ($id) {
					$title = "Редактирование";
					$ads  = getRow("
						SELECT 
							`ads_id`       AS `id`,
							`ads_name`     AS `name`,
							`ads_position` AS `position`,
							`ads_img`      AS `img`,
							`ads_text`     AS `text`,
							`ads_sort`     AS `sort`
						FROM `ads` 
						WHERE `ads_id` = $id
					");
				}
				else {
					$title = "Добавление";
					$query = "SHOW COLUMNS FROM `ads`;";
					$cols  = getArray($query);
					
					foreach ($cols as $col) {
						$name = preg_replace('/^ads_(.*)/U', '$1', $col['Field']);
						$ads[$name] = '';
					}
				}
				
				$mainPaig = 'ads/ads_edit';
				$arrMain  = array('info' => $info, 'ads' => $ads);
				break;
			
			default:
				$filter = isset($_POST['filter']) ? mysqli_real_escape_string($db, $_POST['filter']) : '';
				$title  = 'Просмотр';
				$ads    = getArray('
					SELECT 
						`ads_id`,
						`ads_name`,
						`ads_position`,
						`ads_sort`
					FROM `ads`
					WHERE 1
					AND (`ads_name`    LIKE "%' . $filter . '%"
					OR  `ads_position` LIKE "%' . $filter . '%")
					ORDER BY `ads_name`;
				');
				$mainPaig = 'ads/ads_list';
				$arrMain  = array('ads' => $ads, 'filter' => $filter);
		}
		
		// Отображение всех необходимых страниц из вьювера
		viewCoreInit('main/admin_header', array('arr' => $arrCss));
		viewCoreInit('main/admin_menu');
		viewCoreInit($mainPaig, $arrMain);
		viewCoreInit('main/admin_footer');
		viewCoreInit('main/jcore', array('arr' => $arrJs));
		viewCoreInit('main/admin_end');
	}
?>