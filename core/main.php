<?php
	function call_db($host, $user, $password, $dbname){
		global $ln;
		$db = mysqli_connect($host, $user, $password);
		if (mysqli_connect_errno($db)) {
			writeErrLog(__FILE__, __LINE__, mysqli_connect_errno($db), mysqli_connect_error($db));
			exit('Ошибка при создании соединения с базой');
		}
		else {
			mysqli_select_db($db, $dbname);
			mysqli_set_charset($db, "utf8");
			mysqli_query($db,"set names 'utf8'");

			if ($ln == '') {
				mysqli_query($db, 'SET lc_time_names = "ru_RU";');
				mysqli_query($db, 'SET time_zone = "+0:00";');
			}
			elseif ($ln == '_en') {
				mysqli_query($db, 'SET lc_time_names = "en_US";');
				mysqli_query($db, 'SET time_zone = "+0:00";');
			}
			return $db;
		}
	}

	function getArray($query, $db = ''){
		if (!$db) {
			global $db;
		}
		$arr   = array();
		$q     = mysqli_query($db, $query);
		if (mysqli_errno($db)) {
			$data = debug_backtrace();
			writeErrLog($data[0]['file'], $data[0]['line'], mysqli_errno($db), mysqli_error($db), $query);
		}
		else if (mysqli_num_rows($q) > 0) {
			while($res = mysqli_fetch_assoc($q)) {
				$arr[] = $res;
			}
		}
		return $arr;
	}

	function getRow($query){
		global $db;
		$arr = array();
		$q   = mysqli_query($db, $query);
		if (mysqli_errno($db)) {
			$data = debug_backtrace();
			writeErrLog($data[0]['file'], $data[0]['line'], mysqli_errno($db), mysqli_error($db), $query);
		}
		else {
			if (mysqli_num_rows($q) > 0) {
				$arr = mysqli_fetch_assoc($q);
			}
		}
		return $arr;
	}

	function getValue($query) {
		global $db;
		$arr = array();
		$q   = mysqli_query($db, $query);
		if (mysqli_errno($db)) {
			$data = debug_backtrace();
			writeErrLog($data[0]['file'], $data[0]['line'], mysqli_errno($db), mysqli_error($db), $query);
		}
		else {
			if (mysqli_num_rows($q) > 0) {
				$arr = mysqli_fetch_array($q);
			}
		}
		return isset($arr[0]) ? $arr[0] : '';
	}

	function getTableDataInfo($table) {
		$db    = mysqli_connect(HOST, USER, PASSWORD, 'information_schema');
		$info  = array();
		$query = mysqli_query($db, '
			SELECT
				`COLUMN_NAME` AS `col`,
				`DATA_TYPE`   AS `type`,
				`IS_NULLABLE` AS `null`
			FROM `COLUMNS`
			WHERE `TABLE_NAME` = ' . escape($table) . '
			AND `TABLE_SCHEMA` = "' . DBNAME . '";
		');
		while ($data = mysqli_fetch_assoc($query)) {
			$info[$data['col']]['type'] = $data['type'];
			$info[$data['col']]['null'] = $data['null'];
		}
		return $info;
	}

	function insert($table, $arr) {
		global $db;
		$cols = '';
		$vals = '';
		$info = getTableDataInfo($table);
		
		foreach ($arr as $column => $value) {
			if (isset($info[$column])) {
				$cols.= escape($column, 'col') . ",";
				$vals.= escape($value, $info[$column])  . ",";
			}
		}

		if (isset($info[$table . '_add'])) {
			$cols.= escape($table . '_add', 'col') . ',';
			$vals.= 'NOW(),';
		}

		$cols  = trim($cols, ',');
		$vals  = trim($vals, ',');
		$query = "INSERT INTO " . escape($table, 'col') . " ($cols) VALUE($vals);";
		
		mysqli_query($db, $query);
		if (mysqli_errno($db)) {
			$data = debug_backtrace();
			writeErrLog($data[0]['file'], $data[0]['line'], mysqli_errno($db), mysqli_error($db), $query);
			return 0;
		}
		else {
			return mysqli_insert_id($db);
		}
	}

	function insertMultiple($data) {
		global $db;
		if(isset($data['table']) AND isset($data['cols']) AND isset($data['data'])) {
			$info  = getTableDataInfo($data['table']);
			$query = 'INSERT INTO ' . escape($data['table'], 'col') . '(';
			foreach($data['cols'] as $col) {
				$query.= escape($col, 'col') . ',';
			}
			if (isset($info[$data['table'] . '_add'])) {
				$query.= escape($data['table'] . '_add', 'col') . ',';
			}
			$query = trim($query, ',') . ') VALUES ';
			foreach($data['data'] as $list) {
				$query.= '(';
				foreach($list as $col => $val) {
					$query.= escape($val) . ',';
				}
				if (isset($info[$data['table'] . '_add'])) {
					$query.= 'NOW(),';
				}
				$query = trim($query, ',') . '),';
			}
			$query = trim($query, ',') . ';';

			mysqli_query($db, $query);
			if (mysqli_errno($db)) {
				$data = debug_backtrace();
				writeErrLog($data[0]['file'], $data[0]['line'], mysqli_errno($db), mysqli_error($db), $query);
			}
		}
	}

	function update($table, $arr, $where = array()) {
		global $db;

		if (!isset($table) OR !isset($arr)) {
			return 0;
		}
		else {
			$query = 'UPDATE ' . escape($table, 'col') . ' SET ';
			$info = getTableDataInfo($table);

			unset($info[$table . '_id']);
			unset($info[$table . '_add']);
			unset($info[$table . '_views_count']);

			foreach($info as $column => $type) {
				if (isset($arr[$column])) {
					$query.= escape($column, 'col') . ' = ' . escape($arr[$column], $type) . ',';
				}
				else {
					$query.= escape($column, 'col') . ' = NULL,';
				}
			}

			$query = trim($query, ',');
			$query.= ' WHERE 1 ';

			if (!empty($where)) {
				foreach ($where as $col => $val) {
					$query.= ' AND ' . escape($col, 'col') . ' = ' . escape($val);
				}
			}
			
			mysqli_query($db, $query);

			if (mysqli_errno($db)) {
				$data = debug_backtrace();
				writeErrLog($data[0]['file'], $data[0]['line'], mysqli_errno($db), mysqli_error($db), $query);
			}
		}
	}

	function shortUpdate($arr = array()) {
		global $db;

		extract($arr, EXTR_OVERWRITE);

		if (!isset($table) OR !isset($values)) {
			return 0;
		}
		else {
			$query = 'UPDATE ' . escape($table, 'col') . ' SET ';
			$info = getTableDataInfo($table);

			unset($info[$table . '_id']);
			unset($info[$table . '_add']);
			unset($info[$table . '_views_count']);

			foreach($info as $column => $type) {
				if (isset($values[$column])) {
					$query.= escape($column, 'col') . ' = ' . escape($values[$column], $type) . ',';
				}
			}

			$query = trim($query, ',');
			$query.= ' WHERE 1 ';

			foreach ($where as $col => $val) {
				if (is_null($val)) {
					$query.= ' AND ' . escape($col, 'col') . ' IS NULL';
				}
				else {
					$query.= ' AND ' . escape($col, 'col') . ' = ' . escape($val);
				}
			}

			mysqli_query($db, $query);

			if (mysqli_errno($db)) {
				$data = debug_backtrace();
				writeErrLog($data[0]['file'], $data[0]['line'], mysqli_errno($db), mysqli_error($db), $query);
			}
		}
	}

	function delete($arr = array()) {
		global $db;
		extract($arr, EXTR_OVERWRITE);
		if (!isset($table) OR !isset($where)) {
			$error = 1;
		}
		else {
			$query = 'DELETE FROM ' . escape($table, 'col') . ' WHERE 1 ';
			foreach ($where as $col => $val) {
				$query.= ' AND ' . escape($val['col'], 'col') . ' ' . $val['operator'] . ' ' . escape($val['value']);
			}
			mysqli_query($db, $query);
			$error = mysqli_errno($db);
			if ($error) {
				$data = debug_backtrace();
				writeErrLog($data[0]['file'], $data[0]['line'], mysqli_errno($db), mysqli_error($db), $query);
			}
		}

		return $error;
	}

	function escape($str, $info = array('type' => 'text', 'null' => 'NO')) {
		global $db;

		if ($info == 'col') {
			return '`' . mysqli_real_escape_string($db, $str) . '`';
		}

		$type = $info['type'];
		$null = $info['null'];

		if (strstr($type, 'int')) {
			if (!$str AND $null == 'YES') {
				return 'NULL';
			}
			else {
				return intval($str);
			}
		}
		elseif ($type == 'float' OR $type == 'decimal' OR $type == 'double' OR $type == 'real') {
			return floatval($str);
		}
		else {
			$str = trim($str, "\r\n");
			return "'" . mysqli_real_escape_string($db, $str) . "'";
		}
	}

	function writeErrLog ($file, $line, $num, $text, $query) {
		$date = date('d_m_y', time());
			if (!file_exists(LOG)) {
				mkdir(LOG, 755);
			}
			if (!file_exists(LOG . date('d_m_y', time()) . '_errors.html')) {
				$pattern = file_get_contents(LOG . 'error_pattern.html');
				file_put_contents(LOG . $date . '_errors.html', $pattern);
			}
			$err  = fopen(LOG . $date . '_errors.html', 'a');
			$text = '
			<tr class="db">
				<td>' . date('H:i:s', time()) . '</td>
				<td>' . $file  . ' </td>
				<td>' . $line  . ' </td>
				<td>' . $num   . ' </td>
				<td>' . $text  . ' </td>
				<td>' . $query . ' </td>
			</tr> ';
			fwrite ($err, $text);
			fclose ($err);
	}

	function errorHandler ($errno, $errstr, $errfile, $errline) {
		if	($errno) {
			$date = date('d_m_y', time());
			if (!file_exists(LOG)) {
				mkdir(LOG, 755);
			}
			if (!file_exists(LOG . date('d_m_y', time()) . '_errors.html')) {
				$pattern = file_get_contents(LOG . 'error_pattern.html');
				file_put_contents(LOG . $date . '_errors.html', $pattern);
			}
			$err  = fopen(LOG . $date . '_errors.html', 'a');
			$text = '
			<tr>
				<td>' . date('H:i:s', time()) . '</td>
				<td>' . $errfile . ' </td>
				<td>' . $errline . ' </td>
				<td>' . $errno   . ' </td>
				<td>' . $errstr  . ' </td>
				<td></td>
			</tr> ';
			fwrite ($err, $text);
			fclose ($err);
		}
		return true;
	}

	function pagination($limit = 5,$page = 1,$total){
		$stages = 2;
		$start  = $page ? (($page - 1) * $limit) : 0;

		// Initial page num setup
		$prev       = $page - 1;
		$next       = $page + 1;
		$lastpage   = ceil($total/$limit);
		$LastPagem1 = $lastpage - 1;
		$paginate   = '';
		$paginate  .= "<div class='pagination'>";

		if ($lastpage < 2) {
			return '';
		}

		if($lastpage > 1) {
			// prev
			if ($page > 1)
				$paginate.= "<a rel='$prev'><i class='fa fa-angle-left'></i></a>";
			// Pages
			if ($lastpage < 7 + ($stages * 2)) {	// Not enough pages to breaking it up
				for ($counter = 1; $counter <= $lastpage; $counter++) {
					$paginate.= "<a rel='$counter' " . ($counter == $page ? 'class="current"' : '') . ">$counter</a>";
				}
			}
			elseif($lastpage > 5 + ($stages * 2)) {	// Enough pages to hide a few?
				// Beginning only hide later pages
				if($page < 1 + ($stages * 2)) {
					for ($counter = 1; $counter < 4 + ($stages * 2); $counter++) {
						$paginate.= "<a rel='$counter' " . ($counter == $page ? "class='current'" : "") . ">$counter</a>";
					}
					$paginate.= "<span class='dots'><i class='fa fa-ellipsis-h'></i></span>";
					$paginate.= "<a rel='$LastPagem1'>$LastPagem1</a>";
					$paginate.= "<a rel='$lastpage'>$lastpage</a>";
				}
				// Middle hide some front and some back
				elseif($lastpage - ($stages * 2) > $page && $page > ($stages * 2)) {
					$paginate.= "<a rel='1'>1</a>";
					$paginate.= "<a rel='2'>2</a>";
					$paginate.= "<span class='dots'><i class='fa fa-ellipsis-h'></i></span>";

					for ($counter = $page - $stages; $counter <= $page + $stages; $counter++) {
						$paginate.= "<a rel='$counter' " . ($counter == $page ? "class='current'" : "") . ">$counter</a>";
					}

					$paginate.= "<span class='dots'><i class='fa fa-ellipsis-h'></i></span>";
					$paginate.= "<a rel'=$LastPagem1'>$LastPagem1</a>";
					$paginate.= "<a rel'=$lastpage'>$lastpage</a>";
				}
				// End only hide early pages
				else {
					$paginate.= "<a rel='1'>1</a>";
					$paginate.= "<a rel='2'>2</a>";
					$paginate.= "<span class='dots'><i class='fa fa-ellipsis-h'></i></span>";
					for ($counter = $lastpage - (2 + ($stages * 2)); $counter <= $lastpage; $counter++) {
						$paginate.= "<a rel='$counter' " . ($counter == $page ? "class='current'" : "") . ">$counter</a>";
					}
				}
			}
			// Next;
			if ($page != $lastpage)
				$paginate.= "<a rel='$next'><i class='fa fa-angle-right'></i></a>";
		}
		else {
			$paginate.= '<a rel="1" class="current">1</a>';
		}
		$paginate.= "</div>";

		return $paginate;
	}

	function viewInit($view, $arrList = array()) {
		global $title;
		global $infoBlocks;
		global $sets;
		global $ln;
		
		extract($arrList);

		$view     = str_replace('.html', '', $view);
		$viewPath = VIEW . "$view.html";

		if(file_exists($viewPath)) {
			include $viewPath;
		}
	}

	function getHeaderMenu($ln = '') {
		$q = "
			SELECT
				`menu_item_name`                                                    AS `item`,
				`menu_item_title`                                                   AS `title`,
				IF( `menu_item_type` = 'link', `menu_item_link_href`, `pages_url` ) AS `url`,
                `menu_item_parent_id`                                               AS `parent`,
                `menu_item_id`                                                      AS `id`,
				`menu_item_link_out`                                                AS `out`,
				`menu_item_link_target`                                             AS `target`,
				`menu_item_mega_menu`												AS `mega`
			FROM
				`menu_zone`
			LEFT JOIN `menu_item`      ON `menu_zone`.`menu_zone_id`      = `menu_item`.`menu_item_menu_zone`
			LEFT JOIN `pages`          ON `pages`.`pages_id`              = `menu_item`.`menu_item_page_id`
			LEFT JOIN `menu_zone_type` ON `menu_zone`.`menu_zone_type_id` = `menu_zone_type`.`menu_zone_type_id`
			LEFT JOIN `language`       ON `menu_zone_lang_id`             = `language_id`
			WHERE `menu_zone_active` = 1
			AND `language_code`      = '$ln'
			AND `menu_zone_type`.`menu_zone_type_id` = 2
			ORDER BY `menu_zone_id`, `menu_item_sort`
		";

		$menu = getArray($q);
		$menu = sortMenu($menu, 0);
		$menu = getMenu($menu);
		echo $menu;
	}

	function getCAMenu($ln = '') {
		$menu = getArray("
			SELECT
				`menu_item_name`                                                    AS `item`,
				`menu_item_title`                                                   AS `title`,
				IF( `menu_item_type` = 'link', `menu_item_link_href`, `pages_url` ) AS `url`,
				`menu_item_parent_id`                                               AS `parent`,
				`menu_item_id`                                                      AS `id`,
				`menu_item_link_out`                                                AS `out`,
				`menu_item_link_target`                                             AS `target`,
				`menu_item_mega_menu`												AS `mega`
			FROM
				`menu_zone`
			LEFT JOIN `menu_item`      ON `menu_zone`.`menu_zone_id`           = `menu_item`.`menu_item_menu_zone`
			LEFT JOIN `pages`          ON `pages`.`pages_id`                   = `menu_item`.`menu_item_page_id`
			LEFT JOIN `menu_zone_type` ON `menu_zone_type`.`menu_zone_type_id` = `menu_zone`.`menu_zone_type_id`
			LEFT JOIN `language`       ON `menu_zone_lang_id`                  = `language_id`
			WHERE `menu_zone_active` = 1
			AND `language_code`      = '$ln'
			AND `menu_zone_type`.`menu_zone_type_id`  = 1
			ORDER BY `menu_zone_id`, `menu_item_sort`
		");

		$menu = sortMenu($menu, 0);
		$menu = getMenu($menu, 'frst-li');
		echo $menu;
	}

	function sortMenu($items, $parent) {
		$block = array();
		foreach ($items as $item) {
			if ($item['id']) {
				if ($item['parent'] == $parent) {
					$block[(int)$item['id']] = $item;
					$block[(int)$item['id']]['children'] = sortMenu($items, $item['id']);
				}
			}
		}

		return $block;
	}
	
	function getMenu($items, $class = 'scnd-li') {
		$text = '';
		foreach ($items as $num => $item) {
			$text.= '<li class="' . $class . ($item['mega'] ? ' mega-menu' : '') . (!empty($item['children']) ? ' has-child' : '') . '">';
			$text.= '<a href="' . ($item['out'] ? $item['url'] : MAINLN . $item['url'] . ($item['url'] ? '/' : '')) . '" ' . ($item['target'] ? 'target="_blank"' : '') . ' title="' . $item['title'] . '">' . $item['item'] . '</a>';
			if (!empty($item['children'])) {
				$text.= $item['mega'] ? '<div class="mega-block">' : '';
				$text.='<ul class="is-hidden ' . ($item['mega'] ? 'mega-wrap' : '') . '"><li class="go-back"><a href="#">Меню</a></li>';
				$text.= getMenu($item['children'], '');
				$text.= '</ul>';
				$text.= $item['mega'] ? '</div>' : '';
			}
			$text.= '</li>';
		}
		return $text;
	}

	function generateSideMenu($action) {
		global $ln;
		$info = getRow("
			SELECT
				`menu_item_id`        AS `id`,
				`menu_item_parent_id` AS `parent`
			FROM
				`menu_item`
			LEFT JOIN `pages`     ON `pages_id`     = `menu_item_page_id`
			LEFT JOIN `menu_zone` ON `menu_zone_id` = `menu_item_menu_zone`
			WHERE (
				`menu_item_link_href` = '$action'
				OR `pages_url` = '$action'
			)
			AND `menu_zone_active` = 1;
		");
		
		$prnt = (int)$info['parent'];
		$id   = $info['id'];
		$or   = $prnt ? '' : ' OR `menu_item_parent_id` IS NULL ';

		$menu = getArray("
			SELECT
				`menu_item_name`                                                                               AS `name`,
				IF( `menu_item_title` = '' OR `menu_item_title` IS NULL, `menu_item_name`, `menu_item_title` ) AS `title`,
				IF( `menu_item_type` = 'link', `menu_item_link_href`, `pages_url` )                            AS `url`,
				IF( `pages_url` = '$action' OR `menu_item_link_href` = '$action', 'current', '' )              AS `class`,
                `menu_item_parent_id`                                                                          AS `parent`,
                `menu_item_id`                                                                                 AS `id`,
				`menu_item_link_out`                                                                           AS `out`,
				`menu_item_link_target`                                                                        AS `target`
			FROM
				`menu_zone`
			LEFT JOIN `menu_item`      ON `menu_zone`.`menu_zone_id` = `menu_item`.`menu_item_menu_zone`
			LEFT JOIN `pages`          ON `pages`.`pages_id`         = `menu_item`.`menu_item_page_id`
			LEFT JOIN `language`       ON `menu_zone_lang_id`        = `language_id`
			WHERE `menu_zone_active`  = 1
			AND `language_code`       = '$ln'
			AND `menu_zone_type_id`   = 2
			AND ( `menu_item_parent_id` IN( $id, $prnt )
			$or )
			ORDER BY `menu_zone_id`, `menu_item_sort`;
		");

		$menu = sortMenu($menu, $prnt);

		echo getSideMenu($menu);
	}

	function getSideMenu($items) {
		$text = '';
		foreach ($items as $num => $item) {
			$text.= '<li class="' . $item['class'] . ' ' . (!empty($item['children']) ? 'has-child' : '') . '">';
			if (!empty($item['children'])) {
				$text.= '<div class="box">';
				$text.= '<a href="' . ($item['out'] ? $item['url'] : MAINLN . $item['url'] . ($item['url'] ? '/' : '')) . '" ' . ($item['target'] ? 'target="_blank"' : '') . ' title="' . $item['title'] . '">' . $item['title'] . '</a>';
				$text.= '<div class="ar-btn"></div>';
				$text.= '</div>';
				$text.='<ul style="display: none;">';
				$text.= getSideMenu($item['children'], '');
				$text.= '</ul>';
			}
			else {
				$text.= '<a href="' . ($item['out'] ? $item['url'] : MAINLN . $item['url'] . ($item['url'] ? '/' : '')) . '" ' . ($item['target'] ? 'target="_blank"' : '') . ' title="' . $item['title'] . '">' . $item['title'] . '</a>';
			}
			$text.= '</li>';
		}
		return $text;
	}

	function getDayInfo() {
		$date = getRow("SELECT DATE_FORMAT(NOW(), '%d %M %Y') AS `day`, IF( (DATE_FORMAT(NOW(), '%V')) % 2 = 0, 'Четная неделя', 'Нечетная неделя') AS `week`;");
		return $date['day'] . ' г. ' . $date['week'];
	}

	function getDayInfoEn() {
		global $db;
		mysqli_query($db, "SET lc_time_names = 'en_US';");
		$date = getRow("SELECT DATE_FORMAT(NOW(), '%d %M %Y') AS `day`, IF( (DATE_FORMAT(NOW(), '%V')) % 2 = 0, 'Even week', 'Odd week') AS `week`;");
		return $date['day'] . ' ' . $date['week'];
	}

	function getBreadcrumbs($action, $title = '', $page = '') {
		global $ln;
		$lng = trim($ln, '_');
		$menu = getRow("
			SELECT
				`lvl1`.`menu_item_name` AS `name1`,
				`lvl2`.`menu_item_name` AS `name2`,
				`lvl3`.`menu_item_name` AS `name3`,
				IF( `lvl1`.`menu_item_type` = 'link', `lvl1`.`menu_item_link_href`, `pg1`.`pages_url` ) AS `url1`,
				IF( `lvl2`.`menu_item_type` = 'link', `lvl2`.`menu_item_link_href`, `pg2`.`pages_url` ) AS `url2`,
				IF( `lvl3`.`menu_item_type` = 'link', `lvl3`.`menu_item_link_href`, `pg3`.`pages_url` ) AS `url3`
			FROM `menu_zone`
			LEFT JOIN `menu_item` AS `lvl1` ON `menu_zone`.`menu_zone_id` = `lvl1`.`menu_item_menu_zone`
			LEFT JOIN `pages`     AS `pg1`  ON `pg1`.`pages_id`           = `lvl1`.`menu_item_page_id`
			LEFT JOIN `menu_item` AS `lvl2` ON `lvl2`.`menu_item_id`      = `lvl1`.`menu_item_parent_id`
			LEFT JOIN `pages`     AS `pg2`  ON `pg2`.`pages_id`           = `lvl2`.`menu_item_page_id`
			LEFT JOIN `menu_item` AS `lvl3` ON `lvl3`.`menu_item_id`      = `lvl2`.`menu_item_parent_id`
			LEFT JOIN `pages`     AS `pg3`  ON `pg3`.`pages_id`           = `lvl3`.`menu_item_page_id`
			LEFT JOIN `language`            ON `menu_zone_lang_id`        = `language_id`
			WHERE (`pg1`.`pages_url` = '$action' OR `lvl1`.`menu_item_link_href` = '$action')
			AND `language_code`       = '$lng'
			ORDER BY `menu_zone_id` DESC;
		");
		$text = '<a href="' . MAINLN . '"><i class="fa fa-home"></i></a>';
		if (!empty($menu)) {
			for ($i = 3; $i > 0; $i--) {
				if ($menu['name' . $i] != '') {
					if ($menu['url' . $i] != '' AND ($i != 1 OR $title != '')) {
						$text.= '<a href="' . MAINLN . $menu['url' . $i] . '/">' . $menu['name' . $i] . '</a>';
					}
					else {
						$text.= '<span href="#">' . $menu['name' . $i] . '</span>';
					}
				}
			}
		}

		$text.= $title != '' ? '<span href="#">' . $title . '</span>' : '';
		$text.= $page  != '' ? '<span href="#">' . $page  . '</span>' : '';
		echo $text;
	}

	function no_cache() {
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Cache-Control: no-cache, must-revalidate");
		header("Pragma: no-cache");
		header("Last-Modified: ".date("D, d M Y H:i:s")." GMT");
	}

	function getInfoBlocks() {
		$blocks = getArray("SELECT `info_blocks_name` AS `name`, `info_blocks_value` AS `value` FROM `info_blocks`;");
		foreach($blocks as $list) {
			$info[$list['name']] = $list['value'];
		}
		return $info;
	}

	function getSettings() {
		$sets = getRow("SELECT * FROM `settings`;");

		if (empty($sets)) {
			$query = "SHOW COLUMNS FROM `settings`;";
			$cols  = getArray($query);

			foreach ($cols as $col) {
				$sets[$col['Field']] = '';
			}
		}

		return $sets;
	}

	function translit1($str) {
		$en = Array('q','w','e','r','t','y','u','i','o','p','a','s','d','f','g','h','j','k','l','z','x','c','v','b','n','m');
		$EN = Array('Q','W','E','R','T','Y','U','I','O','P','A','S','D','F','G','H','J','K','L','Z','X','C','V','B','N','M');
		$ru = Array('й','ц','у','к','е','н','г','ш','щ','з','х','ъ','ф','ы','в','а','п','р','о','л','д','ж','э','я','ч','с','м','и','т','ь','б','ю');
		$RU = Array('Й','Ц','У','К','Е','Н','Г','Ш','Щ','З','Х','Ъ','Ф','Ы','В','А','П','Р','О','Л','Д','Ж','Э','Я','Ч','С','М','И','Т','Ь','Б','Ю');
		
		$ru2en  = Array('y','c','u','k','e','n','g','sh','sh','z','h','','f','u','v','a','p','r','o','l','d','zgh','e','ya','ch','s','m','i','t','','b','u');
		$RU2EN  = Array('Y','C','U','K','E','N','G','Sh','Sh','Z','H','','F','U','V','A','P','R','O','L','D','Zgh','E','YA','CH','S','M','I','T','','B','U');
		$digits = Array('0','1','2','3','4','5','6','7','8','9');
		
		$ret = '';
		
		for ($i = 0; $i < mb_strlen($str, 'utf-8'); $i++) {
			$ch = mb_substr($str, $i, 1, 'utf-8');
			
			if (!in_array($ch, $en) && !in_array($ch, $EN) && !in_array($ch, $digits)) {
					if (($key = array_search($ch, $ru)) !== false) {
						$ret.= $ru2en[$key];
					} elseif (($key = array_search($ch, $RU)) !== false) {
						$ret.= $RU2EN[$key];
					} else {
						$ret.= '_';
					}
			} else {
				$ret.=$ch;
			}
		}
		
		$ret = trim(preg_replace("/[_]+/ui", "_", $ret), '_');
		
		return $ret;
}

function checkUser2() {
	global $db;

	if (isset($_SESSION['token'])) {
		$session = sql_cool($_COOKIE['PHPSESSID']);
		$token   = sql_cool($_SESSION['token']);

		$user = getRow("
			SELECT `emp`.* FROM `emp`
			LEFT JOIN `connect` ON( `id` = `user_id` )
			WHERE `session` = '$session'
			AND   `token`   = '$token';
		");

		if (!empty($user)) {
			$token = getHash();

			$_SESSION['token'] = $token;

			mysqli_query($db, "UPDATE `connect` SET `token` = '$token' WHERE `session` = '$session'");
			mysqli_query($db, "UPDATE `emp` SET `lastvisit` = NOW() WHERE `id` = ${user['id']};");

			return $user;
		}
		else {
			$query = mysqli_query($db, "
				SELECT `user_id` FROM `connect`
				WHERE `session` = '$session';
			");

			if (mysqli_num_rows($query) > 0) {
				mysqli_query($db, "
					DELETE FROM `connect`
					WHERE `session` = '$session';
				");
			}

			unset($_SESSION['token']);

			return array();
		}
	}
	else {
		unset($_SESSION['token']);
		return array();
	}
}

function getHash($size = 32) {
	$str  = "abcdefghijklmnopqrstuvwxyz0123456789";
	$hash = "";
	for ($i = 0; $i < $size; $i++) {
		$hash.= $str[rand(0, 35)];
	}
	return $hash;
}

	/*---Функция кодирования символов---*/
function sql_cool($str) {
	$str=htmlspecialchars(trim($str));
	$str=mysqli_real_escape_string($GLOBALS['db'],$str);
	return $str;
}
	
	function showViewList ($page, $arr, $lvl = 1) {
		extract($arr);
		
		if (isset($list)) {
			foreach($list as $item) {
				$item['lvl'] = 'lvl_' . $lvl;
				viewInit($page, array('item' => $item, 'dictionary' => $dictionary));
				
				if (isset($item['children']) AND !empty($item['children'])) {
					showViewList($page, array('list' => $item['children']), $lvl+1);
				}
			}
		}
	}
	
	function getPageContent($page, $item = array(), $dictionary = array()) {
		$content = '';
		
		if (isset($item)) {
				ob_start();
				viewInit($page, array('item' => $item, 'dictionary' => $dictionary));
				
				$content = ob_get_contents();
				
				ob_end_clean();
		}
		
		return $content;
	}
	
	function decodeText($text) {
		$text = htmlspecialchars_decode($text);
		$text = preg_replace_callback('/\[([\w]+).*id=\"([\d]+)\"\]/', function($matches){
			global $ln;
			$data = '';
			
			switch($matches[1]) {
				case 'person':
					$person = getRow("
						SELECT
							`person_lastname$ln`                                                      AS `lastname`,
							`person_firstname$ln`                                                     AS `firstname`,
							`person_patronymic$ln`                                                    AS `patronymic`,
							`person_photo`                                                            AS `photo`,
							GROUP_CONCAT(CONCAT(`person_degree_name$ln`,' ',`degree_sphere_name$ln`)) AS `degree`,
							`person_rank_name$ln`                                                     AS `rank`,
							`person_acrank_name$ln`                                                   AS `acrank`,
							`positions_name$ln`                                                       AS `position`,
							`person_url`                                                              AS `url`
						FROM
							`person`
						LEFT JOIN `person_department`  ON `person_id`        = `person_dep_person_id`
						LEFT JOIN `person_degree_list` ON `person_id`        = `person_degree_list_pers_id`
						LEFT JOIN `person_degree`      ON `person_degree_id` = `person_degree_list_deg_id`
						LEFT JOIN `degree_sphere`      ON `degree_sphere_id` = `person_degree_list_sphere_id`
						LEFT JOIN `positions`          ON `positions_id`     = `person_dep_position_id`
						LEFT JOIN `person_rank`        USING( `person_rank_id` )
						LEFT JOIN `person_acrank`      USING( `person_acrank_id` )
						WHERE `person_id` = " . $matches[2] . "
						GROUP BY `person_id`;
					");
					
					$dictionary = array(
						'position'    => 'Занимаемые должности',
						'position_en' => 'Positions',
						'degree'      => 'Ученая степень',
						'degree_en'   => 'Degree',
						'rank'        => 'Ученое звание',
						'rank_en'     => 'Rank',
						'acrank'      => 'Академическое звание',
						'acrank_en'   => 'Academic rank',
					);
					
					$data = getPageContent('person/person_card', $person, $dictionary);
					
					break;
					
				case 'department':
					$department = getRow("
						SELECT 
							`department_name`                                            AS `name`,
							IF(`department_logo`!='',`department_logo`,`department_img`) AS `logo`,
							`department_url`                                             AS `url`
						FROM 
							`department`
						WHERE `department_id` = " . $matches[2] . ";
					");
					
					$data = getPageContent('department/item', $department);
					
					break;
				
				case 'group':
					$item['name']   = getValue("SELECT `groups_name$ln` FROM `groups` WHERE `groups_id` = " . $matches[2] . ";");
					$item['person'] = getArray("
						SELECT
							CONCAT(`person_lastname$ln`,' ',`person_firstname$ln`,' ',COALESCE(`person_patronymic$ln`, ''))                                                      AS `name`,
							GROUP_CONCAT(CONCAT(`positions_name$ln`, IF(`department_name$ln` <> '', ' - ', ''), `department_name$ln`) ORDER BY `positions_id` SEPARATOR ';<br>') AS `position`,
							GROUP_CONCAT(DISTINCT CONCAT(`person_degree_name$ln`,' ',`degree_sphere_name$ln`) SEPARATOR ';<br>')                                                 AS `degree`,
							`person_rank_name$ln`                                                                                                                                AS `rank`
						FROM
							`groups_person`
						LEFT JOIN `person`             ON `person_id`        = `groups_person_person_id`
						LEFT JOIN `person_department`  ON `person_id`        = `person_dep_person_id`
						LEFT JOIN `person_degree_list` ON `person_id`        = `person_degree_list_pers_id`
						LEFT JOIN `person_degree`      ON `person_degree_id` = `person_degree_list_deg_id`
						LEFT JOIN `degree_sphere`      ON `degree_sphere_id` = `person_degree_list_sphere_id`
						LEFT JOIN `positions`          ON `positions_id`     = `person_dep_position_id`
						LEFT JOIN `department`         ON `department_id`    = `person_dep_dep_id`
						LEFT JOIN `person_rank`        USING( `person_rank_id` )
						WHERE `groups_person_council_id` = " . $matches[2] . "
						GROUP BY `person_id`
						HAVING `name` <> ''
						AND `name` IS NOT NULL;
					");
					
					$dictionary = array(
						'name'        => 'ФИО',
						'name_en'     => 'Name',
						'position'    => 'Должность',
						'position_en' => 'Position',
						'rank'        => 'Ученая степень',
						'rank_en'     => 'Degree',
						'degree'      => 'Академическое звание',
						'degree_en'   => 'Academic rank',
					);
					
					if (!empty($item['person']) AND $item['name']) {
						$data = getPageContent('person/person_group', $item, $dictionary);
					}
					
					break;
				
				case 'album':
					$item = getArray("
						SELECT
							`photos_photo` AS `photo`,
							`albums_dir`   AS `dir`
						FROM
							`albums`
						LEFT JOIN `photos` ON `photos_albums_id` = `albums_id`
						WHERE `albums_id` = " . $matches[2] . "
						ORDER BY `photos_sort`;
					");
					
					$data = getPageContent('albums/slider', $item);
					
					break;
				
				default: $data = $matches[0];
			}
			
			return $data;
		},$text);
		
		return $text;
	}
?>
