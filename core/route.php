<?php
	session_start();
	include_once '../global.php';
	include_once 'main.php';

	error_reporting();
	set_error_handler('errorHandler');
	
	$url  = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
	$lang = array(
		array('code' => 'en'),
	);
	$ln   = '';
	$code = '/';
	$lng  = 'ru';
  
	foreach ($lang as $type) {
		if (strpos(SCHEME . '://' . SERVER. $url, MAIN . "{$type['code']}") !== FALSE) {
			$ln   = "_{$type['code']}";
			$code = $type['code'] . '/';
			$lng  = $type['code'];
			break;
		}
	}

	no_cache();

	$db     = call_db(HOST, USER, PASS, DB);
	$action = '';
	$id     = 0;
	$sets   = getSettings();
	
	$routes = array(
		'controller/auth.php' => array(
			'#^' . SUBSERVER . '/?control/auth/$#',
			'#^' . SUBSERVER . '/?control/auth$#',
			'#^' . SUBSERVER . '/?control/$#',
			'#^' . SUBSERVER . '/?control$#',
		),
		'controller/sitemap.php' => array(
			'#^' . SUBSERVER . 'control/sitemap/$#',
			'#^' . SUBSERVER . 'control/sitemap$#',
		),
		'controller/settings.php' => array(
			'#^' . SUBSERVER . 'control/settings/(\w+)/(\d+)/$#',
			'#^' . SUBSERVER . 'control/settings/(\w+)/(\d+)$#',
			'#^' . SUBSERVER . 'control/settings/(\w+)/$#',
			'#^' . SUBSERVER . 'control/settings/(\w+)$#',
			'#^' . SUBSERVER . 'control/settings/$#',
			'#control/settings$#',
		),
		'controller/ads.php' => array(
			'#^' . SUBSERVER . 'control/ads/(\w+)/(\d+)/$#',
			'#^' . SUBSERVER . 'control/ads/(\w+)/(\d+)$#',
			'#^' . SUBSERVER . 'control/ads/(\w+)/$#',
			'#^' . SUBSERVER . 'control/ads/(\w+)$#',
			'#^' . SUBSERVER . 'control/ads/$#',
			'#^' . SUBSERVER . 'control/ads$#',
		),
		'controller/course.php' => array(
			'#^' . SUBSERVER . 'control/course/(\w+)/(\d+)/$#',
			'#^' . SUBSERVER . 'control/course/(\w+)/(\d+)$#',
			'#^' . SUBSERVER . 'control/course/(\w+)/$#',
			'#^' . SUBSERVER . 'control/course/(\w+)$#',
			'#^' . SUBSERVER . 'control/course/$#',
			'#^' . SUBSERVER . 'control/course$#',
		),
		'controller/faq.php' => array(
			'#^' . SUBSERVER . 'control/faq/(\w+)/(\d+)/$#',
			'#^' . SUBSERVER . 'control/faq/(\w+)/(\d+)$#',
			'#^' . SUBSERVER . 'control/faq/(\w+)/$#',
			'#^' . SUBSERVER . 'control/faq/(\w+)$#',
			'#^' . SUBSERVER . 'control/faq/$#',
			'#^' . SUBSERVER . 'control/faq$#',
		),
		'controller/users.php' => array(
			'#^' . SUBSERVER . 'control/users/$#',
			'#^' . SUBSERVER . 'control/users$#',
			'#^' . SUBSERVER . 'control/users/(\w+)/$#',
			'#^' . SUBSERVER . 'control/users/(\w+)$#',
			'#^' . SUBSERVER . 'control/users/(\w+)/(\d+)/$#',
			'#^' . SUBSERVER . 'control/users/(\w+)/(\d+)$#',
		),
		'controller/speakers.php' => array(
			'#^' . SUBSERVER . 'control/speakers/$#',
			'#^' . SUBSERVER . 'control/speakers$#',
			'#^' . SUBSERVER . 'control/speakers/(\w+)/$#',
			'#^' . SUBSERVER . 'control/speakers/(\w+)$#',
			'#^' . SUBSERVER . 'control/speakers/(\w+)/(\d+)/$#',
			'#^' . SUBSERVER . 'control/speakers/(\w+)/(\d+)$#',
		),
		'controller/ajax.php' => array(
			'#^' . SUBSERVER . '/?control/ajax/$#',
			'#^' . SUBSERVER . '/?control/ajax$#',
		),
		'controller/participants.php' => array(
			'#^' . SUBSERVER . '/?control/participants/$#',
			'#^' . SUBSERVER . '/?control/participants$#',
			'#^' . SUBSERVER . '/?control/participants/(\w+)/$#',
			'#^' . SUBSERVER . '/?control/participants/(\w+)$#',
			'#^' . SUBSERVER . '/?control/participants/(\w+)/(\d+)/$#',
			'#^' . SUBSERVER . '/?control/participants/(\w+)/(\d+)$#',
		),
		'controller/index.php' => array(
			'#^' . SUBSERVER . 'index/$#',
			'#^' . SUBSERVER . 'index$#',
			'#^' . SUBSERVER . '/$#',
			'#^' . SUBSERVER . 'en$#',
			'#^' . SUBSERVER . 'en/$#',
		),
		'controller/404.php' => '#^' . SUBSERVER . '/',
	);
	
	define('MAINLN', (trim(MAIN . $code, '/') . '/'));
	foreach ($routes as $page => $list) {
		if (is_array($list)) {
			foreach ($list as $pattern) {
				//echo $pattern . ' =======> ' . $url . '<br>';
				if (preg_match($pattern, $url, $uri)) {
					$action = isset($uri[1]) ? $uri[1] : '';
					$id     = isset($uri[2]) ? $uri[2] : 0;
					break 2;
				}
			}
		}
	}
	
	if ($page && file_exists($page)) {include "$page"; }
	else {
		viewInit('main/head', array('info' => array('title' => 404), 'arr' => array()));
		viewInit('main/header' . $ln);
		viewInit('404');
		viewInit('main/footer');
		viewInit('main/nav_bar');
		viewInit('main/script_side', array());
	}
?>
