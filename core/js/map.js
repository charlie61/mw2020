var map;
var markers = [];
var arr = [];

function initMap() {
	var haightAshbury = {lat: 60.009, lng: 30.375};
	var startPoint = [];
	
	if ($('input[name="anonses_map"]').val() !== '') {
		arr = JSON.parse($('input[name="anonses_map"]').val());
		for (i = 0; i < arr.length; i++) {
			if (arr[i] !== null) {
				startPoint.push(arr[i]);
			}
		}
		haightAshbury = startPoint[0];
	}
	
	map = new google.maps.Map(document.getElementById('map'), {
		zoom: 16,
		center: haightAshbury,
		mapTypeId: google.maps.MapTypeId.TERRAIN
	});
	
	var startPoint = JSON.parse($('input[name="anonses_map"]').val());
	for (i = 0; i < startPoint.length; i++) {
		if (startPoint[i] !== null) {
			addMarker(startPoint[i]);
		}
	}
	
	// This event listener will call addMarker() when the map is clicked.
	map.addListener('click', function(event) {
		addMarker(event.latLng);
	});
}

// Adds a marker to the map and push to the array.
function addMarker(location) {
	if (location !== null) {
		var marker = new google.maps.Marker({
			position: location,
			map: map
		});
		if (location.lat === undefined) {
			newmarker = {lat: location.lat(), lng: location.lng()}
			arr.push(newmarker);
		}
		else {
			arr.push(location);
		}
		
		var num = markers.push(marker);
		$('input[name="anonses_map"]').val(JSON.stringify(arr));
		marker.addListener('click', function(){
			i = num - 1;
			markers[i].setMap(null);
			delete arr[i];
			delete markers[i];
			$('input[name="anonses_map"]').val(JSON.stringify(arr));
		});
	}
}