$(document).ready(function() {
	$('.footable').footable();
	
	$('#filter .reset').on('click', function(){
		$('#filter input[name="filter"]').val('');
		$('#filter').submit();
	});
	
	$('.remove').on('click', function(){
		$('#myModal').attr('rel', $(this).closest('tr').attr('rel'));
	});
	
	$('.remove_data').on('click', function(){
		var id = $('#myModal').attr('rel');
		removeParticipant(id);
	});
	
	$('.show').on('click', function(){
		id = $(this).closest('tr').attr('rel');
		$(location).attr('href', MAIN + 'control/participants/show/' + id + '/');
	});

	$('.xls').on('click', function(){
		getExcel()
	});
	$('.csv').on('click', function(){
		getCSV();
	});


	$('#collect').on('click', function(){
		$('input:checkbox').prop('checked', true);
	});
	$('#clear_all').on('click', function(){
		$('input:checkbox').prop('checked', false);
	});

});

function removeParticipant(id) {
	$.post(AJAX_URL, {type: 'remove_participants', id: id}, function(data){
		data = JSON.parse(data);
		if (data['code'] == 1) {
			$('.footable tbody tr[rel="' + id + '"]').remove();
			$('.footable tbody tr').each(function(i, el){
				$(el).find('td:first').text(++i);
			});
			$('.footable').trigger('footable_redraw');
		}
	});
	$('#myModal').modal('hide');
}

function BrowseServer() {
	var finder = new CKFinder();
	finder.basePath = 'ckfinder/';
	finder.selectActionFunction = SetFileField;
	finder.popup();
}

function SetFileField( fileUrl ) {
	document.getElementById( 'xFilePath' ).value = fileUrl;
}

function removeIco() {
	$('.image_block').remove();
	$('.icon_block').html('<div class="row"><div class="col-md-4"><label>Иконка:</label><div class="input-group"><input id="xFilePath" name="fupload" type="text" size="20" class="form-control" /><span class="input-group-btn"><input type="button" value="Загрузить" onclick="BrowseServer();" class="btn btn-ui"/></span></div></div></div>');
}

function getExcel() {
	var count = $(':checkbox:checked');
	var picked = {};
	if(count.length === 0){
		count = $(':checkbox');
	}
	$.each(count, function(index){
		var row = $(this).parent().nextAll();
		row.length = 4;
		var myRow = [];
		$.each(row, function(){
			myRow.push($(this).text())
		});
		picked[index] = myRow;
	});

	$.post(AJAX_URL, {type: 'getParticipantsList', table: picked}, function(data){
		if (data['error'] === 0) {
			var $a = $("<a>");
			$a.attr("href",data.file);
			$("body").append($a);
			$a.attr("download","list.xls");
			$a[0].click();
			$a.remove();
		}
	}, 'json');
}

function getCSV() {
	var count = $(':checkbox:checked');
	var td = '';
	if(count.length === 0){
		count = $(':checkbox');
	}

	$.each(count, function(){
		var row = $(this).parent().nextAll();
		row.length = 4;
		$.each(row, function(){
			td += $(this).text() + ';';
		});
		td += '\n';
	});
	td = '№;ФИО;Телефон;E-mail;\n'+td;
	var name = 'list';
	var dlink = '<a id="download" href="data:text/xls;charset=utf-8,' + encodeURIComponent('\ufeff' + td) + '" download="' + name + '.csv">' + name + '</a>';
	$('body').append(dlink);
	$("#download")[0].click();
	$("#download").remove();

}
