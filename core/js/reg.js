$(document).ready(function(){
	$('#submit').on('click', function(e){
		e.preventDefault();
		
		var login = $('#login').val();
		var pass  = $('#password').val();
		
		$.post(AJAX_URL, {type: 'auth', login: login, pass: pass}, function(data){
			data = JSON.parse(data);
			if (data['code'] == 0) {
				showNotify(data['text'], 'warning');
			}
			else {
				$(location).attr('href', CONTROL + 'participants/');
			}
		});
	});
});