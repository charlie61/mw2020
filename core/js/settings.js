var file;

$(document).ready(function() {
	$('.ckeditor').ckeditor(function() {
		CKFinder.setupCKEditor(this, CORE + 'ckfinder/');
	});
	/*$("#tabs").tabs();*/
	//$("").on('click', function(){ initMap(); });
	$('.nav-tabs li a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
  		var target = $(e.target).attr("href") // activated tab
  		initMap();
	});
});


function BrowseServer(block) {
	file = block;
	var finder = new CKFinder();
	
	finder.basePath = 'ckfinder/';
	finder.selectActionFunction = SetFileField;
	finder.popup();
}

function SetFileField( fileUrl ) {
	document.getElementById(file).value = fileUrl;
}

function removeIco(text, id, name, el) {
	$(el).parent().html('');
	$('.' + id).html('<div class="row"><div class="col-md-4"><label>' + text + ':</label><div class="input-group"><input id="' + id + '" name="' + name + '" type="text" size="20" class="form-control"/><span class="input-group-btn"><input type="button" value="Загрузить" class="btn btn-ui" onclick="BrowseServer(\'' + id + '\');" /></span></div></div></div>');
}

function initMap() {
	if ($('#map').length > 0) {
		var map        = {};
		var markers    = [];
		var arr        = [];
		var startPoint = [];
		var base       = {lat: 60.00750398, lng: 30.37684321};
		
		if ($('input[name="settings_coord"]').val() !== '') {
			arr = JSON.parse($('input[name="settings_coord"]').val());
			
			for (i = 0; i < arr.length; i++) {
				startPoint.push(arr[i]);
			}
			base = startPoint[0];
		}
		
		map = new google.maps.Map(document.getElementById('map'), {
			zoom: 15,
			center: base,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});
		
		for (i = 0; i < startPoint.length; i++) {
			addMarker(startPoint[i]);
		}
		
		map.addListener('click', function(event) {
			addMarker(event.latLng);
		});
		
		function addMarker(location) {
			var marker = new google.maps.Marker({
				position: location,
				icon: MAIN + '/img/marker-admin.png',
				map: map
			});
			
			if (!$.isNumeric(location.lat)) {
				newmarker = {lat: location.lat(), lng: location.lng()}
				arr.push(newmarker);
			}
			
			var num = markers.push(marker);
			
			$('input[name="settings_coord"]').val(JSON.stringify(arr));
			
			marker.addListener('click', function(){
				i = num - 1;
				var lat = markers[i].position.lat();
				var lng = markers[i].position.lng();
				markers[i].setMap(null);
				delete markers[i];
				arr = arr.filter(function(number) {
					return number.lat != lat && number.lng != lng;
				});
				
				$('input[name="settings_coord"]').val(JSON.stringify(arr));
			});
		}
	}
}