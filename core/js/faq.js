$(document).ready(function() {
	$('.ckeditor').ckeditor(function() {
		CKFinder.setupCKEditor(this, CORE + 'ckfinder/');
	});
	
	$('.footable').footable();
	
	$('#filter .reset').on('click', function(){
		$('#filter input[name="filter"]').val('');
		$('#filter').submit();
	});
	
	$('.remove').on('click', function(){
		$('#myModal').attr('rel', $(this).closest('tr').attr('rel'));
	});
	
	$('.remove_data').on('click', function(){
		var id = $('#myModal').attr('rel');
		removeAds(id);
	});
});

function removeAds(id) {
	$.post(AJAX_URL, {type: 'remove_faq', id: id}, function(data){
		data = JSON.parse(data);
		if (data['code'] == 1) {
			$('.footable tbody tr[rel="' + id + '"]').remove();
			$('.footable').trigger('footable_redraw');
		}
		$('#myModal').modal('hide');
	});
}

function BrowseServer() {
	var finder = new CKFinder();
	finder.basePath = 'ckfinder/';
	finder.selectActionFunction = SetFileField;
	finder.popup();
}

function SetFileField( fileUrl ) {
	document.getElementById( 'xFilePath' ).value = fileUrl;
}

function removeIco() {
	$('.image_block').remove();
	$('.icon_block').html('<div class="row"><div class="col-md-4"><label>Иконка:</label><div class="input-group"><input id="xFilePath" name="fupload" type="text" size="20" class="form-control"/><span class="input-group-btn"><input type="button" value="Загрузить" onclick="BrowseServer();" class="btn btn-ui"/></span></div></div></div>');
}