$(document).ready(function() {
	$('.remove').on('click', function(){
		$('#myModal').attr('rel', $(this).closest('tr').attr('rel'));
	});
	
	$('.remove_data').on('click', function(){
		var id = $('#myModal').attr('rel');
		removetags(id);
	});
});

function removetags(id) {
	$.post(AJAX_URL, {type: 'remove_tags', id: id}, function(data){
		data = JSON.parse(data);
		if (data['code'] == 1) {
			$('.footable tbody tr[rel="' + id + '"]').remove();
			$('.footable tbody tr').each(function(i, el){
				$(el).find('td:first').text(++i);
			});
			$('.footable').trigger('footable_redraw');
		}
		$('#myModal').modal('hide');
	});
}