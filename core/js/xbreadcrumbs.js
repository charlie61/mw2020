/*
	xBreadcrumbs (Extended Breadcrums) jQuery Plugin
	� 2009 ajaxBlender.com
	For any questions please visit www.ajaxblender.com 
	or email us at support@ajaxblender.com
*/

;(function($){
	/*  Variables  */
	$.fn.xBreadcrumbs = function(settings){
		var element = $(this);
		var settings = $.extend({}, $.fn.xBreadcrumbs.defaults, settings);

		function _build(){
			if(settings.collapsible){
				var sz = element.children('li').length;
				element.children('li').children('a').css('white-space', 'nowrap').css('float', 'left');
				element.children('li').children('a').each(function(i, el){
					if(i != sz - 1){
						$(this).css('overflow', 'hidden');
						$(this).attr('init-width', $(this).width());
						$(this).width(settings.collapsedWidth);
					}
				});
			}
			
            element.children('li').mouseenter(function(){
                if($(this).hasClass('hover')){ return; }
                
            	_hideallSubLevels();
            	if(!_subLevelExists($(this))){ return; }

            	// Show sub-level
            	var subLevel = $(this).children('ul');
            	_showHideSubLevel(subLevel, true);
            	
            	if(settings.collapsible && !$(this).hasClass('current')){
            		var initWidth = $(this).children('a').attr('init-width');
            		$(this).children('a').animate({width: initWidth}, 'normal');
            	}
            });
            
            element.children('li').mouseleave(function(){
                var subLevel = $(this).children('ul');
                _showHideSubLevel(subLevel, false);
                
                if(settings.collapsible && !$(this).hasClass('current')){
                	$(this).children('a').animate({width: settings.collapsedWidth}, 'fast');
                }
            });
		};
		
		function _hideallSubLevels(){
			element.children('li').children('ul').each(function(){
                $(this).hide();
                $(this).parent().removeClass('hover');
			});
		};
		
		function _showHideSubLevel(subLevel, isShow){
			if(isShow){
                subLevel.parent().addClass('hover');
                if($.browser.msie){
                	var pos = subLevel.parent().position();
                	subLevel.css('left', parseInt(pos['left']));
                }
				if(settings.showSpeed != ''){ subLevel.fadeIn( settings.showSpeed ); } 
				else { subLevel.show(); }
			} else {
                subLevel.parent().removeClass('hover');
                if(settings.hideSpeed != ''){ subLevel.fadeOut( settings.hideSpeed ); } 
                else { subLevel.hide(); }
			}
		};
		
		function _subLevelExists(obj){
			return obj.children('ul').length > 0;
		};
		
		//    Entry point
		_build();
	};
	
	/*  Default Settings  */
	$.fn.xBreadcrumbs.defaults = {
		showSpeed:        'fast',
		hideSpeed:        '',
		collapsible:      false,
		collapsedWidth:   10
	};
})(jQuery);