$(document).ready(function() {
	$('.ckeditor').ckeditor(function() {
		CKFinder.setupCKEditor(this, CORE + 'ckfinder/');
	});
	$('.remove').on('click', function(){
		$('#myModal').attr('rel', $(this).closest('tr').attr('rel'));
	});
	
	$('.remove_data').on('click', function(){
		var id = $('#myModal').attr('rel');
		removespeaker(id);
	});
});

function removespeaker(id) {
	$.post(AJAX_URL, {type: 'remove_speakers', id: id}, function(data){
		data = JSON.parse(data);
		if (data['code'] == 1) {
			$('.footable tbody tr[rel="' + id + '"]').remove();
			$('.footable tbody tr').each(function(i, el){
				$(el).find('td:first').text(++i);
			});
			$('.footable').trigger('footable_redraw');
		}
		$('#myModal').modal('hide');
	});
}

function BrowseServer()
{
	// You can use the "CKFinder" class to render CKFinder in a page:
	var finder = new CKFinder();
	finder.basePath = 'ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
	finder.selectActionFunction = SetFileField;
	finder.popup();
}

// This is a sample function which is called when a file is selected in CKFinder.
function SetFileField( fileUrl ) {
	document.getElementById( 'xFilePath' ).value = fileUrl;
}

function removeIco() {
	$('.image_block').remove();
	$('.icon_block').html('<div class="row"><div class="col-md-4"><label>Фото:</label><div class="input-group"><input id="xFilePath" name="fupload" type="text" size="20" class="form-control" /><span class="input-group-btn"><input type="button" value="Загрузить" onclick="BrowseServer();" class="btn btn-ui"/></span></div></div></div>');
}