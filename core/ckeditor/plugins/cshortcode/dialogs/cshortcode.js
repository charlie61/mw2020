CKEDITOR.dialog.add( 'cshortcodeDialog', function( editor ) {
    return {
        title: 'Insert Shortcode',
        contents: [{
            id: "tab-basic",
            label: "Basic Settings",
            elements: [{
                type: "text",
                id: "personId",
                label: "Please enter person id",
				validate: CKEDITOR.dialog.validate.notEmpty( "Person id field cannot be empty." )
            }]
        }],
        onOk: function() {
            var dialog = this;
			var id = dialog.getValueOf( 'tab-basic', 'personId' ).trim();
			
			if (id){
				var el = editor.document.createElement( 'div' );
					el.setText('[person id="' + id + '"]');
				   //editor.insertHtml( '[person id="' + id + '"]' );
				 editor.insertElement( el );
			}
     
        }
    };
});