CKEDITOR.plugins.add( 'cshortcode', {
    icons: 'cshortcode',
    init: function( editor ) {
        editor.addCommand( 'cshortcode', new CKEDITOR.dialogCommand( 'cshortcodeDialog' ) );
        editor.ui.addButton( 'Cshortcode', {
            label: 'Insert shortcode',
            command: 'cshortcode',
            toolbar: 'insert'
        });

        CKEDITOR.dialog.add( 'cshortcodeDialog', this.path + 'dialogs/cshortcode.js' );
    }
});

      /*   editor.addCommand( 'insertCshortcode', {
            exec: function( editor ) {
				 var now = new Date();
                editor.insertHtml( 'The current date and time is: <em>' + now.toString() + '</em>' );
               //CKEDITOR.dialog.add(  );
            }
        });*/