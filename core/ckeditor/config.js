CKEDITOR.editorConfig = function( config ) {
	config.language = 'ru';
	//config.skin='moono2';
	//config.contentsCss = [ '/css/main.css', '/css/colors.css' ];
	config.toolbarCanCollapse = true;
	config.format_tags = 'p;h2;h3;h4;h5;h6;pre;address;div';
	config.extraPlugins = 'btgrid,widgetbootstrap,widgettemplatemenu,widgetcommon,markdown,cshortcode';
	config.mathJaxLib = '//cdn.mathjax.org/mathjax/2.6-latest/MathJax.js?config=TeX-AMS_HTML';
	config.extraAllowedContent = '*[id]; *(*);';//не убирать классы и id
    forcePasteAsPlainText= 'true';//очистить насильно форматирование
	config.toolbar = [
	{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', 'Markdown','autoFormat', 'CommentSelectedRange', 'UncommentSelectedRange', 'AutoComplete', '-', 'WidgetTemplateMenu',/*'Save', 'NewPage', 'Preview',*/ 'Print', '-', 'Templates' ] },
	{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
	{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll'/*, '-', 'Scayt' */] },
	'/',
	{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'RemoveFormat','-','Bold', 'Italic', 'TransformTextSwitcher', 'TransformTextToLowercase', 'TransformTextToUppercase', 'TransformTextCapitalize',/*'Underline', 'Strike', */'Subscript', 'Superscript'] },
	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align'/*, 'bidi'*/ ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv','WidgetBootstrap'/*, '-', 'BidiLtr', 'BidiRtl', 'Language'*/ ] },
	{ name: 'alignment', groups:[ 'align'], items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
	{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
	'/',
	{ name: 'styles', items: [ /*'Styles',*/ 'Format', /*'Font', 'FontSize' */] },
	{ name: 'insert', items: [ 'Image','Table','oembed','btgrid','Chart','-','Mathjax','SpecialChar'] },
	{ name: 'richembed', items: [  'Iframe', 'Syntaxhighlight','Googledocs','-','Sketchfab', 'qrc','wenzgmap','Cshortcode' ] },
	{ name: 'tools', items: [ 'Zoom', 'Maximize', 'ShowBlocks' ] },
];

}
