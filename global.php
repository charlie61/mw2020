<?php
	# Настройки БД
	define('HOST', 'localhost');
	define('USER', 'mw2020');
	define('PASS', 'Mw@@2oRko2019!');
	define('DB',   'mw2020');

	# Определение путей
	define('DIR',  pathinfo($_SERVER['SCRIPT_FILENAME'], PATHINFO_DIRNAME) . '/');
	define('SCHEME', (is_null($_SERVER['REQUEST_SCHEME']) ? 'http' : $_SERVER['REQUEST_SCHEME']) . '://');
	define('SERVER', $_SERVER['SERVER_NAME'] . '/');
	define('SUBSERVER', str_replace(array('/en', '/main.php', '/core/route.php'), array('', ''), $_SERVER['SCRIPT_NAME']) != '' ? str_replace(array('/en', '/main.php', '/core/route.php'), array('', ''), $_SERVER['SCRIPT_NAME']) . '/' : '');
	define('BASE',  str_replace('//', '/', SERVER . SUBSERVER));
	define('MAIN',  SCHEME . str_replace('//', '/', SERVER . SUBSERVER));
	define('MAINEN',  MAIN . 'en/');
	define('TEST', TRUE);
	define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']));

	# Основная секция
	define('MODEL',      DIR  . 'model/');
	define('CONTROLLER', DIR  . 'controller/');
	define('VIEW',       DIR  . 'view/');
	define('CSS',        MAIN . 'css/');
	define('JS',         MAIN . 'js/');
	define('IMG',        MAIN . 'img/');
	define('FAVIC',      MAIN . 'favicon/');
	define('FONT',       MAIN . 'fonts/');
	define('AJAX',       MAIN . 'ajax/');
	define('LOG',        DIR  . 'log/');
	define('USERFILES',  MAIN . 'userfiles/');
	define('DIRFILES',   DIR  . 'userfiles/');

	setlocale(LC_ALL, 'rus_rus');
	date_default_timezone_set('Europe/Moscow');
?>
