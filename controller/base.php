<?php 
	class controller_base {
		protected static $ln = '';
		protected static $title;
		protected static $main;
		protected static $bc  = array();
		protected static $css = array();
		protected static $js  = array();
		
		static function main($method, $id){}
		
		protected static function render($page, $data = array()) {
			extract($data);
			if (preg_match('/([a-z_\/0-9]*)\.?\w*/i', $page, $result)) {
			    if(file_exists(VIEW . $result[1] . '.html')){
                    include_once VIEW . $result[1] . '.html';
                }
			}
		}
		
		protected static function showOnePage(){
			if (!IS_AJAX) {
				foreach (self::$main as $page => $data) {
					self::render($page, $data);
				}
			}
		}
		
		protected static function showPage(){
			if (!IS_AJAX) {
				self::render('main/head',   array('css'  => self::$css, 'title' => self::$title));
				self::render('main/header', array('bc' => self::$bc));
				
				foreach (self::$main as $page => $data) {
					self::render($page, $data);
				}
				
				self::render('main/footer');
				self::render('main/script', array('js' => self::$js));
			}
		}
		
		protected static function showError(){
			if(IS_AJAX) {
				$answer['error'] = 1;
				$answer['type'] = 'warning';
				$answer['text'] = 'У Вас не достаточно прав для работы с этим разделом!';
				echo json_encode($answer);
			}
			else {
				controller_error::main('403');
				self::showPage();
			}
		}
		
		protected static function showNotFound(){
			if(IS_AJAX) {
				$answer['error'] = 1;
				$answer['type'] = 'warning';
				$answer['text'] = 'Указанной страницы не существует';
				echo json_encode($answer);
			}
			else {
				controller_error::main('404');
				self::showPage();
			}
		}
	}
?>