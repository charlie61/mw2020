<?php
	class controller_error extends controller_base {
		static function main($method, $id = 0) {
			switch($method) {
				case '403': 
					self::$main = array(
						'codes/403' => array(),
					);
					break;
				case '404':
					self::$main = array(
						'codes/404' => array(),
					);
					break;
			}
		}
	}
?>