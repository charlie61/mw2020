<?php
	class Controller_index extends Controller_base {
		
		static function main($method, $id = 0) {
			self::$css[] = CSS . 'main.css';
			switch($method) {
				default: self::showIndex();
			}
			
			self::showOnePage();
		}
		
		private static function showIndex() {
			self::$main = array(
				'main/index' . self::$ln => array(),
			);
		}
	}
?>