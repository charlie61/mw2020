<?php
	class DB {
		static private $_connect;
		
		static function connect() {
			self::$_connect = mysqli_connect(HOST, USER, PASS, DB);
			if (mysqli_connect_errno()) {
				writeErrLog(__FILE__, __LINE__, mysqli_connect_errno(self::$_connect), mysqli_connect_error(self::$_connect));
			}
			else {
				mysqli_set_charset(self::$_connect, 'utf8');
			}
		}
		
		static function getCount($sql) {
			self::connect();
			
			$info = mysqli_query(self::$_connect, $sql);
			
			if (mysqli_errno(self::$_connect)) {
				$data = debug_backtrace();
				writeErrLog($data[0]['file'], $data[0]['line'], mysqli_errno(self::$_connect), mysqli_error(self::$_connect), $sql);
				return false;
			}
			else {
				return mysqli_num_rows($info);
			}
		}
		
		static function getValue($sql) {
			self::connect();
			
			$info = mysqli_query(self::$_connect, $sql);
			
			if (mysqli_errno(self::$_connect)) {
				$data = debug_backtrace();
				writeErrLog($data[0]['file'], $data[0]['line'], mysqli_errno(self::$_connect), mysqli_error(self::$_connect), $sql);
				return false;
			}
			else {
				if (mysqli_num_rows($info)) {
					$data = mysqli_fetch_array($info);
					return $data[0];
				}
				else {
					return null;
				}
			}
		}
		
		static function getRow($sql) {
			self::connect();
			
			$info = mysqli_query(self::$_connect, $sql);
			
			if (mysqli_errno(self::$_connect)) {
				$data = debug_backtrace();
				writeErrLog($data[0]['file'], $data[0]['line'], mysqli_errno(self::$_connect), mysqli_error(self::$_connect), $sql);
				return false;
			}
			else {
				if (mysqli_num_rows($info)) {
					return mysqli_fetch_assoc($info);
				}
				else {
					return null;
				}
			}
		}
		
		static function getArray($sql) {
			self::connect();
			
			$info = mysqli_query(self::$_connect, $sql);
			
			if (mysqli_errno(self::$_connect)) {
				$data = debug_backtrace();
				writeErrLog($data[0]['file'], $data[0]['line'], mysqli_errno(self::$_connect), mysqli_error(self::$_connect), $sql);
				return false;
			}
			else {
				if (mysqli_num_rows($info)) {
					return mysqli_fetch_all($info, MYSQLI_ASSOC);
				}
				else {
					return array();
				}
			}
		}
		
		static function update($arr = array()) {
			self::connect();
			extract($arr, EXTR_OVERWRITE);
			
			if (!isset($table) OR !isset($value) OR !isset($where)) {
				return false;
			}
			else {
				$sql = 'UPDATE ' . DB::escape($table) . ' SET ';
				
				foreach($value as $column => $val) {
					$sql.= DB::escape($column) . ' = "' . DB::escape($val) . '",';
				}

				$sql = trim($sql, ',');
				$sql.= ' WHERE 1 ';
				
				foreach ($where as $col => $val) {
					$sql.= ' AND ' . DB::escape($col) . ' = ' . DB::escape($val);
				}
				$sql .= ";";
				mysqli_query(self::$_connect, $sql);
				
				if (mysqli_errno(self::$_connect)) {
					$data = debug_backtrace();
					writeErrLog($data[0]['file'], $data[0]['line'], mysqli_errno(self::$_connect), mysqli_error(self::$_connect), $sql);
					return false;
				}
				else {
					return true;
				}
			}
		}
		
		static function insert($arr = array()) {
			$exception = array('NOW()');
			self::connect();
			extract($arr, EXTR_OVERWRITE);
			
			if (!isset($table) OR !isset($value)) {
				return false;
			}
			else {
				$cols = '';
				$vals = '';
				
				foreach ($value as $col => $val) {
					$cols.= self::escape($col) . ",";
					$vals.= (in_array($val, $exception) ? $val : '"' . self::escape($val) . '"')  . ',';
				}
				
				$cols  = trim($cols, ',');
				$vals  = trim($vals, ',');
				
				$sql = 'INSERT INTO  ' . self::escape($table) . '(' . $cols . ') VALUE(' . $vals . ');';
				
				
				mysqli_query(self::$_connect, $sql);
				
				if (mysqli_errno(self::$_connect)) {
					$data = debug_backtrace();
					writeErrLog($data[0]['file'], $data[0]['line'], mysqli_errno(self::$_connect), mysqli_error(self::$_connect), $sql);
					return false;
				}
				else {
					return mysqli_insert_id(self::$_connect);
				}
			}
		}
		
		static function delete($table, $where = []) {
			if (!isset($table) OR empty($where)) {
				return false;
			}
			else {
				self::connect();
				
				$sql = 'DELETE FROM ' . self::escape($table) . ' WHERE 1 ';
				
				foreach ($where as $col => $val) {
					$sql.= 'AND ' . self::escape($col) . ' = "' . self::escape($val) . '"';
				}
				
				$sql.= ';';
				
				mysqli_query(self::$_connect, $sql);
				
				if (mysqli_errno(self::$_connect)) {
					$data = debug_backtrace();
					writeErrLog($data[0]['file'], $data[0]['line'], mysqli_errno(self::$_connect), mysqli_error(self::$_connect), $sql);
					return false;
				}
				else {
					return true;
				}
			}
		}
		
		static function escape($str) {
			self::connect();
			return mysqli_real_escape_string(self::$_connect, $str);
		}
	}
?>