<?php
	class route extends controller_base {
		private static $patterns;
		
		public static function start($url) {
			self::$patterns = array(
				'controller_index' => array(
					'#^' . SUBSERVER . '/?()$#',
                    '#^' . SUBSERVER . '/?()index/$#',
				),
				'controller_participant' => array(
					'#^' . SUBSERVER . '/?()$#',
					'#^' . SUBSERVER . '/?()participant/(ajaxAdd)/$#',
				),
				'error' => array(
					'#^.*$#',
				),
			);
			
			foreach (self::$patterns as $class => $list) {
				foreach ($list as $pattern) {
					if (preg_match($pattern, $url, $info)) {
						if(isset($info[1])){
						    parent::$ln = $info[1] == '' ? $info[1] : '_' . $info[1];
                        }
						$method     = isset($info[2]) && $info[2] !== '' ? htmlspecialchars($info[2]) : 'list';
						$id         = isset($info[3]) ? (int)$info[3] : 0;
						break 2;
					}
				}
			}
			
			$path = str_replace('_', '/', $class) . '.php';
			
			if ($class != 'Error' AND file_exists($path)) {
				include_once $path;
				$class::main($method, $id);
			}
			else {
				self::showNotFound();
			}
		}
		
		public static function main($method, $id){}
	}
?>