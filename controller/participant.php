<?php
	class controller_participant extends controller_base {
		
		static function main($method, $id = 0) {
			switch($method) {
				case 'ajaxAdd':
					self::ajaxAdd();
					break;
			}
			
			self::showPage();
		}
		
		private static function ajaxAdd() {
			if (IS_AJAX) {
				if (!empty($_POST)) {
					$arr = array(
						
						'participants_last_name',
						'participants_first_name',
						'participants_patronymic',
						'participants_organization',
						'participants_position',
						'participants_phone',
						'participants_email'
					);
					$data['table'] = 'participants';
					
					foreach ($_POST as $key => $value) {
						if (in_array($key, $arr)) {
							$data['value'][$key] = htmlspecialchars(trim($value));
						}
					}
					

					$data['value']['participants_add'] = $date = date('Y-m-d H:i:s');;
					if ($id = DB::insert($data)) {
						
							$name = $_POST['participants_last_name'] . ' ' . $_POST['participants_first_name'];
							
							$info['participant'] = $name;
							$info['conf']        = 'The 35th ECMI Mathematical Modelling Week';
							$info['mail']        = $_POST['participants_email'];
							$info['phone']       = $_POST['participants_phone'];
							
							self::sendMail($name, $_POST['participants_email'], 'letter/letter_en.email.html');
							self::sendFromMail($info, 'pavboi@live.spbstu.ru');

						if (!file_exists(MAIN . 'files')) {
							mkdir(MAIN . 'files');
						}

						$answer['error'] = 0;
						$answer['text']  = 'Запись добавлена';
						$answer['type']  = 'success';
					}
					else {
						$answer['error'] = 1;
						$answer['text']  = 'Ошибка обрашения к серверу базы данных';
						$answer['type']  = 'danger';
					}
				}
				else {
					$answer['error'] = 1;
					$answer['type'] = 'info';
					$answer['text'] = 'Не выбраны данные для добавления';
				}
				
				echo json_encode($answer);
			}
			else {
				controller_error::main('404');
			}
		}
		
		private static function sendMail($name, $mail, $page) {
			$subject = 'Notification of registration';
			$message = getPageContent($page, array('participant' => $name));
			$headers = "MIME-Version: 1.0\r\n";
			$headers.= "Content-type: text/html; charset=utf-8\r\n";
			$headers.= 'To: ' . $name . ' <' . $mail . ">\r\n";
			$headers.= 'From: ECMI Modelling Week 2020 <pavboi@live.spbstu.ru >';
			
			mail($mail, $subject, $message, $headers);
		}
		
		private static function sendFromMail($info, $mail) {
			$subject = 'Новая регистрация';
			$message = getPageContent('letter/letter_notice.email.html', array('info' => $info));
			$headers = "MIME-Version: 1.0\r\n";
			$headers.= "Content-type: text/html; charset=utf-8\r\n";
		/*	$headers.= 'To: Администратор <' . $mail . ">\r\n";*/
			$headers.= "To: Администратор <pavboi@live.spbstu.ru >\r\n";
			$headers.= 'From: ECMI Modelling Week 2020 <pavboi@live.spbstu.ru >';
			
			mail($mail, $subject, $message, $headers);
		}
	}
?>